function C = get_plot_colors(N,M,sequential)

if nargin < 1
    N = 8;
end

if nargin < 2
    M = 1;
end

if N > 8
    warning('N cannot be greater than 8. Setting N=8.');
    N = 8;
end

if nargin < 3
    sequential = false;
end

if sequential
C = ...
[008, 029, 088;
 037, 052, 148;
 034, 094, 168;
 029, 145, 192;
 065, 182, 196;
 127, 205, 187;
 199, 233, 180;
 237, 248, 117;
 255, 255, 217;];
else
C = ...
[000, 000, 000;
 255, 000, 000;
 000, 000, 255;
 000, 145, 010;
 255, 000, 255;
 243, 109, 004;
 000, 169, 183;
 118, 042, 131;];
end

C = C(1:N,:) ./ 255;

C = repmat(C,M,1);

N = length(C(:,1));

if true
    x = (1:N);
    y = (2:N+1);
    A = [x; y;];
    figure(101);
    for i = 1:N
        plot(A(:,i),'Color',C(i,:)); hold on;
    end
    grid on;
    hold off;
end