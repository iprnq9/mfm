function [F_RF,W_RF] = beamtraining(H,Atx,Arx,Lt,Lr)

AA = Arx' * H * Atx;
AA = abs(AA);
if Lt > Lr
    [~,idx] = maxk(AA(:),Lr);
    [r,~] = ind2sub(size(AA),idx);
    AAA = AA(r,:);
    [~,idx] = maxk(AAA(:),Lt);
    [~,c] = ind2sub(size(AA(r,:)),idx);
else
    [~,idx] = maxk(AA(:),Lt);
    [~,c] = ind2sub(size(AA),idx);
    AAA = AA(:,c);
    [~,idx] = maxk(AAA(:),Lr);
    [r,~] = ind2sub(size(AA(:,c)),idx);
end

F_RF = [];
W_RF = [];
for ii = 1:length(c)
    F_RF = [F_RF Atx(:,c(ii))];
end
for ii = 1:length(r)
    W_RF = [W_RF Arx(:,r(ii))];
end

return