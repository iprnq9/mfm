function [g,h,J] = omp_based_signal_recovery(PHI,PSI,y,m)
% OMP_BASED_SIGNAL_RECOVERY Compressed sensing estimation using OMP.
%
% We observe the true signal (PSI * g) via measurements made with PHI.
%
%  y = PHI' * PSI * g 
%  
% Args:
%  PHI: L-by-M measurement matrix
%  PSI: L-by-L sparsity basis 
%  y: M-by-1 data vector (observations)
%  m: sparsity level of truth
% 
% Returns:
%  g: L-by-1 estimate of sparsity coefficients
%  h: L-by-1 sparse channel estimate (PSI * g)
%  J: m-by-1 support vector (indeces)
%
% Reference:
%  "Signal Recovery From Random Measurements Via Orthogonal Matching 
%  Pursuit" by Joel A. Tropp and Anna C. Gilbert.

r = y; % init residual
J = []; % init index set
B = []; % init atoms

A = PHI' * PSI; 

% for each sparsity degree
for i = 1:m
    idx = argmax(A' * r); % greedy selection
    J = [J idx]; % update index set
    B = [B A(:,idx)]; % update our atoms
    x = pinv(B) * y; % projection weights
    a = B * x; % project
    r = y - a; % calculate new residual
end

% sparse coefficients
[~,L] = size(PSI);
g = zeros(L,1);
g(J,1) = x;

% signal reconstruction
h = PSI * g; 

return