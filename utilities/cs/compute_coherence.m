function mu = compute_coherence(PHI,PSI)

% Assumes PHI * PSI is our effective measurement matrix.
% If PHI' * PSI is instead valid, it will automatically adjoint PHI.




[D,C] = size(PHI);
[R,B] = size(PSI);

if C ~= R
    if D == R
        PHI = PHI';
        D = C;
        C = R;
    else
        error('PHI is not of proper size.');
    end
end

A = PHI * PSI;
for idx_row = 1:D
    a = A(idx_row,:);
    a = a ./ norm(a,2) .* sqrt(B);
    A(idx_row,:) = a;
end

T = abs(A' * A); % absolute inner product of each pair of columns of A
T(sub2ind(size(T),1:B,1:B)) = 0; % set diag elements to zero

mu = sqrt(B) .* max(max(T));

end