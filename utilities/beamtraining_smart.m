function [F_RF,W_RF] = beamtraining_smart(H,Atx,Arx,Lt,Lr)

AA = Arx' * H * Atx;
AA = abs(AA);
B = AA;

grd = [1 1];
nbr = [1 1];
alpha = 1;
show = false;
debug = false;
[Y,Z] = cell_based_detection_2d(AA,grd,nbr,alpha,show,debug);

AA = Z;

if Lt > Lr
    [~,idx] = maxk(AA(:),Lr);
    [r,~] = ind2sub(size(AA),idx);
    AAA = AA(r,:);
    [~,idx] = maxk(AAA(:),Lt);
    [~,c] = ind2sub(size(AA(r,:)),idx);
else
    [~,idx] = maxk(AA(:),Lt);
    [~,c] = ind2sub(size(AA),idx);
    AAA = AA(:,c);
    [~,idx] = maxk(AAA(:),Lr);
    [r,~] = ind2sub(size(AA(:,c)),idx);
end

disp(['Transmit indeces: ' num2str(c.')]);
disp(['Receive indeces: ' num2str(r.')]);

F_RF = [];
W_RF = [];
for ii = 1:length(c)
    F_RF = [F_RF Atx(:,c(ii))];
end
for ii = 1:length(r)
    W_RF = [W_RF Arx(:,r(ii))];
end

figure(1);
imagesc(B);

figure(2);
imagesc(abs(Y));

figure(3);
imagesc(abs(Z));

figure(4);
mesh(abs(Z));

figure(5);
mesh(B);

return