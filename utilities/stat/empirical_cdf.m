function [F,x] = empirical_cdf(x,show_plot)

if nargin < 2
    show_plot = false;
end

% x = sort(x);
% F = cumsum(x) ./ sum(x);

[F,x] = ecdf(x(:));

if show_plot
    figure();
    plot(x,F,'-k'); grid on;
    xlabel('Value');
    ylabel('CDF');
end

% F = F(:);
% x = x(:);

return