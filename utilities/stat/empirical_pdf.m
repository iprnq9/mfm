function [f,x] = empirical_pdf(x,show_plot,nbins)

if nargin < 2 || isempty(show_plot)
    show_plot = false;
end

if nargin < 3
    % h = histogram(x,'Normalization','pdf');
    [f,e] = histcounts(x,'Normalization','pdf');
else
    % h = histogram(x,nbins,'Normalization','pdf');
    [f,e] = histcounts(x,nbins,'Normalization','pdf');
end

if false
    f = h.Values;
    e = h.BinEdges;
    N = h.NumBins;
else
    N = length(e) - 1;
end

x = [e(1:N) + e(2:N+1)] ./ 2;

if show_plot
    figure();
    plot(x,f,'-k'); grid on;
    xlabel('Event');
    ylabel('Probability');
end

return
