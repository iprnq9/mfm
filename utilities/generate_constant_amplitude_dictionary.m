function A = generate_constant_amplitude_dictionary(Na,resolution,num_entries,opt)

if nargin < 2 || isempty(resolution)
    resolution = Na;
end

if nargin < 3 || isempty(num_entries)
    num_entries = Na;
end

if nargin < 4 || isempty(opt)
    opt = 'dft';
end

if strcmp(opt,'dft')
    A = dftmtx(Na);
elseif strcmp(opt,'random')
    for i = 1:num_entries
        phi = 2 * pi * randi([0, resolution],Na,1) ./ resolution;
        a = exp(1j.*phi);
        A(:,i) = a;
    end
end

return