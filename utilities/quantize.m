function y = quantize(x,bits)

g = max(abs(x));

x = x ./ g; % normalize to +/-1

y = x .* 2^(bits - 1);

if false
    y(y > 0) = floor(y(y > 0));
    y(y < 0) = ceil(y(y < 0));
else
    y = round(y);
end

y = y ./ 2^(bits - 1);
y = y .* g;

return