function params = get_params_device()
% GET_PARAMS_DEVICE Returns a struct of params with default parameters for
% constructing a device object.
%
% Usage:
%  params = GET_PARAMS_DEVICE()
%  params = GET_PARAMS_DEVICE(name)
%
% Args:
%  name: an optional name for the device parameters (e.g., device name)
% 
% Returns:
%  params: a struct of default parameters for constructing a device object

params.object       = 'device';
params.name         = 'device-1';
params.type         = 'transceiver'; % transceiver, transmitter, receiver
params.coordinate   = [0,0,0];
params.marker       = 'kx';
params.duplexing    = 'half'; % half, full

end