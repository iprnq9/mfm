function [Y,Z] = cell_based_detection_2d(X,grd,nbr,alpha,show,debug)
% CELL_BASED_DETECTION_2D Performs cell-based detection on a
% matrix based on some guard sizes, neighbor sizes, and
% thresholding constant.
%
% Usage:
%  [Y,Z] = CELL_BASED_DETECTION_2D(X,grd,nbr,alpha_nbr)
%
% Args:
%  X: the input matrix of samples to perform detection on
%  grd: guard size (in samples) in the horizontal and vertical
%       directions of X; can be a scalar or 2-element vector
%  nbr: neighbor size (in samples) in the horizontal and
%       vertical directions of X; can be a scalar or 2-element
%       vector
%  alpha: the thresholding constant when comparing the energy
%         in the neighbor to the energy in the cell-under-test
%         (CUT)
%  show: boolean option to display the detection process in a
%       figure as it progresses
%  debug: boolean option to step through detection using a
%         keyboard input on each cell (show must be enabled)
%
% Returns:
%  Y: a matrix of hard decisions where 1 is a detection and 0
%     is not corresponding to the locations in X
%  Z: a matrix of soft decisions where higher values indicate
%     stronger likelihood of detection; only values in Z above
%     alpha are candidates for detection
%
% Notes:
%  Could be used for constant false alarm rate (CFAR) detection
%  with appropriate choice of alpha.
%
%  x corresponds to columns, y corresponds to rows. Imagine the
%  matrix origin is top left corner, the x-axis extends to the
%  right across columns, the y-axis extends down across rows.
%  To visualize this, use MATLAB's function imagesc().
%
%  The sizes of grd should be chosen according to the 2-D pulse
%  width of a target.
%
%  A CUT is only classified as a detection if it satisfies the
%  threshold and is the maximum value in the guard region. This
%  indicates only one point will be detected within the guard
%  region. Turning this off increases the number of points
%  detected around a detection.
%
% Example 1:
%  X = eye(10);
%  figure(); imagesc(X); title('Detector input');
%  dx = detector();
%  grd = [0,0];
%  nbr = [1,1];
%  alpha = 0.25;
%  show = true;
%  debug = true;
%  [Y,Z] = dx.cell_based_detection_2d(X,grd,nbr,alpha,show,debug);
%  figure(); imagesc(Y); title('Detector output: hard decisions');
%  figure(); imagesc(Z); title('Detector output: soft decisions');
%
% Example 2:
%  x = ones(1,11);
%  x(1:2:end) = 0;
%  X = diag(x);
%  figure(); imagesc(X); title('Detector input');
%  dx = detector();
%  grd = [1,1];
%  nbr = [1,1];
%  alpha = 0.25;
%  show = true;
%  debug = true;
%  [Y,Z] = dx.cell_based_detection_2d(X,grd,nbr,alpha,show,debug);
%  figure(); imagesc(Y); title('Detector output: hard decisions');
%  figure(); imagesc(Z); title('Detector output: soft decisions');
if nargin < 6
    show = false;
end
if nargin < 7
    debug = false;
end
if length(grd) == 1
    grd_x = grd;
    grd_y = grd;
elseif length(grd) == 2
    grd_x = grd(1);
    grd_y = grd(2);
else
    error('Invalid guard input. Should be a scalar or 2-element vector.');
end
if length(nbr) == 1
    nbr_x = nbr;
    nbr_y = nbr;
elseif length(nbr) == 2
    nbr_x = nbr(1);
    nbr_y = nbr(2);
else
    error('Invalid neighbor input. Should be a scalar or 2-element vector.');
end
[M,N] = size(X);
Y = zeros(M,N);
Z = zeros(M,N);
for m = 1:M
    for n = 1:N
        % cell under test
        idx_cut_x = n;
        idx_cut_y = m;
        cut = abs(X(idx_cut_y,idx_cut_x))^2;
        
        % southeast neighbor
        idx_nbr_se_x = idx_cut_x + (grd_x + nbr_x);
        idx_nbr_se_y = idx_cut_y + (grd_y + nbr_y);
        
        % northeast neighbor
        idx_nbr_ne_x = idx_cut_x + (grd_x + nbr_x);
        idx_nbr_ne_y = idx_cut_y - (grd_y + nbr_y);
        
        % southwest neighbor
        idx_nbr_sw_x = idx_cut_x - (grd_x + nbr_x);
        idx_nbr_sw_y = idx_cut_y + (grd_y + nbr_y);
        
        % northwest neighbor corner
        idx_nbr_nw_x = idx_cut_x - (grd_x + nbr_x);
        idx_nbr_nw_y = idx_cut_y - (grd_y + nbr_y);
        
        % guard region southeast corner
        idx_grd_se_x = idx_cut_x + grd_x;
        idx_grd_se_y = idx_cut_y + grd_y;
        
        % guard region northeast corner
        idx_grd_ne_x = idx_cut_x + grd_x;
        idx_grd_ne_y = idx_cut_y - grd_y;
        
        % guard region southwest corner
        idx_grd_sw_x = idx_cut_x - grd_x;
        idx_grd_sw_y = idx_cut_y + grd_y;
        
        % guard region northwest corner
        idx_grd_nw_x = idx_cut_x - grd_x;
        idx_grd_nw_y = idx_cut_y - grd_y;
        
        % guard region limits (top left, bottom left, top
        % right)
        a = idx_grd_nw_y;
        b = idx_grd_sw_y;
        c = idx_grd_nw_x;
        d = idx_grd_ne_x;
        
        % take only unique indeces so we don't double count
        ab = unique(constrain(a:b,1,M));
        cd = unique(constrain(c:d,1,N));
        
        % neighbor region limits (top left, bottom left, top
        % right)
        A = idx_nbr_nw_y;
        B = idx_nbr_sw_y;
        C = idx_nbr_nw_x;
        D = idx_nbr_ne_x;
        
        % take only unique indeces so we don't double count
        AB = unique(constrain(A:B,1,M));
        CD = unique(constrain(C:D,1,N));
        
        % integrate big regions (including overlap)
        CUT = abs(X(idx_cut_y,idx_cut_x))^2;
        GRD_MAX = max(max(abs(X(ab,cd)).^2));
        GRD = sum(sum(abs(X(ab,cd)).^2));
        NBR = sum(sum(abs(X(AB,CD)).^2));
        
        % integrated energy in each region (remove overlap)
        int_nbr = NBR - GRD;
        int_cut = CUT;
        int_grd = GRD - CUT;
        
        % decision (if CUT exceeds threshold and is max)
        if int_cut > (int_nbr * alpha) && int_cut == GRD_MAX
            decision = 1; % detection
        else
            decision = 0; % no detection
        end
        
        % hard decision
        Y(m,n) = decision;
        
        % soft decision
        Z(m,n) = int_cut / (int_nbr + 1e-9); % avoid divide by 0 (sorry)
        
        % for plotting only
        if show
            XX = abs(X).^2;
            XX = XX ./ max(max(XX)) * 255;
            XX(AB,CD) = 255/2;
            XX(ab,cd) = 255/4;
            XX(idx_cut_y,idx_cut_x) = 255/8;
            figure(999);
            imagesc(XX);
            % title('Detector: cell-based detection 2D');
            if debug % to step through
                input('Continue: ');
            end
        end
    end
end
end