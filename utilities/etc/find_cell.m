function [idx,idx_logical] = find_cell(a,b)

if iscell(a)
    C = a; % C is the cell to search through
    obj = b; % obj is the object to find
elseif iscell(b)
    C = b;
    obj = a;
else
    error('Incorrect input types.');
end

[M,N] = size(C);
idx_logical = zeros(M,N);
idx = [];
for i = 1:M
    for j = 1:N
        if C{i,j} == obj
            idx_logical(i,j) = 1;
            idx = [idx, (i-1)*N + j];
        end
    end
end

idx_logical = logical(idx_logical);

end