function [val,r,c] = min2d(A)

[val,idx] = min(A(:));
[r,c] = ind2sub(size(A),idx);

end