function Y = constrain(X,lower,upper)
% CONSTRAIN Constrains the elements of a matrix (or scalar or vector) to be
% within a lower bound and an upper bound.
%
% Usage:
%  Y = CONSTRAIN(X,lower,upper)
%
% Args:
%  X: input matrix to be constrained; if elements of X are complex, their
%     magnitude is used
%  lower: lower bound
%  upper: upper bound
% 
% Returns:
%  Y: matrix of the same size of X whose elements are bounded by
%     [lower,upper]

if ~isreal(X)
    X = abs(X);
end

if false
[M,N] = size(X);
Y = zeros(M,N);

for m = 1:M
    for n = 1:N
        Y(m,n) = min(max(X(m,n),lower),upper);
    end
end
end

X(X<lower) = lower;
X(X>upper) = upper;

Y = X;

return
