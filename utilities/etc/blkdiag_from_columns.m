function B = blkdiag_from_columns(A)
% BLKDIAG_FROM_COLUMNS Returns a matrix that is in a block diagonal form,
% constructed from the columms of the input matrix.
%
% Usage:
%  B = BLKDIAG_FROM_COLUMNS(A)
%
% Args:
%  A: a matrix whose columns are used to generate a block diagonal matrix
%
% Returns:
%  B: a block diagonal matrix constructed from the columns of A
% 
% Notes:
%  If A is M-by-N, B is M*N-by-N.

[M,N] = size(A);

B = zeros(M*N,N);
for n = 1:N
    start = (n-1) * M + 1;
    stop = start + M - 1;
    B(start:stop,n) = A(:,n);
end

end