function [val,r,c] = max2d(A)

[val,idx] = max(A(:));
[r,c] = ind2sub(size(A),idx);

end