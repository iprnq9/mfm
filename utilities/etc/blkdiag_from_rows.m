function B = blkdiag_from_rows(A)
% BLKDIAG_FROM_ROWS Returns a matrix that is in a block diagonal form,
% constructed from the rows of the input matrix.
%
% Usage:
%  B = BLKDIAG_FROM_ROWS(A)
%
% Args:
%  A: a matrix whose rows are used to generate a block diagonal matrix
%
% Returns:
%  B: a block diagonal matrix constructed from the rows of A
% 
% Notes:
%  If A is M-by-N, B is M-by-M*N.

A = A';

B = blkdiag_from_columns(A);

B = B';

end