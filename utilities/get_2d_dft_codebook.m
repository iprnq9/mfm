function [A,D] = get_2d_dft_codebook(M,N)

% this thing is not right i think

% M: number of rows in array
% N: number of columns in array

A = zeros(M*N,M*N);
B = zeros(M,N);
D = zeros(2,M*N);

for u = 0:M-1
    for v = 0:N-1
        for i = 0:M-1
            for j = 0:N-1
                B(i+1,j+1) = exp(-1j*2*pi*(u*i/M+v*j/N));
            end
        end
        A(:,u*M+v+1) = B(:);
        D(:,u*M+v+1) = [u*2*pi/M; v*2*pi/N];
    end
end

return