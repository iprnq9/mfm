function sweep_beams(a,A,delay)

if nargin < 3 || isempty(delay)
    delay = 1;
end

[Na,M] = size(A);

for i = 1:M
    w = A(:,i);
    a.set_weights(w);
    if i == 1
        a.show_polar_array_pattern_azimuth();
    else
        a.show_polar_array_pattern_azimuth(ax);
    end
    ax = gca;
    pause(delay);
end

return