function F = get_mmse_precoder(des,int,snr_des,snr_int,Ns)

[M,N] = size(des);

I = eye(N);

X = inv(des' * des + snr_int/snr_des * int' * int + Ns / snr_des * I) * des';

F = X(:,1:Ns);

return