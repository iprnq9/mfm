function [A,theta] = get_dft_codebook(Na,Nrf,half)

if nargin < 3 || isempty(half)
    half = false;
end

A = dftmtx(Na) ./ sqrt(Na) ./ sqrt(Nrf);

% assumes broadside is 0 degrees
theta = asin((0:Na/2).*2/Na); % first half (0 to +90)

if half
    A = A(:,1:Na/2); % only take first half of matrix
    theta = theta(1:end-1);
else
    theta = [theta -fliplr(theta(2:end-1))]; % second half (-89.99 to -0.01)
end

theta = theta.'; % return column

return
