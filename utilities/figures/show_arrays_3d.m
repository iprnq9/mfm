function show_arrays_3d(arrays)

fig = figure();
ax = axes();

for idx_array = 1:length(arrays)
    a = arrays(idx_array);
    a.show_3d(ax);
    hold(ax,'on');
end

hold(ax,'off');

end