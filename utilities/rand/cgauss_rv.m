function y = cgauss_rv(mu,var,m,n)
% CGAUSS_RV Complex Gaussian Random Variable
%
% Args:
%  mu: mean
%  var: variance
%  m: number of rows in generated r.v. (optional, default = 1)
%  n: number of cols in generated r.v. (optional, default = m)
%
% Returns:
%  y: (m by n) r.v. with complex Gaussian distribution given mean and
%  variance

if nargin < 3
    m = 1; % m is optional
end

if nargin < 4
    n = m; % n is optional, square matrix (m by m) if n not specified
end

y =  1/sqrt(2) * ((mu + sqrt(var) * randn([m,n])) + 1j * (mu + sqrt(var) * randn([m,n])));