function Y = uniformi_rv(a,b,m,n)
% UNIFORMI_RV Returns a matrix whose elements are i.i.d. random integers 
% drawn uniformly over some inclusive range.
%
% Usage:
%  Y = UNIFORMI_RV(a,b)
%  Y = UNIFORMI_RV(a,b,m)
%  Y = UNIFORMI_RV(a,b,m,n)
%
% Args:
%  a: lower limit of uniform r.v. (integer)
%  b: upper limit of uniform r.v. (integer)
%  m: number of rows in generated r.v. (optional, default = 1)
%  n: number of cols in generated r.v. (optional, default = m)
%
% Returns:
%  Y: (m-by-n) r.v. with uniform distribution over [a,b]

if nargin < 2
    error('Specify a lower limit and upper limit.');
end

if nargin < 3
    m = 1; % m is optional
end

if nargin < 4
    n = m; % n is optional, square matrix (m by m) if n not specified
end

Y = randi([a,b],[m,n]);
