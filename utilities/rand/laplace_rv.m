function Y  = laplace_rv(mu,sigma,m,n)
% LAPLACE_RV Generates a matrix of i.i.d. random variables from a Laplacian
% distribution.
%
% Usage:
%  Y = LAPLACE_RV(mu,sigma)
%  Y = LAPLACE_RV(mu,sigma,m)
%  Y = LAPLACE_RV(mu,sigma,m,n)
% 
% Args:
%  mu: mean of the Laplacian distribution
%  sigma: standard deviation of the Laplacian distribution
%  m: number of rows in generated r.v. (optional, default = 1)
%  n: number of cols in generated r.v. (optional, default = m)
%
% Returns:
%  Y: (m-by-n) matrix of elements who are i.i.d. Laplacian r.v.

if nargin < 2
    error('Specify the mean and standard deviation.');
end

if nargin < 3
    m = 1; % m is optional
end

if nargin < 4
    n = m; % n is optional, square matrix (m by m) if n not specified
end

u = rand(m,n) - 0.5;
b = sigma / sqrt(2);
Y = mu - b * sign(u).* log(1- 2* abs(u));