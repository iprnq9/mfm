function B = decrease_rank(A,N)

% Decreases matrix A's rank by dropping the N smallest nonzero singular
% values.

[U,S,V] = svd(A);

S = diag(S);
ss = S(S > 0);

ss(end-N+1:end) = 0;

S(S > 0) = ss;

B = U * diag(S) * V';

return