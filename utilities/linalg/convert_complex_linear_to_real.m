function [C,d,lb,ub] = convert_complex_linear_to_real(A,b,lb,ub)
% CONVERT_COMPLEX_LINEAR_TO_REAL Converts a complex linear equation to a
% real one.
%
% (A + jB) * (x + jy) = (s + jt)
%
% +-     -+ +- -+   +- -+
% | A  -B | | x | = | s |
% |       | | y |   |   |
% | B   A | +- -+   | t |
% +-     -+         +- -+

C = [real(A) -imag(A); imag(A) real(A);];
d = [real(b); imag(b);];

if nargin > 2
    lb = [lb; lb;];
    ub = [ub; ub;];
else
    lb = [];
    ub = [];
end