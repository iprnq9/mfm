function P = get_projection_matrix(M)

P = M * inv(M' * M) * M';

end