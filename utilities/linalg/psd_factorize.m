function [G] = psd_factorize(A)

if any(eig(A) < -1e-15)
    warning('A is not positive semidefinite.');
    disp(['Min eig: ' num2str(min(eig(A)))]);
end

if true
    [U,D] = eig(A);
    D = real(D);
    D(D < 0) = 0;
    G = U * sqrt(D);
    W = G';
else
    [U,S,V] = svd(A);
    G = U * sqrt(S) * eye(size(U));
    % W = G';
end

return