function V = my_vandermonde(x,n)

x = x(:);
m = length(x);

V = zeros(m,n);
for j = 1:n
    V(:,j) = x.^(j-1);
end

return