function [F,W] = get_eigen_beamformers(H,Ns)

[U,~,V] = svd(H);
F = V(:,1:Ns);
W = U(:,1:Ns);

return