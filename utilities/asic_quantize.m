function C = asic_quantize(H,bits)

if bits == 0
    C = H .* 0;
else
    % normalize to max real/imag value
    HH = [real(H); imag(H);];
    max_val = max(max(HH));
    H = H ./ max_val;

    % quantize elementwise
    C = H .* 2^(bits - 1);
    C = round(C);
    C = C ./ 2^(bits - 1);
    C = C .* max_val;
end

return