function R = calculate_mimo_gaussian_mutual_information_no_interference(H,F,W,Rs,Rn)
% CALCULATE_MIMO_GAUSSIAN_MUTUAL_INFORMATION Returns the MIMO mutual information
% based on a channel, precoder, combiner, symbol covariance, noise
% covariance, and interference channel.
%
% Usage:
%  R = CALCULATE_MIMO_GAUSSIAN_MUTUAL_INFORMATION(H,F,W,Rs,Rn,Q)
%
% Args:
%  H: channel matrix between a transmitter and receiver
%  F: precoding matrix at the transmitter
%  W: combining matrix at the receiver
%  Rs: covariance matrix of the circularly symmetric Gaussian distribution
%      from which the symbol vector was drawn (assuming zero-mean)
%  Rn: covariance matrix of the circularly symmetric Gaussian distribution 
%      from which the noise vector was drawn (assuming zero-mean)
%
% Returns:
%  R: the MIMO Gaussian mutual information (capacity) based on the input
%     arguments, assuming interference as noise (worst case) (bps/Hz)
%
% Notes:
%  R = log2 det [ I  + W' H F Rs F' H' W (W' Rn W + W' Q Q' W)^(-1) ]

[Nr_H,Nt_H] = size(H);
[Nt_F,Ns_F] = size(F);
[Nr_W,Ns_W] = size(W);
[Ns_Rs,~] = size(Rs);
[Nr_Rn,~] = size(Rn);

if ~(Nt_H == Nt_F) || ~(Nr_H == Nr_W)
    error('Number of transmit and receive antenna dimensions must match.');
else
    Nt = Nt_H;
    Nr = Nr_H;
end

if ~(Ns_F == Ns_W)
    error('Number of streams in precoder and combiner do not match.');
else
    Ns = Ns_F;
end

if ~(Ns == Ns_Rs)
    error('Number of streams does not match size of symbol covariance matrix.');
end

if ~(Nr == Nr_Rn)
    error('Number of receive antennas does not match size of noise covariance matrix.');
end

I = eye(Ns);

R = log2(det(I + W' * H * F * Rs * F' * H' * W * inv(W' * Rn * W)));
R = real(R); % take real in case there is a residual imaginary component

return