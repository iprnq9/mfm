function w = waterfilling(H,P,SNR,Ns)

% !!! returns power weights! need to take square root for precoder !!!

if nargin < 2 || isempty(P) % power budget
    P = 1; 
end

if nargin < 3 || isempty(SNR) % SNR
    SNR = 1; 
end

if nargin < 4 || isempty(Ns) % number of streams
    Ns = rank(H);
end

S = svd(H);
Ns_orig = Ns;

cont = true;
while cont
    gamma = S(1:Ns).^2 * SNR;
    tmp = sum(1./gamma);
    cutoff = Ns / (1 + tmp);
    % disp(['Cutoff: ' num2str(cutoff) ', min(gamma): ' num2str(min(gamma))]);
    if cutoff > min(gamma)
        cont = true;
        Ns = Ns - 1;
        % disp('Reducing number of streams.');
        % disp(['Cutoff: ' num2str(cutoff) ', min(gamma): ' num2str(min(gamma))]);
    else
        cont = false;
        p = P .* (1/cutoff - 1./gamma);
    end
end

% return zero for disabled streams
w = zeros(Ns_orig,1);
w(1:Ns) = p;

% p = 0;

return


