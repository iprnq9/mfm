function a = combine_arrays(arrays)

a = array();

num_arrays = length(arrays);

for i = 1:num_arrays
    b = arrays(i);
    a.add_element(b.x,b.y,b.z);
end

return