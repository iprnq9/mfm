function W = get_mmse_combiner(des,int,snr_des,snr_int,Ns)

[M,N] = size(des);

I = eye(M);

X = inv(des * des' + snr_int/snr_des * int * int' + Ns / snr_des * I) * des;

W = X(:,1:Ns);

return