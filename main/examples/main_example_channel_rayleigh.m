% Script: MAIN_EXAMPLE_CHANNEL_RAYLEIGH.M
% Summary: Example setup and realization of a Rayleigh-faded channel 
% object.
% By: Ian P. Roberts, MIMO for MATLAB (MFM)
% Date: 1-12-2021

% -------------------------------------------------------------------------
% Setup.
% -------------------------------------------------------------------------
clc; clearvars; close all;

% -------------------------------------------------------------------------
% Create ray/cluster channel object.
% -------------------------------------------------------------------------
c = channel.create('Rayleigh');

% -------------------------------------------------------------------------
% Set propagation velocity.
% -------------------------------------------------------------------------
vel = 3e8;
c.set_propagation_velocity(vel);

% -------------------------------------------------------------------------
% Set carrier frequency (also sets wavelength).
% -------------------------------------------------------------------------
fc = 2.4e9;
c.set_carrier_frequency(fc);

% -------------------------------------------------------------------------
% Create a transmit array and receive array for channel input and output.
% -------------------------------------------------------------------------
array_transmit = array.create(8);
array_receive = array.create(4);

% -------------------------------------------------------------------------
% Set transmit and receive arrays.
% -------------------------------------------------------------------------
c.set_array_transmit(array_transmit);
c.set_array_receive(array_receive);

% -------------------------------------------------------------------------
% Optional: force channel matrix to normalize its energy.
% -------------------------------------------------------------------------
% c.set_force_channel_energy_normalization(true);
% c.set_normalized_channel_energy(1);

% -------------------------------------------------------------------------
% Invoke a realization of the channel.
% -------------------------------------------------------------------------
H = c.realization();
