% Script: MAIN_EXAMPLE_CHANNEL_RAY_CLUSTER.M
% Summary: Example setup and realization of a ray/cluster channel.
% By: Ian P. Roberts, MIMO for MATLAB (MFM)
% Date: 1-04-2021

% -------------------------------------------------------------------------
% Setup.
% -------------------------------------------------------------------------
clc; clearvars; close all;

% -------------------------------------------------------------------------
% Create ray/cluster channel object.
% -------------------------------------------------------------------------
c = channel.create('ray-cluster');

% -------------------------------------------------------------------------
% Set propagation velocity.
% -------------------------------------------------------------------------
vel = 3e8;
c.set_propagation_velocity(vel);

% -------------------------------------------------------------------------
% Set carrier frequency (also sets wavelength).
% -------------------------------------------------------------------------
fc = 2.4e9;
c.set_carrier_frequency(fc);

% -------------------------------------------------------------------------
% Create a transmit array and receive array for channel input and output.
% -------------------------------------------------------------------------
array_transmit = array.create(8,8);
array_receive = array.create(4,4);

% -------------------------------------------------------------------------
% Set transmit and receive arrays.
% -------------------------------------------------------------------------
c.set_array_transmit(array_transmit);
c.set_array_receive(array_receive);

% -------------------------------------------------------------------------
% Optional: force channel matrix to normalize its energy.
% -------------------------------------------------------------------------
c.set_force_channel_energy_normalization(true);

% -------------------------------------------------------------------------
% Set ray gains to be drawn from a complex Normal distribution.
% -------------------------------------------------------------------------
ray_gain_opt.type = 'cgauss';
% ray_gain_opt.type = 'unit';
c.set_ray_gain_distribution(ray_gain_opt);

% -------------------------------------------------------------------------
% Set ray angles to be drawn from a Laplacian distribution.
% -------------------------------------------------------------------------
ray_angle_opt.type = 'laplacian';
ray_angle_opt.std_dev = [0.1,0.1];
% ray_angle_opt.type = 'gaussian';
% ray_angle_opt.variance = [0.1,0.1];
c.set_ray_angle_distribution(ray_angle_opt);

% -------------------------------------------------------------------------
% Set bounds on rays' AoDs.
% -------------------------------------------------------------------------
az_min_AoD = -pi/2;
az_max_AoD = pi/2;
el_min_AoD = -pi/2;
el_max_AoD = pi/2;
c.set_AoD_range(az_min_AoD,az_max_AoD,el_min_AoD,el_max_AoD);

% -------------------------------------------------------------------------
% Set bounds on rays' AoAs.
% -------------------------------------------------------------------------
az_min_AoA = -pi/2;
az_max_AoA = pi/2;
el_min_AoA = -pi/2;
el_max_AoA = pi/2;
c.set_AoA_range(az_min_AoA,az_max_AoA,el_min_AoA,el_max_AoA);

% -------------------------------------------------------------------------
% Set a bound on the number of clusters.
% -------------------------------------------------------------------------
num_clusters_range = [1,10];
c.set_num_clusters_range(num_clusters_range);

% -------------------------------------------------------------------------
% Set a bound on the number of rays per cluster.
% -------------------------------------------------------------------------
num_rays_per_cluster_range = [1,10];
c.set_num_rays_per_cluster_range(num_rays_per_cluster_range);

% -------------------------------------------------------------------------
% Invoke a realization of the channel.
% -------------------------------------------------------------------------
H = c.realization();

% -------------------------------------------------------------------------
% View the beamspace.
% -------------------------------------------------------------------------
c.show_beamspace_azimuth();
c.show_beamspace_elevation();