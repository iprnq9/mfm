% Script: MAIN_EXAMPLE_CHANNEL.M
% Summary: Example setup of a generic channel object.
% By: Ian P. Roberts, MIMO for MATLAB (MFM)
% Date: 1-05-2021

% -------------------------------------------------------------------------
% Setup.
% -------------------------------------------------------------------------
clc; clearvars; close all;

% -------------------------------------------------------------------------
% Create generic channel object.
% -------------------------------------------------------------------------
c = channel.create();

% -------------------------------------------------------------------------
% Set propagation velocity.
% -------------------------------------------------------------------------
vel = 3e8;
c.set_propagation_velocity(vel);

% -------------------------------------------------------------------------
% Set carrier frequency (also sets wavelength).
% -------------------------------------------------------------------------
fc = 2.4e9;
c.set_carrier_frequency(fc);

% -------------------------------------------------------------------------
% Create a transmit array and receive array for channel input and output.
% -------------------------------------------------------------------------
array_transmit = array.create(8);
array_receive = array.create(4);

% -------------------------------------------------------------------------
% Set transmit and receive arrays.
% -------------------------------------------------------------------------
c.set_array_transmit(array_transmit);
c.set_array_receive(array_receive);

% -------------------------------------------------------------------------
% Optional: force channel matrix to normalize its energy to desired value.
% -------------------------------------------------------------------------
% c.set_force_channel_energy_normalization(true);
% c.set_normalized_channel_energy(val);