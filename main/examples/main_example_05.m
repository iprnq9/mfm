% Script: MAIN_EXAMPLE_05.M
% Summary: Demonstrates how MFM's free space path loss object can be used.
% By: Ian P. Roberts, MIMO for MATLAB (MFM)
% Date: 01-14-2020

% -------------------------------------------------------------------------
% Setup.
% -------------------------------------------------------------------------
clc; clearvars; close all;
rng(99);

% -------------------------------------------------------------------------
% System variables.
% -------------------------------------------------------------------------
carrier_frequency_Hz = 5e9;
propagation_velocity_meters_per_sec = 3e8;

% -------------------------------------------------------------------------
% Path loss exponent.
% -------------------------------------------------------------------------
path_loss_exponent = 2;

% -------------------------------------------------------------------------
% Create free-space path loss object.
% -------------------------------------------------------------------------
path_loss_object = path_loss.create('free-space');
path_loss_object.set_path_loss_exponent(path_loss_exponent);

% -------------------------------------------------------------------------
% Setup the path loss object.
% -------------------------------------------------------------------------
path_loss_object.set_propagation_velocity(propagation_velocity_meters_per_sec);
path_loss_object.set_carrier_frequency(carrier_frequency_Hz);

% -------------------------------------------------------------------------
% Set the distance of the path.
% -------------------------------------------------------------------------
distance = 100; % meters
path_loss_object.set_distance(distance);

% -------------------------------------------------------------------------
% Invoke a realization of the path loss object.
% -------------------------------------------------------------------------
L = path_loss_object.realization()

% -------------------------------------------------------------------------
% Fetch the path loss (attenuation). This is equivalent to the path loss
% returned when invoking a realization.
% -------------------------------------------------------------------------
L = path_loss_object.get_attenuation() % power loss (linear)

% -------------------------------------------------------------------------
% Distance can also be a vector, as demonstrated here comparing PLEs.
% -------------------------------------------------------------------------
distance = 10.^([0:1:4]); % meters
path_loss_object.set_distance(distance);
L_2 = path_loss_object.realization(); % power loss (linear)

path_loss_object.set_path_loss_exponent(3);
L_3 = path_loss_object.realization(); % power loss (linear)

path_loss_object.set_path_loss_exponent(4);
L_4 = path_loss_object.realization(); % power loss (linear)

figure();
semilogx(distance,10*log10(L_2),'-k'); grid on; hold on;
semilogx(distance,10*log10(L_3),'--r'); grid on;
semilogx(distance,10*log10(L_4),':b'); grid on; hold off;
legend('PLE of 2','PLE of 3','PLE of 4','Location','Northwest');
xlabel('Distance (meters)');
ylabel('Path Loss (dB)');
print('main_example_05','-depsc');