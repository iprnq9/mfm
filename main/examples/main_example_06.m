% Script: MAIN_EXAMPLE_06.M
% Summary: Demonstrates usage of the two-slope path loss object.
% By: Ian P. Roberts, MIMO for MATLAB (MFM)
% Date: 01-14-2020

% -------------------------------------------------------------------------
% Setup.
% -------------------------------------------------------------------------
clc; clearvars; close all;

% -------------------------------------------------------------------------
% Create a two-slope path loss object.
% -------------------------------------------------------------------------
p = path_loss_two_slope();

% -------------------------------------------------------------------------
% Set up the two-slope path loss object.
% -------------------------------------------------------------------------
p.set_path_loss_exponents(2,4);
p.set_reference_path_loss(80,'dB');
p.set_reference_distance(10);

% -------------------------------------------------------------------------
% Distances to evaluate the path loss model at.
% -------------------------------------------------------------------------
d = 10.^([0:1:3]);

% -------------------------------------------------------------------------
% Evaluate the two-slope path loss at our distances of interest.
% -------------------------------------------------------------------------
L = p.compute_path_loss_attenuation(d);

% -------------------------------------------------------------------------
% Plot the results.
%  * Note that at the reference distance, our reference path loss is
%  achieved by design.
% -------------------------------------------------------------------------
figure();
semilogx(d,-10*log10(L),'-k');
grid on;
xlabel('Distance (meters)');
ylabel('Inverse Path Loss (dB)');
print('path_loss_two_slope','-depsc');