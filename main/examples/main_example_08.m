% Script: MAIN_EXAMPLE_08.M
% Summary: Simulates a network of two links to demonstrate the MFM's
% ability to automatically compute the interference that arises between
% links. Reports the achieved mutual information and symbol estimation
% error on each link for a variety of receive strategies.
% By: Ian P. Roberts, MIMO for MATLAB (MFM)
% Date:1-20-2020

% -------------------------------------------------------------------------
% Setup.
% -------------------------------------------------------------------------
clc; clearvars; close all;
rng(99);

% -------------------------------------------------------------------------
% System variables.
% -------------------------------------------------------------------------
symbol_bandwidth_Hz = 50e6;
carrier_frequency_Hz = 5e9;
propagation_velocity_meters_per_sec = 3e8;
noise_power_per_Hz_dBm_Hz = -174;
num_streams = 4;

% -------------------------------------------------------------------------
% Create some arrays to use.
% -------------------------------------------------------------------------
Nt = 4;
Nr = 8;
array_transmit_object = array.create(Nt);
array_receive_object = array.create(Nr);

% -------------------------------------------------------------------------
% Create a default device.
% -------------------------------------------------------------------------
transmit_power_dBm = 0;
device_object = device.create('transceiver','digital');
device_object.set_transmit_power(transmit_power_dBm,'dBm');
device_object.set_arrays(array_transmit_object,array_receive_object);

% -------------------------------------------------------------------------
% Channel object.
% -------------------------------------------------------------------------
channel_object = channel.create('Rayleigh');

% -------------------------------------------------------------------------
% Path loss object.
% -------------------------------------------------------------------------
path_loss_object = path_loss.create('free-space');
path_loss_object.set_path_loss_exponent(2);

% -------------------------------------------------------------------------
% Create our device instances.
% -------------------------------------------------------------------------
dev_1 = copy_object(device_object);
dev_1.set_name('Tx-1');
dev_1.set_coordinate([0,0,0]);
dev_1.set_marker('bx');

dev_2 = copy_object(device_object);
dev_2.set_name('Rx-1');
dev_2.set_coordinate([0,100,0]);
dev_2.set_marker('bo');

dev_3 = copy_object(device_object);
dev_3.set_name('Tx-2');
dev_3.set_coordinate([100,0,0]);
dev_3.set_marker('mx');

dev_4 = copy_object(device_object);
dev_4.set_name('Rx-2');
dev_4.set_coordinate([100,100,0]);
dev_4.set_marker('mo');

% -------------------------------------------------------------------------
% Create network.
% -------------------------------------------------------------------------
net = network_mfm();

% -------------------------------------------------------------------------
% Add devices to network as source-destination pairs.
% -------------------------------------------------------------------------
net.add_source_destination(dev_1,dev_2);
net.add_source_destination(dev_3,dev_4);

% -------------------------------------------------------------------------
% Automatically populate links based on source-destination pairs.
% -------------------------------------------------------------------------
net.populate_links_from_source_destination();

% -------------------------------------------------------------------------
% Set network-wide channel and path loss models.
% -------------------------------------------------------------------------
net.set_path_loss(path_loss_object);
net.set_channel(channel_object);

% -------------------------------------------------------------------------
% Finish network setup.
% -------------------------------------------------------------------------
net.set_symbol_bandwidth(symbol_bandwidth_Hz);
net.set_propagation_velocity(propagation_velocity_meters_per_sec);
net.set_carrier_frequency(carrier_frequency_Hz);
net.set_num_streams(num_streams);
net.set_transmit_power(transmit_power_dBm,'dBm');
net.set_noise_power_per_Hz(noise_power_per_Hz_dBm_Hz,'dBm_Hz');

% -------------------------------------------------------------------------
% View the network.
% -------------------------------------------------------------------------
net.show_2d();

% -------------------------------------------------------------------------
% Invoke a realization of the entire network.
% -------------------------------------------------------------------------
net.realization();

% -------------------------------------------------------------------------
% Compute and supply channel state information to each device.
% -------------------------------------------------------------------------
net.compute_channel_state_information();
net.supply_channel_state_information();

% -------------------------------------------------------------------------
% Configure both transmitters.
% -------------------------------------------------------------------------
net.configure_transmitter('eigen');

% -------------------------------------------------------------------------
% Vary receive strategy.
% -------------------------------------------------------------------------
receive_strategy_list = {'eigen','mmse','mmse-int'};

for idx_receive_strategy = 1:length(receive_strategy_list)
    % -------------------------------------------------------------------------
    % Configure both receivers.
    % -------------------------------------------------------------------------
    receive_strategy = receive_strategy_list{idx_receive_strategy};
    disp(['Receive strategy: ' receive_strategy]);
    net.configure_receiver(receive_strategy);
    
    % -------------------------------------------------------------------------
    % Compute received signals based on realization and configuration.
    % -------------------------------------------------------------------------
    net.compute_received_signals();
    
    % -------------------------------------------------------------------------
    % Report mutual information of the two desired links.
    % -------------------------------------------------------------------------
    mi_12 = net.report_mutual_information(dev_1,dev_2);
    mi_34 = net.report_mutual_information(dev_3,dev_4);
    
    disp(['M.I. (1) --> (2): ' num2str(mi_12) ' bps/Hz']);
    disp(['M.I. (3) --> (4): ' num2str(mi_34) ' bps/Hz']);
    
    % -------------------------------------------------------------------------
    % Report symbol estimation error of the two desired links.
    % -------------------------------------------------------------------------
    [err_12,nerr_12] = net.report_symbol_estimation_error(dev_1,dev_2);
    [err_34,nerr_34] = net.report_symbol_estimation_error(dev_3,dev_4);
    
    disp(['Norm. symb. est. error: (1) --> (2): ' num2str(10*log10(nerr_12)) ' dB']);
    disp(['Norm. symb. est. error: (3) --> (4): ' num2str(10*log10(nerr_34)) ' dB']);
end