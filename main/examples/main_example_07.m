% Script: MAIN_EXAMPLE_04.M
% Summary: Demonstrates how MFM can be used at the device/link-level.
% By: Ian P. Roberts, MIMO for MATLAB (MFM)
% Date: 01-19-2020

% -------------------------------------------------------------------------
% Setup.
% -------------------------------------------------------------------------
clc; clearvars; close all;
rng(99);

% -------------------------------------------------------------------------
% System variables.
% -------------------------------------------------------------------------
symbol_bandwidth_Hz = 50e6;
carrier_frequency_Hz = 5e9;
propagation_velocity_meters_per_sec = 3e8;
noise_power_per_Hz_dBm_Hz = -174;
num_streams = 4;
transmit_power_dBm = 0;

% -------------------------------------------------------------------------
% Channel object.
% -------------------------------------------------------------------------
channel_object = channel.create('Rayleigh');

% -------------------------------------------------------------------------
% Path loss object.
% -------------------------------------------------------------------------
path_loss_object = path_loss.create('free-space');
path_loss_object.set_path_loss_exponent(3.5);

% -------------------------------------------------------------------------
% Create transmitting device.
%  * Fully-digital transmitter (as opposed to hybrid digital/analog)
% -------------------------------------------------------------------------
dev_tx = device.create('transmitter','digital');
dev_tx.set_symbol_bandwidth(symbol_bandwidth_Hz);
dev_tx.set_num_streams(num_streams);
dev_tx.set_transmit_power(transmit_power_dBm,'dBm');
dev_tx.set_name('Tx-1');
dev_tx.set_coordinate([0,0,0]);
dev_tx.set_marker('bx');

% -------------------------------------------------------------------------
% Create and set transmit array.
%  * Using a uniform linear array for simplicity here.
% -------------------------------------------------------------------------
Nt = 4;
array_transmit_object = array.create(Nt);
dev_tx.set_transmit_array(array_transmit_object);

% -------------------------------------------------------------------------
% Create receiving device.
%  * Fully-digital receiver (as opposed to hybrid digital/analog)
% -------------------------------------------------------------------------
dev_rx = device.create('receiver','digital');

% -------------------------------------------------------------------------
% Setup receiver.
% -------------------------------------------------------------------------
dev_rx.set_symbol_bandwidth(symbol_bandwidth_Hz);
dev_rx.set_num_streams(num_streams);
dev_rx.set_noise_power_per_Hz(noise_power_per_Hz_dBm_Hz,'dBm_Hz');
dev_rx.set_name('Rx-1');
dev_rx.set_coordinate([0,10,0]);
dev_rx.set_marker('bo');

% -------------------------------------------------------------------------
% Create and set transmit array.
%  * Using a uniform linear array for simplicity here.
% -------------------------------------------------------------------------
Nr = 4;
array_receive_object = array.create(Nr);
dev_rx.set_receive_array(array_receive_object);

% -------------------------------------------------------------------------
% Create link between the two devices.
%  * First device is transmitter, second device is receiver.
%  * Some properties of the link, such as its distance is automatically
%    computed based on the two devices.
% -------------------------------------------------------------------------
lnk = link(dev_tx,dev_rx);

% -------------------------------------------------------------------------
% Set channel and path loss models to use on the link.
% -------------------------------------------------------------------------
lnk.set_path_loss(path_loss_object);
lnk.set_channel(channel_object);

% -------------------------------------------------------------------------
% Link setup.
%  * Sets these properties on the channel and path loss models also.
% -------------------------------------------------------------------------
lnk.set_propagation_velocity(propagation_velocity_meters_per_sec);
lnk.set_carrier_frequency(carrier_frequency_Hz);

% -------------------------------------------------------------------------
% View the link.
%  * lnk.show_2d() or lnk.show_3d()
% -------------------------------------------------------------------------
% lnk.show_2d();
% lnk.show_3d();

% -------------------------------------------------------------------------
% Monte Carlo.
% -------------------------------------------------------------------------
snr_dB_list = [-30:10:20];
num_realizations = 500;
mi = zeros(length(snr_dB_list),num_realizations);

for idx_snr = 1:length(snr_dB_list)
    snr_dB = snr_dB_list(idx_snr);
    for idx_realization = 1:num_realizations
        % -------------------------------------------------------------------------
        % Invoke a realization of the link.
        % -------------------------------------------------------------------------
        lnk.realization_channel();
        lnk.set_snr(snr_dB,[],'dB');

        % -------------------------------------------------------------------------
        % Fetch the channel state information of the link.
        % -------------------------------------------------------------------------
        [csi_fwd,~] = lnk.compute_channel_state_information();
        lnk.head.transmitter.set_channel_state_information(csi_fwd);
        lnk.tail.receiver.set_channel_state_information(csi_fwd);
        dev_tx.set_destination(dev_rx);
        dev_rx.set_source(dev_tx);
        
        % -------------------------------------------------------------------------
        % Configure transmitter and receiver using CSI.
        % -------------------------------------------------------------------------
        dev_tx.configure_transmitter('eigen');
        dev_rx.configure_receiver('eigen');
        
        % -------------------------------------------------------------------------
        % Report mutual information.
        % -------------------------------------------------------------------------
        mi(idx_snr,idx_realization) = lnk.report_mutual_information_forward();
    end
end

% -------------------------------------------------------------------------
% Average over all realizations per SNR.
% -------------------------------------------------------------------------
mi_mean = mean(mi,2,'omitnan');

% -------------------------------------------------------------------------
% Results.
% -------------------------------------------------------------------------
figure();
plot(snr_dB_list,mi_mean,'-k*'); grid on;
xlabel('SNR (dB)');
ylabel('Spectral Efficiency (bps/Hz)');