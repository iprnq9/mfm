% Script: MAIN_EXAMPLE_10.M
% Summary: Demonstrates how MFM can automatically compute the array
% response of an arbitrary antenna array.
% By: Ian P. Roberts, MIMO for MATLAB (MFM)
% Date: 01-20-2020

% -------------------------------------------------------------------------
% Setup.
% -------------------------------------------------------------------------
clc; clearvars; close all;
% rng(99);

% -------------------------------------------------------------------------
% Create an empty array.
% -------------------------------------------------------------------------
a = array.create();

% -------------------------------------------------------------------------
% Place elements randomly.
% -------------------------------------------------------------------------
num_elements = 16;
for idx = 1:num_elements
    x = gauss_rv(0,1);
    y = gauss_rv(0,1) * 0;
    z = gauss_rv(0,1);
    a.add_element(x,y,z);
end

% -------------------------------------------------------------------------
% View array.
% -------------------------------------------------------------------------
a.show_3d();

% -------------------------------------------------------------------------
% Get the array response at some angle.
% -------------------------------------------------------------------------
az = pi/6;
el = pi/6;
v = a.get_array_response(az,el)

% -------------------------------------------------------------------------
% Show array pattern.
% -------------------------------------------------------------------------
a.show_array_pattern();
a.show_polar_array_pattern_azimuth();
a.show_polar_array_pattern_elevation();