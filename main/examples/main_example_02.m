% Script: MAIN_EXAMPLE_02.M
% Summary: Demonstrates how increased distance from an interferer increases
% the mutual information and decreases symbol estimation error.
% By: Ian P. Roberts, MIMO for MATLAB (MFM)
% Date: 01-12-2020

% -------------------------------------------------------------------------
% Setup.
% -------------------------------------------------------------------------
clc; clearvars; close all;
rng(99);

% -------------------------------------------------------------------------
% System variables.
% -------------------------------------------------------------------------
symbol_bandwidth_Hz = 50e6;
carrier_frequency_Hz = 5e9;
propagation_velocity_meters_per_sec = 3e8;
noise_power_per_Hz_dBm_Hz = -174;
num_streams = 4;
transmit_power_dBm = 0;
Nt = 4;
Nr = 8;

% -------------------------------------------------------------------------
% Create some arrays to use.
% -------------------------------------------------------------------------
array_transmit_object = array.create(Nt);
array_receive_object = array.create(Nr);

% -------------------------------------------------------------------------
% Create a default device.
% -------------------------------------------------------------------------
device_object = device.create('transceiver','digital');
device_object.set_transmit_power(transmit_power_dBm,'dBm');
device_object.set_arrays(array_transmit_object,array_receive_object);

% -------------------------------------------------------------------------
% Channel object.
% -------------------------------------------------------------------------
channel_object = channel.create('Rayleigh');

% -------------------------------------------------------------------------
% Path loss object.
% -------------------------------------------------------------------------
path_loss_object = path_loss.create('free-space');
path_loss_object.set_path_loss_exponent(2);

% -------------------------------------------------------------------------
% Create our device instances.
% -------------------------------------------------------------------------
dev_1 = copy_object(device_object);
dev_1.set_name('Tx-1');
dev_1.set_coordinate([0,0,0]);
dev_1.set_marker('bx');

dev_2 = copy_object(device_object);
dev_2.set_name('Rx-1');
dev_2.set_coordinate([0,100,0]);
dev_2.set_marker('bo');

dev_3 = copy_object(device_object);
dev_3.set_name('Tx-2');
dev_3.set_coordinate([100,0,0]); % will change below
dev_3.set_marker('mx');

dev_4 = copy_object(device_object);
dev_4.set_name('Rx-2');
dev_4.set_coordinate([100,100,0]); % will change below
dev_4.set_marker('mo');

% -------------------------------------------------------------------------
% Create network.
% -------------------------------------------------------------------------
net = network_mfm();

% -------------------------------------------------------------------------
% Add devices to network as source-destination pairs.
% -------------------------------------------------------------------------
net.add_source_destination(dev_1,dev_2);
net.add_source_destination(dev_3,dev_4);

% -------------------------------------------------------------------------
% Automatically populate links based on source-destination pairs.
% -------------------------------------------------------------------------
net.populate_links_from_source_destination();

% -------------------------------------------------------------------------
% Set network-wide channel and path loss models.
% -------------------------------------------------------------------------
net.set_path_loss(path_loss_object);
net.set_channel(channel_object);

% -------------------------------------------------------------------------
% Finish network setup.
% -------------------------------------------------------------------------
net.set_symbol_bandwidth(symbol_bandwidth_Hz);
net.set_propagation_velocity(propagation_velocity_meters_per_sec);
net.set_carrier_frequency(carrier_frequency_Hz);
net.set_num_streams(num_streams);
net.set_transmit_power(transmit_power_dBm,'dBm');
net.set_noise_power_per_Hz(noise_power_per_Hz_dBm_Hz,'dBm_Hz');

% -------------------------------------------------------------------------
% View the network.
% -------------------------------------------------------------------------
net.show_3d();

% -------------------------------------------------------------------------
% Loop for each interference link separation distance.
% -------------------------------------------------------------------------
dist_list = (0:10:500); % meters
num_dist = length(dist_list);
mi_12 = zeros(num_dist,1);
mi_34 = zeros(num_dist,1);
nerr_12 = zeros(num_dist,1);
nerr_34 = zeros(num_dist,1);
snr_11 = zeros(num_dist,1);
snr_22 = zeros(num_dist,1);
inr_12 = zeros(num_dist,1);
inr_21 = zeros(num_dist,1);
sinr_11 = zeros(num_dist,1);
sinr_22 = zeros(num_dist,1);

for idx_dist = 1:num_dist
    % -------------------------------------------------------------------------
    % Set distance of interference link.
    % -------------------------------------------------------------------------
    dist = dist_list(idx_dist);
    dev_3.set_coordinate([dist,0,0]);
    dev_4.set_coordinate([dist,100,0]);

    % -------------------------------------------------------------------------
    % Invoke a realization of the entire network.
    % -------------------------------------------------------------------------
    rng(99); % to ensure same channels, etc. on each iteration
    net.realization();
    
    % -------------------------------------------------------------------------
    % Log our large-scale SNRs. SNRs will be constant. INRs will not.
    % -------------------------------------------------------------------------
    snr_11(idx_dist) = net.links{1}.snr_forward; % SNR from Tx-1 onto Rx-1
    snr_22(idx_dist) = net.links{4}.snr_forward; % SNR from Tx-2 onto Rx-2
    inr_21(idx_dist) = net.links{3}.snr_forward; % INR from Tx-2 onto Rx-1
    inr_12(idx_dist) = net.links{2}.snr_forward; % INR from Tx-1 onto Rx-2
    
    % -------------------------------------------------------------------------
    % Compute SINR based on SNR and INR.
    % -------------------------------------------------------------------------
    sinr_11(idx_dist) = snr_11(idx_dist) / (1 + inr_21(idx_dist));
    sinr_22(idx_dist) = snr_22(idx_dist) / (1 + inr_12(idx_dist));
    
    % -------------------------------------------------------------------------
    % Compute and supply channel state information to each device.
    % -------------------------------------------------------------------------
    net.compute_channel_state_information();
    net.supply_channel_state_information();
    
    % -------------------------------------------------------------------------
    % Configure all transmitters and receivers.
    % -------------------------------------------------------------------------
    net.configure_transmitter('eigen');
    net.configure_receiver('mmse');
    
    % -------------------------------------------------------------------------
    % Compute received signals based on realization and configuration.
    % -------------------------------------------------------------------------
    net.compute_received_signals();
    
    % -------------------------------------------------------------------------
    % Report mutual information of the two desired links.
    % -------------------------------------------------------------------------
    mi_12(idx_dist) = net.report_mutual_information(dev_1,dev_2);
    mi_34(idx_dist) = net.report_mutual_information(dev_3,dev_4);
    
    % -------------------------------------------------------------------------
    % Report symbol estimation error of the two desired links.
    % -------------------------------------------------------------------------
    [err_12,nerr_12(idx_dist)] = net.report_symbol_estimation_error(dev_1,dev_2);
    [err_34,nerr_34(idx_dist)] = net.report_symbol_estimation_error(dev_3,dev_4);
end

% -------------------------------------------------------------------------
% Results.
% -------------------------------------------------------------------------
figure();
plot(dist_list,mi_12+mi_34,'-k');
xlabel('Interference link separation distance (meters)');
ylabel('Sum mutual information (bps/Hz)');
grid on;

figure();
plot(dist_list,10*log10(nerr_12+nerr_34),'-k');
xlabel('Interference link separation distance (meters)');
ylabel('Sum normalized symbol estimation error (dB)');
grid on;

figure();
plot(dist_list,10*log10(snr_11),'-bo'); hold on;
plot(dist_list,10*log10(snr_22),'-mx'); hold off;
legend('Tx-1 to Rx-1','Tx-2 to Rx-2','Location','Best');
xlabel('Interference link separation distance (meters)');
ylabel('Large-scale SNR (dB)');
grid on;

figure();
plot(dist_list,10*log10(inr_12),'-ro'); hold on;
plot(dist_list,10*log10(inr_21),'-kx'); hold off;
legend('Tx-1 to Rx-2','Tx-2 to Rx-1','Location','Best');
xlabel('Interference link separation distance (meters)');
ylabel('Large-scale interference-to-noise ratio (dB)');
grid on;

figure();
plot(dist_list,10*log10(sinr_11),'-bo'); hold on;
plot(dist_list,10*log10(sinr_22),'-mx'); hold off;
legend('At Rx-1','At Rx-2','Location','Best');
xlabel('Interference link separation distance (meters)');
ylabel('Large-scale SINR (dB)');
grid on;
