% Script: MAIN_EXAMPLE_03.M
% Summary: Creates a network of many devices to illustrate how MFM can 
% handle the existence of many links within a given network and can 
% automatically compute metrics accounting for interference.
% By: Ian P. Roberts, MIMO for MATLAB (MFM)
% Date: 01-12-2020

% -------------------------------------------------------------------------
% Setup.
% -------------------------------------------------------------------------
clc; clearvars; close all;
rng(99);

% -------------------------------------------------------------------------
% System variables.
% -------------------------------------------------------------------------
symbol_bandwidth_Hz = 50e6;
carrier_frequency_Hz = 5e9;
propagation_velocity_meters_per_sec = 3e8;
noise_power_per_Hz_dBm_Hz = -174;
num_streams = 4;
transmit_power_dBm = 0;
Nt = 4;
Nr = 8;

% -------------------------------------------------------------------------
% Create some template arrays to use.
% -------------------------------------------------------------------------
array_transmit_object = array.create(Nt);
array_receive_object = array.create(Nr);

% -------------------------------------------------------------------------
% Create a template device.
% -------------------------------------------------------------------------
device_object = device.create('transceiver','digital');
device_object.set_transmit_power(transmit_power_dBm,'dBm');
device_object.set_arrays(array_transmit_object,array_receive_object);

% -------------------------------------------------------------------------
% Channel object.
% -------------------------------------------------------------------------
channel_object = channel.create('Rayleigh');

% -------------------------------------------------------------------------
% Path loss object.
% -------------------------------------------------------------------------
path_loss_object = path_loss.create('free-space');
path_loss_object.set_path_loss_exponent(2);

% -------------------------------------------------------------------------
% Create our desired devices.
% -------------------------------------------------------------------------
dev_1 = copy_object(device_object);
dev_1.set_name('Tx-1');
dev_1.set_coordinate([0,0,0]);
dev_1.set_marker('bx');

dev_2 = copy_object(device_object);
dev_2.set_name('Rx-1');
dev_2.set_coordinate([0,100,0]);
dev_2.set_marker('bo');

% -------------------------------------------------------------------------
% Create network.
% -------------------------------------------------------------------------
net = network_mfm();

% -------------------------------------------------------------------------
% Add desired devices to network as source-destination pair.
% -------------------------------------------------------------------------
net.add_source_destination(dev_1,dev_2);

% -------------------------------------------------------------------------
% Create a template device object for our interference devices.
% -------------------------------------------------------------------------
dev = copy_object(device_object);

% -------------------------------------------------------------------------
% Number of interference tx-rx pairs.
% -------------------------------------------------------------------------
num_int = 20;

% -------------------------------------------------------------------------
% Add interference devices to network as source-destination pairs.
% -------------------------------------------------------------------------
sources = cell(num_int,1);
dests = cell(num_int,1);

for idx_int = 1:num_int
    % -------------------------------------------------------------------------
    % Random interference transmitter location (in km).
    % -------------------------------------------------------------------------
    x = gauss_rv(0,1);
    y = gauss_rv(0,1);
    
    % -------------------------------------------------------------------------
    % Transmitter is a source.
    % -------------------------------------------------------------------------
    dev.set_name(['Int-Tx-' num2str(idx_int)]);
    dev.set_coordinate([x,y,0]*150);
    dev.set_marker('rx');
    sources{idx_int} = copy_object(dev);
    
    % -------------------------------------------------------------------------
    % Random interference receiver location (in km).
    % -------------------------------------------------------------------------
    x = gauss_rv(0,1);
    y = gauss_rv(0,1);
    
    % -------------------------------------------------------------------------
    % Receiver is a destination.
    % -------------------------------------------------------------------------
    dev.set_name(['Int-Rx-' num2str(idx_int)]);
    dev.set_coordinate([x,y,0]*150);
    dev.set_marker('ro');
    dests{idx_int} = copy_object(dev);
    
    % -------------------------------------------------------------------------
    % Add interference source-destination pair to network.
    % -------------------------------------------------------------------------
    net.add_source_destination(sources{idx_int},dests{idx_int});
end

% -------------------------------------------------------------------------
% Automatically populate links based on source-destination pairs.
% -------------------------------------------------------------------------
net.populate_links_from_source_destination();

% -------------------------------------------------------------------------
% Set network-wide channel and path loss models.
% -------------------------------------------------------------------------
net.set_path_loss(path_loss_object);
net.set_channel(channel_object);

% -------------------------------------------------------------------------
% Finish network setup.
% -------------------------------------------------------------------------
net.set_symbol_bandwidth(symbol_bandwidth_Hz);
net.set_propagation_velocity(propagation_velocity_meters_per_sec);
net.set_carrier_frequency(carrier_frequency_Hz);
net.set_num_streams(num_streams);
net.set_transmit_power(transmit_power_dBm,'dBm');
net.set_noise_power_per_Hz(noise_power_per_Hz_dBm_Hz,'dBm_Hz');

% -------------------------------------------------------------------------
% View the network: solid black lines are desired links, dashed red lines
% are interference links.
% -------------------------------------------------------------------------
net.show_2d();

% -------------------------------------------------------------------------
% Invoke a realization of the entire network.
% -------------------------------------------------------------------------
net.realization();

% -------------------------------------------------------------------------
% Compute and supply channel state information to each device.
% -------------------------------------------------------------------------
net.compute_channel_state_information();
net.supply_channel_state_information();

% -------------------------------------------------------------------------
% Configure all transmitters and receivers.
% -------------------------------------------------------------------------
net.configure_transmitter('eigen');
net.configure_receiver('mmse');

% -------------------------------------------------------------------------
% Compute received signals based on realization and configuration.
% -------------------------------------------------------------------------
net.compute_received_signals();

% -------------------------------------------------------------------------
% Report mutual information of the desired link.
% -------------------------------------------------------------------------
mi_12 = net.report_mutual_information(dev_1,dev_2);
disp(['M.I. (1) --> (2): ' num2str(mi_12) ' bps/Hz']);

% -------------------------------------------------------------------------
% Report symbol estimation error of the desired link.
% -------------------------------------------------------------------------
[err_12,nerr_12] = net.report_symbol_estimation_error(dev_1,dev_2);
disp(['Norm. symb. est. error: (1) --> (2): ' num2str(10*log10(nerr_12)) ' dB']);

% -------------------------------------------------------------------------
% Compare MMSE and MMSE-Int.
% -------------------------------------------------------------------------
net.configure_receiver('mmse');
mi_12 = net.report_mutual_information(dev_1,dev_2);
disp(['M.I. (MMSE)      (1) --> (2): ' num2str(mi_12) ' bps/Hz']);

net.configure_receiver('mmse-int');
mi_12 = net.report_mutual_information(dev_1,dev_2);
disp(['M.I. (MMSE-Int)  (1) --> (2): ' num2str(mi_12) ' bps/Hz']);