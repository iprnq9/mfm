classdef link < matlab.mixin.Copyable
    properties
        name; % human-reable identifier of the link
        head; % head device (transmitter or transceiver)
        tail; % tail device (receiver or transceiver)
        channel_symmetric; % boolean indicating if the forward and reverse channels are symmetric
        channel_forward; % channel object for the forward link
        channel_reverse; % channel object for the reverse link
        channel_matrix_forward;
        channel_matrix_reverse;
        % large_scale_gain_model; % 'fspl'
        % large_scale_gain_symmetric;
        large_scale_gain_forward;
        large_scale_gain_reverse;
        % path_loss;
        path_loss_forward;
        path_loss_reverse;
        path_loss_symmetric; % boolean indicating if the forward and reverse path loss are symmetric
        % path_loss_exponent = 2;
        % carrier_frequency;
        % carrier_wavelength;
        % propagation_velocity;
        snr_forward;
        snr_reverse;
        distance; % distance (meters) between head device and tail device
        line_style = '-';
        color = 'k';
    end
    methods(Static)
        function obj = create(head,tail)
            % CREATE Creates a link between two devices: a head device
            % (which has transmit capability) and a tail device (which has
            % receive capability).
            % 
            % Usage:
            %  obj = link.create(head,tail)
            %
            % Args:
            %  head: a device with transmit capability
            %  tail: a device with receive capability
            %
            % Returns:
            %  obj: a link object
            obj = link(head,tail);
        end
    end
    methods
        function obj = link(head,tail)
            % LINK Creates an instance of a link object.
            % 
            % Usage:
            %  obj = LINK(head,tail) 
            %
            % Args:
            %  head: a device object at the head of the link; device must
            %  be a transmitter or transceiver
            %  tail: a device object at the tail of the link; device must
            %  be a receiver or transceiver
            % 
            % Returns:
            %  obj: a link object
            obj.set_head(head);
            obj.set_tail(tail);
            obj.initialize();
        end
        
        function initialize(obj)
            % INITIALIZE Executes intialization steps to set up a link.
            %
            % Usage:
            %  INITIALIZE()
            obj.set_name();
            obj.set_distance();
            obj.set_channel_symmetric(obj.isbidirectional());
            obj.set_path_loss_symmetric(obj.isbidirectional());
        end
        
        % -----------------------------------------------------------------
        % Set functions.
        % -----------------------------------------------------------------
        
        function set_name(obj,name)
            % SET_NAME Sets the name of the link.
            %
            % Usage:
            %  SET_NAME()
            %  SET_NAME(name)
            %
            % Args:
            %  name: (optional) a string; if not passed, a name is created
            if nargin < 2 || isempty(name)
                name = ['link: ' obj.head.name ' <--> ' obj.tail.name];
            end
            obj.name = name;
        end
        
        function set_head(obj,head)
            % SET_HEAD Sets the head device of the link.
            %
            % Usage:
            %  SET_HEAD(head)
            %
            % Args:
            %  head: a device of type 'transmitter' or 'transceiver'
            if head.istransmitter()
                obj.head = head;
            else
                warning('Device is not a transmitter or transceiver.');
                warning('Link head device must contain a transmitter.');
                warning('Did not set head device.');
            end
        end
        
        function set_tail(obj,tail)
            % SET_TAIL Sets the tail device of the link.
            %
            % Usage:
            %  SET_TAIL(tail)
            %
            % Args:
            %  head: a device of type 'receiver' or 'transceiver'
            if tail.isreceiver()
                obj.tail = tail;
            else
                warning('Device is not a receiver or transceiver.');
                warning('Link tail device must contain a receiver.');
                warning('Did not set tail device.');
            end
        end

        function set_propagation_velocity(obj,vel)
            % SET_PROPAGATION_VELOCITY Sets the propagation velocity of the
            % link. Also sets the propagation velocity of the head and tail
            % devices.
            %
            % Usage:
            %  SET_PROPAGATION_VELOCITY(vel)
            %
            % Args:
            %  vel: propagation velocity (meters/sec)
            obj.channel_forward.set_propagation_velocity(vel);
            obj.path_loss_forward.set_propagation_velocity(vel);
            obj.head.set_propagation_velocity(vel);
            obj.tail.set_propagation_velocity(vel);
            if obj.isbidirectional()
                obj.channel_reverse.set_propagation_velocity(vel);
                obj.path_loss_reverse.set_propagation_velocity(vel);    
            end
        end
        
        function set_carrier_frequency(obj,fc)
            % SET_CARRIER_FREQUENCY Sets the carrier frequency of the
            % link. Also sets the carrier frequency of the head and tail
            % devices.
            %
            % Usage:
            %  SET_CARRIER_FREQUENCY(fc)
            %
            % Args:
            %  fc: carrier frequency (Hz)
            %
            % Notes:
            %  : Also updates carrier wavelength.
            obj.channel_forward.set_carrier_frequency(fc);
            obj.path_loss_forward.set_carrier_frequency(fc);
            obj.head.set_carrier_frequency(fc);
            obj.tail.set_carrier_frequency(fc);
            if obj.isbidirectional()
                obj.channel_reverse.set_carrier_frequency(fc);
                obj.path_loss_reverse.set_carrier_frequency(fc);
            end
        end
                        
        function set_distance(obj,d)
            % SET_DISTANCE Sets the distance of the link (in meters).
            %
            % Usage:
            %  SET_DISTANCE() 
            %  SET_DISTANCE(d)
            %
            % Args:
            %  d: (optional) distance of the link (in meters); if not
            %  passed, the head and tail devices' coordinates will be used
            %  to calculate the distance of the link
            if nargin < 2
                coordinate_head = obj.head.coordinate;
                coordinate_tail = obj.tail.coordinate;
                delta = coordinate_head - coordinate_tail;
                d = sqrt(sum(delta.^2));
            end
            if d < 0
                warning('Distance cannot be less than 0. Not setting distance.');
            else
                obj.distance = d;
                if ~isempty(obj.path_loss_forward)
                    obj.path_loss_forward.set_distance(d);
                    if obj.isbidirectional()
                        obj.path_loss_reverse.set_distance(d);
                    end
                    obj.realization_path_loss();
                end
            end
        end
        
        function set_large_scale_gain(obj,G_forward,G_reverse)
            % SET_LARGE_SCALE_GAIN Sets the large-scale gain of the
            % forward and reverse links to the same value.
            %
            % Usage:
            %  SET_LARGE_SCALE_GAIN(G_forward)
            %  SET_LARGE_SCALE_GAIN(G_forward,G_reverse)
            %
            % Args:
            %  G_forward: large-scale gain of the forward link
            %  G_reverse: (optional) large-scale gain of the reverse link; 
            %  if not passed, G_forward will be used as G_reverse
            if nargin < 3
                G_reverse = G_forward;
            end
            obj.set_large_scale_gain_forward(G_forward);
            obj.set_large_scale_gain_reverse(G_reverse);
        end
        
        function set_large_scale_gain_forward(obj,G)
            % SET_LARGE_SCALE_GAIN_FORWARD Sets the large-scale gain of the
            % forward link.
            %
            % Usage:
            %  SET_LARGE_SCALE_GAIN_FORWARD(G)
            %
            % Args:
            %  G: large-scale amplitude gain (G^2 is power gain)
            obj.large_scale_gain_forward = G;
            obj.set_snr_forward();
        end
        
        function set_large_scale_gain_reverse(obj,G)
            % SET_LARGE_SCALE_GAIN_REVERSE Sets the large-scale gain of the
            % reverse link.
            %
            % Usage:
            %  SET_LARGE_SCALE_GAIN_REVERSE(G)
            %
            % Args:
            %  G: large-scale amplitude gain (G^2 is power gain)
            if obj.isbidirectional()
                obj.large_scale_gain_reverse = G;
                obj.set_snr_reverse();
            end
        end
                        
        function set_channel(obj,channel_forward,channel_reverse)
            % SET_CHANNEL Sets the channel used on the forward and reverse
            % channels.
            %
            % Usage:
            %  SET_CHANNEL(channel_forward)
            %  SET_CHANNEL(channel_forward,channel_reverse)
            %
            % Args:
            %  channel_forward: a channel object used on the forward link
            %  channel_reverse: (optional) a channel object used on the 
            %  reverse link; if not provided, the reverse channel will use
            %  the same channel object as the forward channel (i.e.,
            %  channel_reverse = channel_forward)
            if nargin < 3
                channel_reverse = channel_forward;
            end
            obj.set_channel_forward(channel_forward);
            obj.set_channel_reverse(channel_reverse);
        end
        
        function set_channel_forward(obj,channel)
            % SET_CHANNEL_FORWARD Sets the channel used on the forward 
            % link.
            % 
            % Usage:
            %  SET_CHANNEL_FORWARD(channel)
            %
            % Args:
            %  channel: a channel object
            channel = copy_object(channel);
            transmit_array = obj.head.transmitter.array;
            receive_array = obj.tail.receiver.array;
            channel.set_transmit_array(transmit_array);
            channel.set_receive_array(receive_array);
            obj.channel_forward = channel;
        end
        
        function set_channel_reverse(obj,channel)
            % SET_CHANNEL_REVERSE Sets the channel used on the reverse 
            % link.
            % 
            % Usage:
            %  SET_CHANNEL_REVERSE(channel)
            %
            % Args:
            %  channel: a channel object
            if obj.isbidirectional()
                channel = copy_object(channel);
                transmit_array = obj.tail.transmitter.array;
                receive_array = obj.head.receiver.array;
                channel.set_transmit_array(transmit_array);
                channel.set_receive_array(receive_array);
                obj.channel_reverse = channel;
            end
        end
        
        function set_channel_symmetric(obj,symmetric)
            % SET_CHANNEL_SYMMETRIC Sets the symmetry of the forward and
            % reverse channels. Note that this is only possible if the
            % transmit array and receive array at the head are equal in
            % size and likewise at the tail.
            %
            % Usage:
            %  SET_CHANNEL_SYMMETRIC(symmetric)
            % 
            % Args:
            %  symmetric: a boolean indicating if the forward and reverse
            %  channels are symmetric; if so, then the reverse channel
            %  matrix is the conjugate-transpose of the the forward channel
            %  matrix
            symmetric = logical(symmetric);
            obj.channel_symmetric = symmetric;
            if symmetric
                obj.channel_matrix_reverse = obj.channel_matrix_forward';
            end
        end
        
        function set_channel_matrices(obj,H_forward,H_reverse)
            % SET_CHANNEL_MATRICES Sets the forward and reverse channel
            % matrices.
            %
            % Usage:
            %  SET_CHANNEL_MATRICES()
            %  SET_CHANNEL_MATRICES(H_forward)
            %  SET_CHANNEL_MATRICES(H_forward,H_reverse)
            %
            % Args:
            %  H_forward: (optional) forward channel matrix
            %  H_forward: (optional) reverse channel matrix
            if nargin < 2
                H_forward = [];
            end
            if nargin < 3
                H_reverse = H_forward;
            end
            obj.set_channel_matrix_forward(H_forward);
            obj.set_channel_matrix_reverse(H_reverse);
        end
        
        function set_channel_matrix_forward(obj,H)
            % SET_CHANNEL_MATRIX_FORWARD Sets the channel matrix of the
            % forward link.
            %
            % Usage:
            %  SET_CHANNEL_MATRIX_FORWARD()
            %  SET_CHANNEL_MATRIX_FORWARD(H) 
            %
            % Args:
            %  H: (optional) a channel matrix; if not passed, the forward
            %  channel's channel matrix will be used
            if nargin < 2 || isempty(H)
                H = obj.channel_forward.get_channel_matrix();
            end
            obj.channel_matrix_forward = H;
        end
        
        function set_channel_matrix_reverse(obj,H)
            % SET_CHANNEL_MATRIX_REVERSE Sets the channel matrix of the
            % reverse link.
            %
            % Usage:
            %  SET_CHANNEL_MATRIX_REVERSE() 
            %  SET_CHANNEL_MATRIX_REVERSE(H)
            %
            % Args:
            %  H: (optional) a channel matrix; if not passed, the reverse
            %  channel's channel matrix will be used (if not symmetric);
            %  else the forward channel's channel matrix will be used
            if nargin < 2 || isempty(H)
                if obj.channel_symmetric
                    H = obj.channel_forward.get_channel_matrix();
                    H = H';
                else
                    H = obj.channel_reverse.get_channel_matrix();
                end
            end
            obj.channel_matrix_reverse = H;
        end
        
        function set_path_loss(obj,path_loss_forward,path_loss_reverse)
            % SET_PATH_LOSS Sets the path loss model used on the link.
            %
            % Usage:
            %  SET_PATH_LOSS(path_loss_forward)
            %  SET_PATH_LOSS(path_loss_forward,path_loss_reverse)
            %
            % Args:
            %  path_loss_forward: a path loss object used on the forward 
            %  link
            %  path_loss_reverse: (optional) a path loss object used on the 
            %  reverse link; if not provided, the reverse path loss will 
            %  use the same path loss object as the forward one (i.e.,
            %  path_loss_reverse = path_loss_forward)
            if nargin < 3
                path_loss_reverse = path_loss_forward;
            end
            obj.set_path_loss_forward(path_loss_forward);
            obj.set_path_loss_reverse(path_loss_reverse);
        end
        
        function set_path_loss_forward(obj,path_loss)
            % SET_PATH_LOSS_FORWARD Sets the path loss model used on the
            % forward link.
            %
            % Usage:
            %  SET_PATH_LOSS_FORWARD(path_loss)
            %  SET_PATH_LOSS_FORWARD(path_loss,copy)
            %
            % Args:
            %  path_loss: a path loss object
            if isa(path_loss,'path_loss')
                path_loss = copy_object(path_loss);
                path_loss.set_distance(obj.distance);
                path_loss.realization();
                obj.path_loss_forward = path_loss;
            else
                error('path_loss should be a path_loss object.');
            end
        end
        
        function set_path_loss_reverse(obj,path_loss)
            % SET_PATH_LOSS_REVERSE Sets the path loss model used on the
            % reverse link.
            %
            % Usage:
            %  SET_PATH_LOSS_REVERSE(path_loss)
            %
            % Args:
            %  path_loss: a path loss object
            if isa(path_loss,'path_loss')
                if obj.isbidirectional()
                    path_loss = copy_object(path_loss);
                    path_loss.set_distance(obj.distance);
                    path_loss.realization();
                    obj.path_loss_reverse = path_loss;
                end
            else
                error('path_loss should be a path_loss object.');
            end
        end
        
        function set_path_loss_symmetric(obj,symmetric)
            % SET_PATH_LOSS_SYMMETRIC Sets the symmetry of the 
            % forward and reverse path loss.
            %
            % Usage:
            %  SET_PATH_LOSS_SYMMETRIC(symmetric)
            % 
            % Args:
            %  symmetric: a boolean indicating if the forward and reverse
            %  path loss are symmetric; if so, then the reverse large-scale
            %  gain will always be equal to the forward large-scale gain
            obj.path_loss_symmetric = logical(symmetric);
        end
        
        function set_snr(obj,snr_forward,snr_reverse,unit)
            % SET_SNR Sets the SNR of the forward and reverse links.
            %
            % Usage:
            %  SET_SNR()
            %  SET_SNR(snr_forward,snr_reverse)
            %  SET_SNR(snr_forward,snr_reverse,unit)
            %
            % Args:
            %  snr_forward: (optional) the large-scale SNR of the forward 
            %  link
            %  snr_reverse: (optional) the large-scale SNR of the reverse
            %  link; if not passed, the SNR of the forward link is used
            if nargin < 2
                snr_forward = [];
            end
            if nargin < 3 || isempty(snr_reverse)
                snr_reverse = snr_forward;
            end
            if nargin < 4 && ~ischar(snr_reverse)
                unit = [];
            end
            if ischar(snr_reverse)
                unit = snr_reverse;
                snr_reverse = snr_forward;
            end
            obj.set_snr_forward(snr_forward,unit);
            obj.set_snr_reverse(snr_reverse,unit);
        end
        
        function set_snr_forward(obj,snr,unit)
            % SET_SNR_FORWARD Sets the SNR of the forward link.
            %
            % Usage:
            %  SET_SNR_FORWARD()
            %  SET_SNR_FORWARD(snr)
            %  SET_SNR_FORWARD(snr,unit)
            %
            % Args:
            %  snr: (optional) the large-scale SNR of the forward link; if
            %  passed, the large-scale gain of the forward link is adjusted
            %  accordingly; if not passed, the SNR is computed based on the
            %  current large-scale gain
            %  unit: (optional) a string specifying the unit of snr
            P = obj.head.transmitter.transmit_energy_per_symbol;
            N = obj.tail.receiver.noise_energy_per_symbol;
            if nargin < 2 || isempty(snr)
                G = obj.large_scale_gain_forward;
                snr = P * G^2 / N;
            else
                if nargin < 3 || isempty(unit)
                    unit = 'linear';
                end
                if strcmpi(unit,'dB')
                    snr = 10^(snr/10);
                end
                G = sqrt(snr * N / P);
                obj.set_large_scale_gain_forward(G);
            end
            obj.snr_forward = snr;
        end
        
        function set_snr_reverse(obj,snr,unit)
            % SET_SNR_REVERSE Sets the SNR of the reverse link.
            %
            % Usage:
            %  SET_SNR_REVERSE()
            %  SET_SNR_REVERSE(snr)
            %  SET_SNR_REVERSE(snr,unit)
            %
            % Args:
            %  snr: (optional) the large-scale SNR of the reverse link; if
            %  passed, the large-scale gain of the reverse link is adjusted
            %  accordingly; if not passed, the SNR is computed based on the
            %  current large-scale gain
            %  unit: (optional) a string specifying the unit of snr
            if obj.isbidirectional()
                P = obj.tail.transmitter.transmit_energy_per_symbol;
                N = obj.head.receiver.noise_energy_per_symbol;
                if nargin < 2 || isempty(snr)
                    G = obj.large_scale_gain_reverse;
                    snr = P * G^2 / N;
                else
                    if nargin < 3 || isempty(unit)
                        unit = 'linear';
                    end
                    if strcmpi(unit,'dB')
                        snr = 10^(snr/10);
                    end
                    G = sqrt(snr * N / P);
                    obj.set_large_scale_gain_reverse(G);
                end
                obj.snr_reverse = snr;
            end
        end
        
        function set_color(obj,color)
            % SET_COLOR Sets the color used when plotting the link.
            %
            % Usage:
            %  SET_COLOR(color)
            %
            % Args:
            %  color: an RGB triplet normalized to [0,1] or a MATLAB color
            %  code (e.g., 'k','b','r').
            obj.color = color;
        end
        
        function set_line_style(obj,line_style)
            % SET_LINE_STYLE Sets the line style used when plotting the 
            % link.
            %
            % Usage:
            %  SET_LINE_STYLE(line_style)
            %
            % Args:
            %  line_style: a MATLAB line style; either '-' (solid), '--'
            %  (dashed), ':' (dotted), or '-.' (dash-dotted)
            obj.line_style = line_style;
        end
        
        % -----------------------------------------------------------------
        % Compute functions.
        % -----------------------------------------------------------------
        
        function [csi_forward,csi_reverse] = compute_channel_state_information(obj)
            % COMPUTE_CHANNEL_STATE_INFORMATION Constrcuts a struct
            % containing channel state information for the forward and 
            % reverse links.
            %
            % Usage:
            %  [csi_forward,csi_reverse] = COMPUTE_CHANNEL_STATE_INFORMATION()
            %
            % Returns:
            %  csi_forward: a struct containing CSI values for the forward
            %  link
            %  csi_reverse: a struct containing CSI values for the reverse
            %  link
            csi_forward = obj.compute_channel_state_information_forward();
            csi_reverse = obj.compute_channel_state_information_reverse();
        end
        
        function csi = compute_channel_state_information_forward(obj)
            % COMPUTE_CHANNEL_STATE_INFORMATION_FORWARD Constrcuts a struct
            % containing channel state information for the forward link.
            %
            % Usage:
            %  csi = COMPUTE_CHANNEL_STATE_INFORMATION_FORWARD()
            %
            % Returns:
            %  csi: a struct containing CSI values
            csi.name = [obj.head.name ' ---> ' obj.tail.name];
            csi.channel_matrix = obj.channel_matrix_forward;
            csi.large_scale_gain = obj.large_scale_gain_forward;
            csi.snr = obj.snr_forward;
            csi.transmit_energy_per_symbol = obj.head.transmitter.transmit_energy_per_symbol;
            csi.noise_energy_per_symbol = obj.tail.receiver.noise_energy_per_symbol;
            csi.num_streams = obj.head.transmitter.num_streams;
            csi.device_transmit = obj.head;
            csi.device_receive = obj.tail;
        end
        
        function csi = compute_channel_state_information_reverse(obj)
            % COMPUTE_CHANNEL_STATE_INFORMATION_REVERSE Constrcuts a struct
            % containing channel state information for the reverse link.
            %
            % Usage:
            %  csi = COMPUTE_CHANNEL_STATE_INFORMATION_REVERSE()
            %
            % Returns:
            %  csi: a struct containing CSI values
            if obj.isbidirectional()
                csi.name = [obj.tail.name ' ---> ' obj.head.name];
                csi.channel_matrix = obj.channel_matrix_reverse;
                csi.large_scale_gain = obj.large_scale_gain_reverse;
                csi.snr = obj.snr_reverse;
                csi.transmit_energy_per_symbol = obj.tail.transmitter.transmit_energy_per_symbol;
                csi.noise_energy_per_symbol = obj.head.receiver.noise_energy_per_symbol;
                csi.num_streams = obj.tail.transmitter.num_streams;
                csi.device_transmit = obj.tail;
                csi.device_receive = obj.head;
            else
                csi = [];
            end
        end
        
        function [budget_fwd, budget_rev] = compute_link_budget(obj)
            % COMPUTE_LINK_BUDGET Returns a struct of values used 
            % for a link budget, in log-scale (e.g., dB, dBm), of the 
            % forward link and reverse link.
            %
            % Usage:
            %  [budget_fwd, budget_rev] = GET_LINK_BUDGET()
            %
            % Returns:
            %  budget_fwd: a struct containing link budget quantities in
            %  log-scale for the forward link
            %  budget_rev: a struct containing link budget quantities in
            %  log-scale for the forward link
            budget_fwd = obj.compute_link_budget_forward();
            budget_rev = obj.compute_link_budget_reverse();
        end
        
        function budget = compute_link_budget_forward(obj)
            % COMPUTE_LINK_BUDGET_FORWARD Returns a struct of values used 
            % for a link budget, in log-scale (e.g., dB, dBm), of the 
            % forward link.
            %
            % Usage:
            %  budget = GET_LINK_BUDGET_FORWARD()
            %
            % Returns:
            %  budget: a struct containing link budget quantities in
            %  log-scale
            P = obj.head.transmitter.transmit_power;
            G = obj.large_scale_gain_forward;
            N = obj.tail.receiver.noise_power;
            budget.transmit_power_dBm = 10 * log10(P) + 30;
            budget.propagation_loss_dB = -20 * log10(G);
            budget.receive_power_dBm = budget.transmit_power_dBm - budget.propagation_loss_dB;
            budget.noise_power_dBm = 10 * log10(N) + 30;
            budget.snr_dB = 10 * log10(P * G^2 / N);
        end
        
        function budget = compute_link_budget_reverse(obj)
            % COMPUTE_LINK_BUDGET_REVERSE Returns a struct of values used 
            % for a link budget, in log-scale (e.g., dB, dBm), of the 
            % reverse link.
            %
            % Usage:
            %  budget = COMPUTE_LINK_BUDGET_REVERSE()
            %
            % Returns:
            %  budget: a struct containing link budget quantities in
            %  log-scale
            if obj.isbidirectional()
                P = obj.head.transmitter.transmit_power;
                G = obj.large_scale_gain_forward;
                N = obj.tail.receiver.noise_power;
                budget.transmit_power_dBm = 10 * log10(P) + 30;
                budget.propagation_loss_dB = -20 * log10(G);
                budget.receive_power_dBm = budget.transmit_power_dBm - budget.propagation_loss_dB;
                budget.noise_power_dBm = 10 * log10(N) + 30;
                budget.snr_dB = 10 * log10(P * G^2 / N);
            else
                budget = [];
            end
        end
        
        function [Ry,Rn,SNR] = compute_covariance_forward(obj)
            % COMPUTE_COVARIANCE_FORWARD Computes the covariance of the
            % received symbols at the tail device from the head device.
            % Also returns the covariance of received noise.
            %
            % Usage:
            %  [Ry,Rn] = COMPUTE_COVARIANCE_FORWARD()
            %
            % Returns:
            %  Ry: the received desired symbols covariance
            %  Rn: the received noise covariance
            source = obj.head;
            destination = obj.tail;
            P = source.transmitter.transmit_energy_per_symbol;
            F = source.transmitter.precoder;
            Rs = source.transmitter.transmit_symbol_covariance;
            W = destination.receiver.combiner;
            Rn = destination.receiver.noise_covariance;
            G = obj.large_scale_gain_forward;
            H = obj.channel_matrix_forward;
            N = destination.receiver.noise_energy_per_symbol;
            SNR = P * G^2 / N;
            % SNR = sqrt(P) * G / sqrt(N);
            % SNR = SNR.^2;
            Ry = W' * (H * (F * Rs * F') * H') * W;
            Rn = W' * W;
            % Ry = P * G^2 * (W' * (H * (F * Rs * F') * H') * W);
            % Rn = W' * Rn * W;
        end
                
        function [Ry,Rn] = compute_covariance_reverse(obj)
            % COMPUTE_COVARIANCE_REVERSE Computes the covariance of the
            % received symbols at the head device from the tail device.
            % Also returns the covariance of received noise.
            %
            % Usage:
            %  [Ry,Rn] = COMPUTE_COVARIANCE_REVERSE()
            %
            % Returns:
            %  Ry: the received desired symbols covariance
            %  Rn: the received noise covariance
            if obj.isbidirectional()
                source = obj.tail;
                destination = obj.head;
                P = source.transmitter.transmit_energy_per_symbol;
                F = source.transmitter.precoder;
                Rs = source.transmitter.transmit_symbol_covariance;
                W = destination.receiver.combiner;
                % N = destination.receiver.noise_energy_per_symbol;
                Rn = destination.receiver.noise_covariance;
                G = lnk.large_scale_gain_forward;
                H = lnk.channel_matrix_forward;
                % SNR = P * G^2 / N;
                Ry = P * G^2 * (W' * (H * (F * Rs * F') * H') * W);
                Rn = W' * Rn * W;
            else
                Ry = [];
                Rn = [];
            end
        end
        
        function mi = report_mutual_information_forward(obj)
            % REPORT_MUTUAL_INFORMATION_FORWARD Reports the Gaussian mutual
            % information achieved between the head and tail devices based
            % on the current link realization and transmit-receive
            % configurations. Returns the mutual information in bps/Hz. 
            % Discards any residual imaginary component.
            %
            % Usage:
            %  mi = REPORT_MUTUAL_INFORMATION_FORWARD()
            % 
            % Returns:
            %  mi: the mutual information in bps/Hz
            %
            % Notes:
            %  : Will warn the user if a significant imaginary portion exists
            %  when computing the mutual information. Ultimately discards
            %  it though.
            [Ry,Rn,SNR] = obj.compute_covariance_forward();
            I = eye(size(Ry));
            % mi = log2(det(I + SNR * (Ry / Rn)));
            r = rank(Rn);
            mi = log2(det(I(1:r,1:r) + SNR * (Ry(1:r,1:r) / Rn(1:r,1:r))));
            if abs(imag(mi)) > 1e-6
                warning('FYI: Imaginary portion of mutual information was greater than 1e-6 in magnitude.');
            end
            mi = real(mi);
        end
        
        function mi = report_mutual_information_reverse(obj)
            % REPORT_MUTUAL_INFORMATION_REVERSE Reports the Gaussian mutual
            % information achieved between the tail and head devices based
            % on the current link realization and transmit-receive
            % configurations. Returns the mutual information in bps/Hz. 
            % Discards any residual imaginary component.
            %
            % Usage:
            %  mi = REPORT_MUTUAL_INFORMATION_REVERSE()
            % 
            % Returns:
            %  mi: the mutual information in bps/Hz
            %
            % Notes:
            %  : Will warn the user if a significant imaginary portion exists
            %  when computing the mutual information. Ultimately discards
            %  it though.
            if obj.isbidirectional()
                [Ry,Rn] = obj.compute_covariance_reverse();
                I = eye(size(Ry));
                mi = log2(det(I + Ry / Rn));
                if abs(imag(mi)) > 1e-6
                    warning('FYI: Imaginary portion of mutual information was greater than 1e-6 in magnitude.');
                end
                mi = real(mi);
            else
                mi = [];
            end
        end
        
        function out = isbidirectional(obj)
            % ISBIDIRECTIONAL Returns a boolean indicating if the link is
            % bidirectional (i.e., if the head device has receive
            % capability and the tail device has transmit capability).
            %
            % Usage:
            %  out = ISBIDIRECTIONAL()
            %
            % Returns:
            %  out: a boolean indicating if the link is bidirectional
            out = obj.head.isreceiver() & obj.tail.istransmitter();
        end

        % -----------------------------------------------------------------
        % Realization functions.
        % -----------------------------------------------------------------
        
        function realization(obj)
            % REALIZATION Invokes a complete realization of the link.
            %
            % Usage:
            %  REALIZATION()
            %
            % Notes:
            %  : Invokes realizations of the foward and reverse path loss
            %  and forward and reverse channels.
            obj.set_distance();
            obj.realization_path_loss();
            obj.realization_channel();
        end
        
        function [G_forward,G_reverse] = realization_path_loss(obj)
            % REALIZATION_PATH_LOSS Invokes a realization fo the forward
            % and reverse path loss.
            % 
            % Usage:
            %  [G_forward,G_reverse] = REALIZATION_PATH_LOSS()
            %
            % Returns:
            %  G_forward: the resulting forward large-scale gain
            %  G_reverse: the resulting reverse large-scale gain
            G_forward = obj.realization_path_loss_forward();
            G_reverse = obj.realization_path_loss_reverse();
        end
        
        function G = realization_path_loss_forward(obj)
            % REALIZATION_PATH_LOSS_FORWARD Invokes a realization of the
            % path loss model on the forward link.
            % 
            % Usage:
            %  G = REALIZATION_PATH_LOSS_FORWARD()
            %
            % Returns:
            %  G: the large-scale gain following the forward path loss
            %  realization
            obj.path_loss_forward.realization();
            L = obj.path_loss_forward.attenuation;
            G = 1 / sqrt(L);
            obj.set_large_scale_gain_forward(G);
        end
        
        function G = realization_path_loss_reverse(obj)
            % REALIZATION_PATH_LOSS_REVERSE Invokes a realization of the
            % path loss model on the reverse link. If the link is set to
            % have symmetric path loss, the current forward path loss is 
            % used.
            % 
            % Usage:
            %  G = REALIZATION_PATH_LOSS_REVERSE()
            %
            % Returns:
            %  G: the large-scale gain following the reverse path loss
            %  realization
            if obj.isbidirectional()
                if ~obj.path_loss_symmetric
                    obj.path_loss_reverse.realization();
                    L = obj.path_loss_reverse.attenuation;
                    G = 1 / sqrt(L);
                else
                    G = obj.large_scale_gain_forward;
                end
                obj.set_large_scale_gain_reverse(G);
            else
                G = [];
            end
        end
        
        function [H_fwd,H_rev] = realization_channel(obj)
            % REALIZATION_CHANNEL Invokes a channel realization on the
            % forward and reverse links.
            %
            % Usage:
            %  [H_fwd,H_rev] = REALIZATION_CHANNEL()
            %
            % Returns:
            %  H_fwd: the forward channel matrix realization
            %  H_rev: the reverse channel matrix realization
            H_fwd = obj.realization_channel_forward();
            H_rev = obj.realization_channel_reverse();
        end
        
        function H = realization_channel_forward(obj)
            % REALIZATION_CHANNEL_FORWARD Invokes a realization of the
            % forward channel and sets the forward channel matrix.
            %
            % Usage:
            %  H = REALIZATION_CHANNEL_FORWARD()
            %
            % Returns:
            %  H: a channel matrix for the forward channel
            obj.channel_forward.realization();
            H = obj.channel_forward.get_channel_matrix();
            obj.set_channel_matrix_forward(H);
        end
        
        function H = realization_channel_reverse(obj)
            % REALIZATION_CHANNEL_REVERSE Invokes a realization of the
            % reverse channel and sets the reverse channel matrix. If the
            % channel is set to symmetric, the reverse channel will be set
            % to the conjugate transpose of the current forward channel 
            % matrix instead of invoking a realization of the reverse 
            % channel. Therefore, a forward channel realization should be 
            % invoked before a reverse channel realization if the channel 
            % is symmetric.
            %
            % Usage:
            %  H = REALIZATION_CHANNEL_REVERSE()
            %
            % Returns:
            %  H: a channel matrix for the reverse channel
            if obj.isbidirectional()
                if ~obj.channel_symmetric
                    H = obj.channel_reverse.realization();
                else
                    H = obj.channel_forward.get_channel_matrix();
                    H = H';
                end
                obj.set_channel_matrix_reverse(H);
            else
                H = [];
            end
        end
        
        function compute_received_signal(obj)
            % COMPUTE_RECEIVED_SIGNAL Computes the received signal 
            % on the forward link and reverse link.
            %
            % Usage:
            %  COMPUTE_RECEIVED_SIGNAL()
            obj.compute_received_signal_forward();
            obj.compute_received_signal_reverse();
        end
        
        function [z,y,n] = compute_received_signal_forward(obj)
            % COMPUTE_RECEIVED_SIGNAL_FORWARD Computes the received signal 
            % at the tail device from the head device.
            %
            % Usage:
            %  [z,y,n] = COMPUTE_RECEIVED_SIGNAL_FORWARD()
            % 
            % Returns:
            %  z: the desired signal plus noise
            %  y: the desired signal vector
            %  n: the noise vector
            source = obj.head;
            destination = obj.tail;
            G = obj.large_scale_gain_forward;
            H = obj.channel_matrix_forward;
            P = source.transmitter.transmit_energy_per_symbol;
            F = source.transmitter.precoder;
            s = source.transmitter.transmit_symbol;
            y = sqrt(P) * G * H * F * s;
            destination.set_noise(); % generate new noise at receiver
            destination.set_received_signal(y);
            n = destination.receiver.noise;
            z = y + n;
            % obj.compute_received_signal();
        end
        
        function [z,y,n] = compute_received_signal_reverse(obj)
            % COMPUTE_RECEIVED_SIGNAL_REVERSE Computes the received signal 
            % at the head device from the tail device.
            %
            % Usage:
            %  [z,y,n] = COMPUTE_RECEIVED_SIGNAL_REVERSE()
            % 
            % Returns:
            %  z: the desired signal plus noise
            %  y: the desired signal vector
            %  n: the noise vector
            if obj.isbidirectional()
                source = obj.tail;
                destination = obj.head;
                G = obj.large_scale_gain_reverse;
                H = obj.channel_matrix_reverse;
                P = source.transmitter.transmit_energy_per_symbol;
                F = source.transmitter.precoder;
                s = source.transmitter.transmit_symbol;
                y = sqrt(P) * G * H * F * s;
                destination.set_noise(); % generate new noise at receiver
                destination.set_received_signal(y);
                n = destination.receiver.noise;
                z = y + n;
                % obj.compute_received_signal();
            end
        end
        
        function [err,nerr] = report_symbol_estimation_error_forward(obj)
            % REPORT_SYMBOL_ESTIMATION_ERROR_FORWARD Returns the 
            % squared-error in the estimated receive symbol at the tail 
            % compared to the transmit symbol at the head.
            %
            % Usage:
            %  [mse,mse_dB] = REPORT_SYMBOL_ESTIMATION_ERROR_FORWARD()
            %
            % Returns:
            %  err: the squared error of the received symbols versus the
            %  transmitted symbols
            %  nerr: err normalized to the transmit symbol power
            s_tx = obj.head.transmitter.transmit_symbol;
            s_rx = obj.tail.receiver.receive_symbol;
            err = norm(s_tx - s_rx,2)^2;
            nerr = err ./ norm(s_tx,2)^2;
        end
        
        function [err,nerr] = report_symbol_estimation_error_reverse(obj)
            % REPORT_SYMBOL_ESTIMATION_ERROR_REVERSE Returns the 
            % squared-error in the estimated receive symbol at the head 
            % compared to the transmit symbol at the tail.
            %
            % Usage:
            %  [mse,mse_dB] = REPORT_SYMBOL_ESTIMATION_ERROR_REVERSE()
            %
            % Returns:
            %  err: the squared error of the received symbols versus the
            %  transmitted symbols
            %  nerr: err normalized to the transmit symbol power
            if obj.isbidirectional()
                s_tx = obj.tail.transmitter.transmit_symbol;
                s_rx = obj.head.receiver.receive_symbol;
                err = norm(s_tx - s_rx,2)^2;
                nerr = err ./ norm(s_tx,2)^2;
            else
                err = [];
                nerr = [];
            end
        end
        
        % -----------------------------------------------------------------
        % Passthrough functions.
        % -----------------------------------------------------------------
        
        function set_symbol_bandwidth(obj,B)
            % SET_SYMBOL_BANDWIDTH Sets the symbol bandwidth at the head
            % and tail devices.
            %
            % Usage:
            %  SET_SYMBOL_BANDWIDTH(B)
            %
            % Args:
            %  B: the symbol bandwidth (in Hertz) at the transmitter and
            %  receiver of the head and tail devices
            %
            % Notes:
            %  : Also updates the SNR of the link.
            obj.head.set_symbol_bandwidth(B);
            obj.tail.set_symbol_bandwidth(B);
            obj.set_snr();
        end
        
        function set_num_streams(obj,num_streams)
            % SET_NUM_STREAMS Sets the number of streams at the head
            % and tail devices.
            %
            % Usage:
            %  SET_NUM_STREAMS(num_streams)
            %
            % Args:
            %  num_streams: number of streams to multiplex at the
            %  transmitter and receiver of the head and tail devices
            obj.head.set_num_streams(num_streams);
            obj.tail.set_num_streams(num_streams);
        end
        
        function set_arrays(obj,transmit_array,receive_array)
            % SET_ARRAYS Sets the transmit and receive arrays at the head
            % and tail devices.
            %
            % Usage:
            %  SET_ARRAYS(transmit_array,receive_array)
            %
            % Args:
            %  transmit_array: array object to use at the transmitter of
            %  the head and tail devices
            %  receive_array: array object to use at the receiver of the
            %  head and tail devices
            obj.head.set_array(transmit_array,receive_array);
            obj.tail.set_array(transmit_array,receive_array);
        end
        
        function set_num_rf_chains(obj,Lt,Lr)
            % SET_NUM_RF_CHAINS Sets the number of transmit and receive RF
            % chains at the head and tail devices.
            %
            % Usage:
            %  SET_NUM_RF_CHAINS(Lt,Lr)
            %
            % Args:
            %  Lt: number of transmit RF chains at the head and tail
            %  devices
            %  Lr: number of receive RF chains at the head and tail devices
            obj.head.set_num_rf_chains(Lt,Lr);
            obj.tail.set_num_rf_chains(Lt,Lr);
        end
        
        function set_transmit_power(obj,P,unit)
            % SET_TRANSMIT_POWER Sets the transmit power at the transmitter
            % of the head and tail devices.
            %
            % Usage:
            %  SET_TRANSMIT_POWER(P)
            %  SET_TRANSMIT_POWER(P,unit)
            %
            % Args:
            %  P: transmit power (e.g., in watts or dBm)
            %  unit: (optional) a string specifying the units of P (e.g., 
            %  'dBm'); if not passed, the default is used.
            %
            % Notes:
            %  : Also updates the SNR of the link.
            if nargin < 3
                unit = [];
            end
            obj.head.set_transmit_power(P,unit);
            obj.tail.set_transmit_power(P,unit);
            obj.set_snr();
        end
        
        function set_transmit_symbol(obj,s)
            % SET_TRANSMIT_SYMBOL Sets the transmit symbol at the head and
            % tail devices.
            %
            % Usage:
            %  SET_TRANSMIT_SYMBOL(s)
            %
            % Args:
            %  s: transmit symbol vector
            obj.head.set_transmit_symbol(s);
            obj.tail.set_transmit_symbol(s);
        end
        
        function set_transmit_symbol_covariance(obj,Rs)
            % SET_TRANSMIT_SYMBOL_COVARIANCE Sets the transmit covariance
            % of the device.
            %
            % Usage:
            %  SET_TRANSMIT_SYMBOL_COVARIANCE(Rs)
            %
            % Args:
            %  Rs: a covariance matrix
            obj.head.set_transmit_symbol_covariance(Rs);
            obj.tail.set_transmit_symbol_covariance(Rs);
        end
        
        function set_noise_power_per_Hz(obj,noise_psd,unit)
            % set_noise_power_per_Hz Sets the noise power per Hz at the
            % receiver of the head and tail devices.
            %
            % Usage:
            %  set_noise_power_per_Hz(noise_psd)
            %  set_noise_power_per_Hz(noise_psd,unit)
            %
            % Args:
            %  noise_psd: the noise power spectral density
            %  unit: (optional) a string specifying the units of noise_psd 
            %  (e.g., 'dBm_Hz' or 'watts_Hz'); if not passed, the default
            %  will be used
            %
            % Notes:
            %  : Also updates the SNR of the link.
            if nargin < 3
                unit = [];
            end
            obj.head.set_noise_power_per_Hz(noise_psd,unit);
            obj.tail.set_noise_power_per_Hz(noise_psd,unit);
            obj.set_snr();
        end
        
        function set_transmit_strategy(obj,strategy)
            % SET_TRANSMIT_STRATEGY Sets the transmit strategy to use at 
            % the head and tail devices.
            %
            % Usage:
            %  SET_TRANSMIT_STRATEGY(strategy)
            %
            % Args:
            %  strategy: a string specifying the transmit strategy
            obj.head.set_transmit_strategy(strategy);
            obj.tail.set_transmit_strategy(strategy);
        end
        
        function set_receive_strategy(obj,strategy)
            % SET_RECEIVE_STRATEGY Sets the receive strategy to use at the
            % head and tail devices.
            %
            % Usage:
            %  SET_RECEIVE_STRATEGY(strategy)
            %
            % Args:
            %  strategy: a string specifying the receive strategy
            obj.head.set_receive_strategy(strategy);
            obj.tail.set_receive_strategy(strategy);
        end
        
        function set_marker(obj,marker_head,marker_tail)
            % SET_MARKER Sets the marker used by the head and tail devices.
            %
            % Usage:
            %  SET_MARKER(marker_head)
            %  SET_MARKER(marker_head,marker_tail)
            %
            % Args:
            %  marker_head: a marker to use at the head device
            %  marker_tail: (optional) a marker to use at the tail device;
            %  if not passed, the marker specified for the head will be
            %  used
            if nargin < 3
                marker_tail = marker_head;
            end
            obj.head.set_marker(marker_head);
            obj.tail.set_marker(marker_tail);
        end
        
        function set_duplexing(obj,duplex)
            % SET_DUPLEXING Sets the duplexing method at the head and tail
            % devices.
            %
            % Usage:
            %  SET_DUPLEXING(duplex)
            %
            % Args:
            %  duplex: a string specifying the duplexing method to use
            obj.head.set_duplexing(duplex);
            obj.tail.set_duplexing(duplex);
        end
        
        function configure_transmitter(obj,strategy)
            % CONFIGURE_TRANSMITTER Configures each device's transmitter
            % according to the current/specified transmit strategy, 
            % incorporating channel state information as applicable.
            %
            % Usage:
            %  CONFIGURE_TRANSMITTER()
            %  CONFIGURE_TRANSMITTER(strategy)
            %
            % Args:
            %  strategy: (optional) transmit strategy to use; if not
            %  passed, each device's current transmit strategy will be used
            if nargin < 2
                strategy = [];
            end
            obj.head.configure_transmitter(strategy);
            obj.tail.configure_transmitter(strategy);
        end
        
        function configure_receiver(obj,strategy)
            % CONFIGURE_RECEIVER Configures each device's receiver
            % according to the current/specified receive strategy, 
            % incorporating channel state information as applicable.
            %
            % Usage:
            %  CONFIGURE_RECEIVER()
            %  CONFIGURE_RECEIVER(strategy)
            %
            % Args:
            %  strategy: (optional) receive strategy to use; if not
            %  passed, each device's current receive strategy will be used
            if nargin < 2
                strategy = [];
            end
            obj.head.configure_receiver(strategy);
            obj.tail.configure_receiver(strategy);
        end
               
        function fig = show_3d(obj,fignum)
            % SHOW_3D Displays the link in 3-D.
            %
            % Usage:
            %  SHOW_3D()
            %  SHOW_3D(fignum)
            %
            % Args:
            %  fignum: (optional) a figure number; if not passed, a new 
            %  figure will be created
            %
            % Returns:
            %  fig: the resulting a figure handle
            if nargin < 2 || isempty(fignum)
                fig = figure();
            else
                fig = figure(fignum);
            end
            ax = gca();
            leg = {};
            head = obj.head;
            xh = head.coordinate(1);
            yh = head.coordinate(2);
            zh = head.coordinate(3);
            tail = obj.tail;
            xt = tail.coordinate(1);
            yt = tail.coordinate(2);
            zt = tail.coordinate(3);
            leg{1} = head.name;
            leg{2} = tail.name;
            color = obj.color;
            line_style = obj.line_style;
            plot3(xh,yh,zh,head.marker); hold(ax,'on');
            plot3(xt,yt,zt,tail.marker); 
            plot3([xh,xt],[yh,yt],[zh,zt],'Color',color,'LineStyle',line_style);
            hold(ax,'off');
            legend(ax,leg,'Location','EastOutside');
            grid(ax,'on');
            xlabel(ax,'$x$ (meters)');
            ylabel(ax,'$y$ (meters)');
            zlabel(ax,'$z$ (meters)');
        end
        
        function fig = show_2d(obj,fignum)
            % SHOW_2D Displays the link in 2-D.
            %
            % Usage:
            %  SHOW_2D()
            %  SHOW_2D(fignum)
            %
            % Args:
            %  fignum: (optional) a figure number; if not passed, a new 
            %  figure will be created
            %
            % Returns:
            %  fig: the resulting a figure handle
            if nargin < 2 || isempty(fignum)
                fig = figure();
            else
                fig = figure(fignum);
            end
            ax = gca();
            leg = {};
            head = obj.head;
            xh = head.coordinate(1);
            yh = head.coordinate(2);
            tail = obj.tail;
            xt = tail.coordinate(1);
            yt = tail.coordinate(2);
            leg{1} = head.name;
            leg{2} = tail.name;
            color = obj.color;
            line_style = obj.line_style;
            plot(xh,yh,head.marker); hold(ax,'on');
            plot(xt,yt,tail.marker); 
            plot([xh,xt],[yh,yt],'Color',color,'LineStyle',line_style);
            hold(ax,'off');
            legend(ax,leg,'Location','EastOutside');
            grid(ax,'on');
            xlabel(ax,'$x$ (meters)');
            ylabel(ax,'$y$ (meters)');
        end
    end
end