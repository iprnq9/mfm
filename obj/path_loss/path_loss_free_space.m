classdef path_loss_free_space < path_loss
    % PATH_LOSS_FREE_SPACE A free-space path loss model.
    properties
        % Inherited:
        % name; % human-readable identifier
        % type;
        % distance;
        % attenuation; % the power loss of the path
        % carrier_frequency; % carrier frequency (Hz)
        % carrier_wavelength; % carrier wavelength (m)
        % propagation_velocity; % propagation velocity (m/s)
        path_loss_exponent; % path loss exponent
    end
    methods
        function obj = path_loss_free_space(name)
            % PATH_LOSS_FREE_SPACE Creates a free-space path loss object.
            % 
            % Usage:
            %  obj = PATH_LOSS_FREE_SPACE()
            %  obj = PATH_LOSS_FREE_SPACE(name)
            % 
            % Args:
            %  name: an optional name for the object
            % 
            % Returns:
            %  obj: an object representing free-space path loss
            if nargin < 1 || isempty(name)
                name = 'path-loss-free-space';
            end
            obj.set_name(name);
        end
        
        function initialize_free_space(obj)
            % INITIALIZE_FREE_SPACE Initializes a free-space path loss
            % object.
            % 
            % Usage:
            %  INITIALIZE_FREE_SPACE()
            obj.initialize(); % default path loss initialization
            obj.set_type('free-space');
            obj.set_carrier_frequency(1);
            obj.set_propagation_velocity(1);
            obj.set_path_loss_exponent(2);
        end
                        
        % -----------------------------------------------------------------
        % Set functions.
        % -----------------------------------------------------------------
        
        function set_path_loss_exponent(obj,ple)
            % SET_PATH_LOSS_EXPONENT Sets the path loss exponent.
            %
            % Usage:
            %  SET_PATH_LOSS_EXPONENT(ple)
            %
            % Args:
            %  ple: path loss exponent (a positive number)
            if ple < 0
                warning('Path loss exponent should be non-negative. Setting to 2.');
                ple = 2;
            end
            obj.path_loss_exponent = ple;
        end
        
        function atten = compute_path_loss_attenuation(obj)
            % COMPUTE_PATH_LOSS_ATTENUATION Computes the path attenuation 
            % (path loss) according to the free-space path loss (FSPL) 
            % formula.
            %
            % Usage:
            %  atten = compute_path_loss_attenuation()
            %
            % Returns:
            %  atten: the path attenuation (path loss) (a power loss)
            %
            % Notes:
            %  : The free-space path loss (FSPL) equation used is
            %  G = (lambda / (4*pi))^2 * (1/d)^ple, where G is the inverse
            %  path loss in terms of power.
            d = obj.distance;
            ple = obj.path_loss_exponent;
            lam = obj.carrier_wavelength;
            val = lam ./ (4 .* pi);
            gain = val.^2 ./ d.^ple; % power gain
            atten = 1 ./ gain; % power attenuation/loss
        end
        
        function atten = realization(obj)
            % REALIZATION Invokes a realization of the path loss.
            %
            % Usage:
            %  atten = REALIZATION()
            %
            % Returns:
            %  atten: the realized path loss attenuation
            % 
            % Notes:
            %  : Since no random variables are involved in the FSPL equation,
            %  it will be determininstic and thus fixed across
            %  realizations.
            atten = obj.compute_path_loss_attenuation();
            obj.set_path_loss_attenuation(atten);
        end
    end
end