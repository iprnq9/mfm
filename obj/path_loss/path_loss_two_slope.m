classdef path_loss_two_slope < path_loss
    % PATH_LOSS_TWO_SLOPE A two-slope path loss model.
    properties
        % Inherited:
        % name; % human-readable identifier
        % type;
        % distance;
        % attenuation; % the power loss of the path
        % carrier_frequency; % carrier frequency (Hz)
        % carrier_wavelength; % carrier wavelength (m)
        % propagation_velocity; % propagation velocity (m/s)
        reference_distance; % reference distance (m)
        reference_path_loss; % path loss at reference distance (linear power loss)
        path_loss_exponents; % path loss exponents
    end
    methods
        function obj = path_loss_two_slope(name)
            % PATH_LOSS_TWO_SLOPE Creates a two-slope path loss object.
            % 
            % Usage:
            %  obj = PATH_LOSS_TWO_SLOPE()
            %  obj = PATH_LOSS_TWO_SLOPE(name)
            % 
            % Args:
            %  name: an optional name for the object
            % 
            % Returns:
            %  obj: an object representing two-slope path loss
            if nargin < 1 || isempty(name)
                name = 'path-loss-two-slope';
            end
            obj.set_name(name);
        end
        
        function initialize_free_space(obj)
            % INITIALIZE_TWO_SLOPE Initializes a two-slope path loss
            % object.
            % 
            % Usage:
            %  INITIALIZE_TWO_SLOPE()
            obj.initialize(); % default path loss initialization
            obj.set_type('two-slope');
            obj.set_carrier_frequency(1);
            obj.set_propagation_velocity(1);
            obj.set_path_loss_exponents(2,4);
            obj.set_reference_distance(10);
            obj.set_reference_path_loss(60,'dB');
        end
                        
        % -----------------------------------------------------------------
        % Set functions.
        % -----------------------------------------------------------------
        
        function set_path_loss_exponents(obj,ple_1,ple_2)
            % SET_PATH_LOSS_EXPONENTS Sets the two path loss exponents.
            %
            % Usage:
            %  SET_PATH_LOSS_EXPONENTS(ple_1,ple_2)
            %
            % Args:
            %  ple_1: path loss exponent for distances within the reference
            %  distance
            %  ple_2: path loss exponent for distances beyond the reference
            %  distance
            obj.path_loss_exponents = [ple_1,ple_2];
        end
        
        function set_reference_distance(obj,d0)
            % SET_REFERENCE_DISTANCE Sets the reference distance.
            %
            % Usage:
            %  SET_REFERENCE_DISTANCE(d0)
            %
            % Args:
            %  d0: the reference distance at which the path loss exponent
            %  changes from one path loss exponent to another
            obj.reference_distance = d0;
        end
        
        function set_reference_path_loss(obj,L0,unit)
            % SET_REFERENCE_PATH_LOSS Sets the reference path loss.
            %
            % Usage:
            %  SET_REFERENCE_PATH_LOSS(d0)
            %  SET_REFERENCE_PATH_LOSS(d0,unit)
            %
            % Args:
            %  L0: the path loss (linear power loss) measured at the 
            %  reference distance
            %  unit: (optional) a string specifying the unit of L0
            if nargin < 3 || isempty(unit)
                unit = 'dB';
            end
            if strcmpi(unit,'dB')
                L0 = 10.^(L0./10); % convert to linear power loss
            end
            obj.reference_path_loss = L0;
        end
        
        function atten = compute_path_loss_attenuation(obj,d)
            % COMPUTE_PATH_LOSS_ATTENUATION Computes the path attenuation 
            % (path loss) according to the free-space path loss (FSPL) 
            % formula.
            %
            % Usage:
            %  atten = compute_path_loss_attenuation()
            %  atten = compute_path_loss_attenuation(d)
            %
            % Args:
            %  d: (optional) distance(s) in meters to evaluate the path 
            %  loss at; if not passed, the path loss object's distance 
            %  property will be used
            %
            % Returns:
            %  atten: the path attenuation (path loss) (a power loss)
            %
            % Notes:
            %  : The path loss equation used is
            if nargin < 2 || isempty(d)
                d = obj.distance;
            end
            ples = obj.path_loss_exponents;
            L0 = obj.reference_path_loss;
            d0 = obj.reference_distance;
            num_dist = length(d);
            L = zeros(num_dist,1);
            idx = find(d <= d0);
            L(idx) = L0 .* (d(idx) ./ d0).^(ples(1));
            idx = find(d > d0);
            L(idx) = L0 .* (d(idx) ./ d0).^(ples(2));
            atten = L; % power attenuation/loss
        end
        
        function atten = realization(obj)
            % REALIZATION Invokes a realization of the path loss.
            %
            % Usage:
            %  atten = REALIZATION()
            %
            % Returns:
            %  atten: the realized path loss attenuation
            % 
            % Notes:
            %  : Since no random variables are involved in the FSPL equation,
            %  it will be determininstic and thus fixed across
            %  realizations.
            atten = obj.compute_path_loss_attenuation();
            obj.set_path_loss_attenuation(atten);
        end
    end
end