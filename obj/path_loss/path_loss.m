classdef path_loss < matlab.mixin.Copyable
    % PATH_LOSS A generic path loss model.
    properties
        name; % human-readable identifier
        type; % a string describing the type of path loss model
        distance; % distance of the path
        attenuation; % power loss of the path
        carrier_frequency; % carrier frequency (Hz)
        carrier_wavelength; % carrier wavelength (m)
        propagation_velocity; % propagation velocity (m/s)
    end
    methods(Static)
        function obj = create(type)
            % CREATE Creates a path loss object of a specific type.
            %
            % Usage:
            %  obj = path_loss.create()
            %  obj = path_loss.create(type)
            %
            % Args:
            %  type: (optional) a string specifying which path loss model
            %  to create
            %
            % Returns:
            %  obj: a path loss object
            if nargin < 1 || isempty(type)
                type = 'default';
            end
            if strcmpi(type,'default')
                obj = path_loss();
            elseif strcmpi(type,'free-space') || strcmpi(type,'free_space') || strcmpi(type,'FSPL')
                obj = path_loss_free_space();
            elseif strcmpi(type,'free-space-log-normal-shadowing') || strcmpi(type,'free_space_log_normal_shadowing') || strcmpi(type,'FSPL+LNS')
                obj = path_loss_free_space_log_normal_shadowing();
            elseif strcmpi(type,'two-slope') || strcmpi(type,'two_slope')
                obj = path_loss_two_slope();
            else
                error('Invalid path loss type specifier.');
            end
        end
    end
    methods
        function obj = path_loss(name)
            % PATH_LOSS Creates a path loss object.
            % 
            % Usage:
            %  obj = PATH_LOSS()
            %  obj = PATH_LOSS(name)
            % 
            % Args:
            %  name: an optional name for the object
            % 
            % Returns:
            %  obj: an object representing path loss
            if nargin < 1
                name = [];
            end
            obj.set_name(name);
            obj.initialize();
        end
        
        function initialize(obj)
            % INITIALIZE Initializes a path loss object.
            %
            % Usage:
            %  INITIALIZE()
            obj.set_carrier_frequency(1);
            obj.set_propagation_velocity(1);
            obj.set_type('default');
        end
        
        % -----------------------------------------------------------------
        % Set functions.
        % -----------------------------------------------------------------
        
        function set_name(obj,name)
            % SET_NAME Sets the name of the path loss model.
            %
            % Usage:
            %  SET_NAME()
            %  SET_NAME(name)
            %
            % Args:
            %  name: (optional) a string; if not passed, 'path-loss' is
            %  the default name used
            if nargin < 2 || isempty(name)
                name = 'path-loss';
            end
            obj.name = name;
        end
        
        function set_type(obj,type)
            % SET_TYPE Sets the type of path loss model.
            %
            % Usage:
            %  SET_TYPE()
            %  SET_TYPE(type)
            %
            % Args:
            %  type: (optional) a string; if not passed, 'default' is
            %  the default type used
            if nargin < 2 || isempty(type)
                type = 'default';
            end
            obj.type = type;
        end
        
        function set_distance(obj,d)
            % SET_DISTANCE Sets the distance of the path (in meters).
            %
            % Usage:
            %  SET_DISTANCE(d) Sets the distance of the link to a specific
            %  value.
            %
            % Args:
            %  d: distance of the path (in meters)
            if d < 0
                warning('Distance cannot be less than 0. Not setting distance.');
            else
                obj.distance = d;
            end
        end
        
        function set_path_loss_attenuation(obj,atten)
            % SET_PATH_LOSS_ATTENUATION Sets the attenuation of the path
            % loss model.
            %
            % Usage:
            %  SET_PATH_LOSS_ATTENUATION(atten)
            %
            % Args:
            %  atten: the attenuation (power loss) of the path
            if atten < 1
                warning('Attenuation is less than one.');
            end
            obj.attenuation = atten;
        end
        
        function set_propagation_velocity(obj,val)
            % SET_PROPAGATION_VELOCITY Sets the propagation velocity of the
            % channel. Also updates the carrier wavelength accordingly.
            %
            % Usage:
            %  SET_PROPAGATION_VELOCITY(val)
            %
            % Args:
            %  val: propagation velocity (meters/sec)
            obj.propagation_velocity = val;
            obj.carrier_wavelength = val / obj.carrier_frequency;
        end
        
        function set_carrier_frequency(obj,fc)
            % SET_CARRIER_FREQUENCY Sets the carrier frequency of the
            % channel. Also updates the carrier wavelength accordingly.
            %
            % Usage:
            %  SET_CARRIER_FREQUENCY(fc)
            %
            % Args:
            %  fc: carrier frequency (Hz)
            %
            % Notes:
            %  : Also updates carrier wavelength.
            obj.carrier_frequency = fc;
            obj.carrier_wavelength = obj.propagation_velocity / fc;
        end
           
        % -----------------------------------------------------------------
        % Get functions.
        % -----------------------------------------------------------------
        
%         function val = get_path_loss_attenuation(obj)
%             % GET_PATH_LOSS_ATTENUATION Returns the attenuation of the path
%             % loss model.
%             %
%             % Usage:
%             %  val = GET_PATH_LOSS_ATTENUATION()
%             %
%             % Returns:
%             %  val: the attenuation (power loss) of the path
%             val = obj.attenuation;
%         end
        
        function val = get_attenuation(obj)
            % GET_ATTENUATION Returns the realized attenuation of the path 
            % loss model.
            %
            % Usage:
            %  val = GET_ATTENUATION()
            %
            % Returns:
            %  val: the attenuation (power loss) of the path
            val = obj.attenuation;
        end
        
%         function atten = get_attenuation_old(obj,unit)
%             % GET_ATTENUATION Returns the path loss attenuation.
%             %
%             % Usage:
%             %  atten = GET_ATTENUATION()
%             %  atten = GET_ATTENUATION(unit)
%             %
%             % Args:
%             %  unit: (optional) a string specifying the unit to return the
%             %  attenuation in; 'dB' or 'log' for units of decibels
%             %
%             % Returns:
%             %  atten: path loss attenuation
%             if nargin < 2
%                 unit = 'linear';
%             end
%             atten = obj.attenuation;
%             if strcmpi(unit,'dB') || strcmpi(unit,'log')
%                 atten = 10 * log10(atten);
%             end
%         end
    end
end