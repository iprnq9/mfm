classdef path_loss_free_space_log_normal_shadowing < path_loss_free_space
    % PATH_LOSS_FREE_SPACE_LOG_NORMAL_SHADOWING A free-space path loss 
    % model with log-normal shadowing.
    properties
        % Inherited:
        % name; % human-readable identifier
        % type;
        % distance;
        % attenuation; % the power loss of the path
        % carrier_frequency; % carrier frequency (Hz)
        % carrier_wavelength; % carrier wavelength (m)
        % propagation_velocity; % propagation velocity (m/s)
        % path_loss_exponent; % path loss exponent
        log_normal_shadowing_variance;
    end
    methods
        function obj = path_loss_free_space_log_normal_shadowing(name)
            % PATH_LOSS_FREE_SPACE_LOG_NORMAL_SHADOWING Creates a 
            % free-space path loss with log-normal shadowing object.
            % 
            % Usage:
            %  obj = PATH_LOSS_FREE_SPACE_LOG_NORMAL_SHADOWING()
            %  obj = PATH_LOSS_FREE_SPACE_LOG_NORMAL_SHADOWING(name)
            % 
            % Args:
            %  name: an optional name for the object
            % 
            % Returns:
            %  obj: an object representing free-space path loss
            if nargin < 1 || isempty(name)
                name = 'path-loss-free-space-log-normal-shadowing';
            end
            obj.set_name(name);
            obj.initialize_free_space_log_normal_shadowing();
        end
        
        function initialize_free_space_log_normal_shadowing(obj)
            % PATH_LOSS_FREE_SPACE_LOG_NORMAL_SHADOWING Initializes a 
            % free-space path loss with log-normal shadowing object.
            % 
            % Usage:
            %  INITIALIZE_FREE_SPACE_LOG_NORMAL_SHADOWING()
            obj.initialize(); % default path loss initialization
            obj.initialize_free_space(); % free-space path loss intialization
            obj.set_type('free-space-log-normal-shadowing');
            obj.set_log_normal_shadowing_variance(1);
        end
                        
        % -----------------------------------------------------------------
        % Set functions.
        % -----------------------------------------------------------------
        
        function set_log_normal_shadowing_variance(obj,var)
            % SET_LOG_NORMAL_SHADOWING_VARIANCE Sets the log-normal
            % shadowing variance.
            %
            % Usage:
            %  SET_LOG_NORMAL_SHADOWING_VARIANCE(var)
            %
            % Args:
            %  var: log-normal shadowing variance (a quantity in dB)
            if var < 0
                warning('Log-normal shadowing variance should be non-negative. Setting to 0.');
                var = 0;
            end
            obj.log_normal_shadowing_variance = var;
        end
        
        function val = realization_log_normal_shadowing(obj)
            % REALIZATION_LOG_NORMAL_SHADOWING Realizes log-normal
            % shadowing based on the current log-normal shadowing variance.
            %
            % Usage:
            %  val = REALIZATION_LOG_NORMAL_SHADOWING()
            %
            % Returns:
            %  val: the realized shadowing factor
            var = obj.log_normal_shadowing_variance;
            val_dB = gauss_rv(0,var);
            val = 10^(val_dB/10);
        end
                
        function val = realization(obj)
            % REALIZATION Invokes a realization of the path loss.
            %
            % Usage:
            %  atten = REALIZATION()
            %
            % Returns:
            %  val: the realized path loss attenuation with shadowing
            atten = obj.compute_path_loss_attenuation(); % from FSPL
            shadowing = obj.realization_log_normal_shadowing();
            val = atten * shadowing;
            obj.set_path_loss_attenuation(val);
        end
    end
end