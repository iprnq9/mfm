classdef device < matlab.mixin.Copyable
    properties
        name; % human-readable identifier
        type; % 'transmitter', 'receiver', or 'transceiver'
        coordinate = [0,0,0]; % [x,y,z] coordinates
        transmitter; % transmitter object
        receiver; % receiver object
        marker; % plot marker
        duplexing; % full or half
        source;
        destination;
        carrier_frequency = 1;
        carrier_wavelength = 1;
        propagation_velocity = 1;
        % ---
        % transmit_strategy; % struct specifying the precoding strategy
        % receive_strategy; % struct specifying the combining strategy
        % transmit_csi; % transmit channel state information
        % receive_csi; % receive channel state information
        % transmit_symbol;
        % receive_symbol;
    end
    methods(Static)
        function obj = create(type,digital_hybrid)
            % CREATE Creates a device of specific type.
            %
            % Usage:
            %  obj = CREATE()
            %  obj = CREATE(type)
            %  obj = CREATE(type,digital_hybrid)
            %  obj = CREATE([],digital_hybrid)
            %
            % Args:
            %  type: (optional) 'transmitter', 'receiver', or 'transceiver'
            %  digital_hybrid: (optional) 'digital' or 'hybrid'
            %
            % Returns:
            %  obj: a device object of the desired type
            if nargin < 1
                type = [];
            end
            if nargin < 2
                digital_hybrid = [];
            end
            obj = device([],type,digital_hybrid);
        end
    end
    methods
        function obj = device(name,type,digital_hybrid)
            % DEVICE Creates a device object.
            % 
            % Usage:
            %  obj = DEVICE()
            %  obj = DEVICE(name)
            % 
            % Args:
            %  name: an optional name for the device
            % 
            % Returns:
            %  obj: an object representing a device
            if nargin < 1
                name = [];
            end
            if nargin < 2 || isempty(type)
                type = 'transceiver';
            end
            if nargin < 3 || isempty(digital_hybrid)
                digital_hybrid = 'digital';
            end
            obj.set_name(name);
            obj.set_type(type,digital_hybrid);
            obj.set_marker('kx');
            obj.set_duplexing('half');
            obj.set_carrier_frequency(1);
            obj.set_propagation_velocity(1);
            obj.set_coordinate([0,0,0]);
        end
        
        function set_name(obj,name)
            % SET_NAME Sets the device's name.
            %
            % Usage:
            %  SET_NAME()
            %  SET_NAME(name)
            if nargin < 2 || isempty(name)
                name = 'device';
            end
            obj.name = name;
        end
        
        function set_type(obj,type,dh)
            % SET_TYPE Sets the device as either a fully-digital or hybrid 
            % digital/analog transmitter, receiver, or transceiver.
            %
            % Usage:
            %  SET_TYPE(type)
            %  SET_TYPE(type,dh)
            % 
            % Args:
            %  type: a string specifying the type of device; either
            %  'transmitter', 'receiver', or 'transceiver' 
            %  dh: a string specifying the digital or hybrid 
            %  digital/analog beamforming nature of the device; either 
            %  'digital' or 'hybrid'
            %
            % Notes:
            %  : Overwrites any existing transmitter or receiver that is
            %  present in the device.
            if nargin < 3 || isempty(dh)
                dh = 'digital';
            end
            if ~strcmp(dh,'digital') && ~strcmp(dh,'hybrid')
                error('Invalid digital/hybrid specifier.');
            end
            if strcmp(type,'transmitter')
                if strcmp(dh,'digital')
                    tx = transmitter();
                else
                    tx = transmitter_hybrid();
                end
                rx = [];
                obj.type = 'transmitter';
            elseif strcmp(type,'receiver')
                if strcmp(dh,'digital')
                    rx = receiver();
                else
                    rx = receiver_hybrid();
                end
                tx = [];
                obj.type = 'receiver';
            elseif strcmp(type,'transceiver')
                if strcmp(dh,'digital')
                    tx = transmitter();
                    rx = receiver();
                else
                    tx = transmitter_hybrid();
                    rx = receiver_hybrid();
                end
                obj.type = 'transceiver';
            else
                error('Invalid device type.');
            end
            obj.set_transmitter(tx);
            obj.set_receiver(rx);
        end
        
        function set_coordinate(obj,x,y,z,move_arrays)
            % SET_COORDINATE Sets the device's position in 3-D space.
            % 
            % Usage:
            %  SET_COORDINATE()
            %  SET_COORDINATE(x)
            %  SET_COORDINATE(x,y)
            %  SET_COORDINATE(x,y,z)
            %  SET_COORDINATE(x,y,z,move_arrays)
            %
            % Args:
            %  x: x-coordinate of the device's position
            %  y: y-coordinate of the device's position
            %  z: z-coordinate of the device's position
            %  move_arrays: (optional) a boolean indicating if the array(s)
            %  should be moved (relatively) to the new coordinate; if not 
            %  passed, true is used
            %
            % Notes:
            %  : If no arguments are passed, the device's current coordinate
            %  will be used.
            % 
            %  : If only x and y are given, z is taken to be 0. Both x and y
            %  need to be singletons.
            %  
            %  : If only x is given and is a singleton, y and z are taken to
            %  be 0. If only x is given and has two elements, y = x(2), x =
            %  x(1), and z = 0. If only x is given and has three elements,
            %  z = x(3), y = x(2), and x = x(1).
            % 
            %  : All other options are invalid.
            %
            %  : When the arrays are shifted according to the device
            %  coordinate, they are shifted relative to the device's
            %  previous location. This maintains any relative geometry
            %  between the transmit and receive arrays, assuming they have
            %  already been added to the device before its coordinate is
            %  set.
            if nargin < 5 || isempty(move_arrays)
                move_arrays = true;
            end
            if nargin < 2 || isempty(x)
                x = obj.coordinate(1);
                y = obj.coordinate(2);
                z = obj.coordinate(3);
            else
                if nargin < 3
                    if length(x) == 1
                        y = 0;
                        z = 0;
                    elseif length(x) == 2
                        y = x(2);
                        x = x(1);
                        z = 0;
                    elseif length(x) == 3
                        z = x(3);
                        y = x(2);
                        x = x(1);
                    else
                        error('Invalid coordinate.')
                    end
                end
                if nargin < 4
                    if length(x) == 1 && length(y) == 1
                        z = 0;
                    else
                        error('Invalid coordinate.');
                    end
                end
            end
            old_coordinate = obj.coordinate;
            obj.coordinate = [x,y,z];
            if move_arrays
                loc = ([x,y,z] - old_coordinate) ./ obj.carrier_wavelength;
                if obj.istransmitter()
                    obj.transmitter.array.translate(loc(1),loc(2),loc(3));
                end
                if obj.isreceiver()
                    obj.receiver.array.translate(loc(1),loc(2),loc(3));
                end
            end
        end
        
        function set_marker(obj,marker)
            % SET_MARKER Sets the device marker.
            %
            % Usage:
            %  SET_MARKER(marker)
            %
            % Args:
            %  marker: a MATLAB-compliant plot marker
            obj.marker = marker;
        end
        
        function set_transmitter(obj,tx)
            % SET_TRANSMITTER Sets the transmitter for the device.
            %
            % Usage:
            %  SET_TRANSMITTER(tx)
            %
            % Args:
            %  tx: a transmitter object
            obj.transmitter = tx;
        end
        
        function set_receiver(obj,rx)
            % SET_RECEIVER Sets the receiver for the device.
            %
            % Usage:
            %  SET_RECEIVER(rx)
            %
            % Args:
            %  rx: a receiver object
            obj.receiver = rx;
        end
        
        function set_duplexing(obj,duplex)
            % SET_DUPLEXING Sets the device as either a half-duplex or
            % full-duplex device.
            %
            % Usage:
            %  SET_DUPLEXING(duplex)
            %
            % Args:
            %  duplex: either 'half' or 'full', specifying the duplexing
            %  nature of the device
            %
            % Notes:
            %  : Setting the device to be full-duplex does not bestow it the
            %  ability to transmit and receive simultaneously and in-band
            %  losslessly, but rather sets it to attempt to. Its
            %  performance when doing so will depend on the design of the 
            %  transmitter and receiver.
            if ~strcmp(duplex,'half') && ~strcmp(duplex,'full')
                warning('Invalid duplexing specifier. Setting to half-duplex device.');
                duplex = half;
            end
            obj.duplexing = duplex;
        end

        function set_source(obj,device_source)
            % SET_SOURCE Sets the source (the transmit device the device 
            % aims to serve).
            %
            % Usage:
            %  SET_SOURCE(device_source)
            %
            % Args:
            %  device_source: a device object 
            if obj.isreceiver()
                obj.source = device_source;
                obj.receiver.set_source(device_source);
            end
        end
        
        function set_destination(obj,device_destination)
            % SET_DESTINATION Sets the destination (the receive device the 
            % device aims to serve).
            %
            % Usage:
            %  SET_DESTINATION(device_destination)
            %
            % Args:
            %  device_destination: a device object 
            if obj.istransmitter()
                obj.destination = device_destination;
                obj.transmitter.set_destination(device_destination);
            end
        end
        
        function set_propagation_velocity(obj,val)
            % SET_PROPAGATION_VELOCITY Sets the propagation velocity of the
            % signals the device.
            %
            % Usage:
            %  SET_PROPAGATION_VELOCITY(val)
            %
            % Args:
            %  val: propagation velocity (meters/sec)
            obj.propagation_velocity = val;
            obj.carrier_wavelength = val ./ obj.carrier_frequency;
        end
        
        function set_carrier_frequency(obj,fc)
            % SET_CARRIER_FREQUENCY Sets the carrier frequency of the
            % device.
            %
            % Usage:
            %  SET_CARRIER_FREQUENCY(fc)
            %
            % Args:
            %  fc: carrier frequency (Hz)
            %
            % Notes:
            %  : Also updates carrier wavelength.
            obj.carrier_frequency = fc;
            obj.carrier_wavelength = obj.propagation_velocity ./ fc;
        end
        
        % -----------------------------------------------------------------
        % Misc. functions.
        % -----------------------------------------------------------------
        
        function turn_off_transmit(obj)
            % TURN_OFF_TRANSMIT Turns off the device's transmitter.
            %
            % Usage:
            %  TURN_OFF_TRANSMIT()
            if obj.istransmitter()
                obj.transmitter.turn_off();
            end
        end
        
        function turn_off_receive(obj)
            % TURN_OFF_RECEIVE Turns off the device's receiver.
            %
            % Usage:
            %  TURN_OFF_RECEIVE()
            if obj.isreceiver()
                obj.receiver.turn_off();
            end
        end
        
        function out = istransmitter(obj)
            % ISTRANSMITTER Returns a boolean indicating if the device has
            % transmit capability (i.e., is either a transmitter or
            % transceiver type).
            %
            % Usage:
            %  out = ISTRANSMITTER()
            %
            % Returns:
            %  out: a boolean
            type = obj.type;
            out = strcmpi(type,'transmitter');
            out = out || obj.istransceiver();
        end
        
        function out = isreceiver(obj)
            % ISRECEIVER Returns a boolean indicating if the device has
            % receive capability (i.e., is either a receiver or
            % transceiver type).
            %
            % Usage:
            %  out = ISRECEIVER()
            %
            % Returns:
            %  out: a boolean
            type = obj.type;
            out = strcmpi(type,'receiver');
            out = out || obj.istransceiver();
        end
        
        function out = istransceiver(obj)
            % ISTRANSCEIVER Returns a boolean indicating if the device has
            % transmit and receive capability (i.e., is a transceiver).
            %
            % Usage:
            %  out = ISTRANSCEIVER()
            %
            % Returns:
            %  out: a boolean
            type = obj.type;
            out = strcmpi(type,'transceiver');
        end

        % -----------------------------------------------------------------
        % Passthrough functions.
        % -----------------------------------------------------------------
        
        function set_precoder(obj,F)
            % SET_PRECODER Sets the precoding matrix used at the 
            % transmitter.
            %
            % Usage:
            %  SET_PRECODER(F)
            %
            % Args:
            %  F: a precoding matrix
            if obj.istransmitter()
                obj.transmitter.set_precoder(F);
            end
        end
        
        function set_combiner(obj,W)
            % SET_COMBINER Sets the combining matrix used at the 
            % receiver.
            %
            % Usage:
            %  SET_COMBINER(W)
            %
            % Args:
            %  W: a combining matrix
            if obj.isreceiver()
                obj.receiver.set_combiner(W);
            end
        end
        
        function set_symbol_bandwidth(obj,B)
            % SET_SYMBOL_BANDWIDTH Sets the symbol bandwidth at the
            % transmitter and receiver.
            %
            % Usage:
            %  SET_SYMBOL_BANDWIDTH(B)
            %
            % Args:
            %  B: the symbol bandwidth (in Hertz)
            obj.set_transmit_symbol_bandwidth(B);
            obj.set_receive_symbol_bandwidth(B);
        end
        
        function set_transmit_symbol_bandwidth(obj,B)
            % SET_TRANSMIT_SYMBOL_BANDWIDTH Sets the symbol bandwidth at the
            % transmitter.
            %
            % Usage:
            %  SET_TRANSMIT_SYMBOL_BANDWIDTH(B)
            %
            % Args:
            %  B: the symbol bandwidth (in Hertz)
            if obj.istransmitter()
                obj.transmitter.set_symbol_bandwidth(B);
            end
        end
        
        function set_receive_symbol_bandwidth(obj,B)
            % SET_RECEIVE_SYMBOL_BANDWIDTH Sets the symbol bandwidth at the
            % receiver.
            %
            % Usage:
            %  SET_RECEIVE_SYMBOL_BANDWIDTH(B)
            %
            % Args:
            %  B: the symbol bandwidth (in Hertz)
            if obj.isreceiver()
                obj.receiver.set_symbol_bandwidth(B);
            end
        end
               
        function set_num_streams(obj,Ns)
            % SET_NUM_STREAMS Sets the number of streams at the transmitter
            % and receiver.
            %
            % Usage:
            %  SET_NUM_STREAMS(num_streams)
            %
            % Args:
            %  num_streams: number of streams to multiplex at the
            %  transmitter and receiver
            obj.set_transmit_num_streams(Ns);
            obj.set_receive_num_streams(Ns);
        end
        
        function set_transmit_num_streams(obj,Ns)
            % SET_NUM_STREAMS Sets the number of streams at the 
            % transmitter.
            %
            % Usage:
            %  SET_NUM_STREAMS(Ns)
            %
            % Args:
            %  Ns: number of streams to multiplex at the
            %  transmitter
            if obj.istransmitter()
                obj.transmitter.set_num_streams(Ns);
            end
        end
        
        function set_receive_num_streams(obj,Ns)
            % SET_NUM_STREAMS Sets the number of streams at the 
            % receiver.
            %
            % Usage:
            %  SET_NUM_STREAMS(Ns)
            %
            % Args:
            %  Ns: number of streams to multiplex at the
            %  receiver
            if obj.isreceiver()
                obj.receiver.set_num_streams(Ns);
            end
        end
        
        function set_transmit_symbol(obj,s)
            % SET_TRANSMIT_SYMBOL Sets the transmit symbol vector.
            %
            % Usage:
            %  SET_TRANSMIT_SYMBOL(s)
            %
            % Args:
            %  s: transmit symbol vector
            if obj.istransmitter()
                obj.transmitter.set_transmit_symbol(s);
            end
        end
        
        function set_receive_symbol(obj,s)
            % SET_RECEIVE_SYMBOL Sets the receive symbol vector.
            %
            % Usage:
            %  SET_RECEIVE_SYMBOL(s)
            %
            % Args:
            %  s: receive symbol vector
            if obj.isreceiver()
                obj.receiver.set_receive_symbol(s);
            end
        end
        
        function set_received_signal(obj,y)
            % SET_RECEIVED_SIGNAL Sets the received vector.
            %
            % Usage:
            %  SET_RECEIVED_SIGNAL(y)
            %
            % Args:
            %  y: received signal vector
            if obj.isreceiver()
                obj.receiver.set_received_signal(y);
            end
        end
        
        function set_noise(obj,n)
            % SET_NOISE Sets the noise vector at the receive array.
            %
            % Usage:
            %  SET_NOISE()
            %  SET_NOISE(n)
            %
            % Args:
            %  n: (optional) a noise vector; if not passed, noise will be
            %  generated according to the noise power
            if nargin < 2
                n = [];
            end
            if obj.isreceiver()
                obj.receiver.set_noise(n);
            end
        end
        
        function set_arrays(obj,transmit_array,receive_array)
            % SET_ARRAYS Sets the transmit and receive arrays.
            %
            % Usage:
            %  SET_ARRAYS(transmit_array)
            %  SET_ARRAYS(transmit_array,receive_array)
            %
            % Args:
            %  transmit_array: array object to use at the transmitter
            %  receive_array: array object to use at the receiver; if not 
            %  passed, the transmit array is used
            if nargin < 3 || isempty(receive_array)
                receive_array = transmit_array;
            end
            obj.set_transmit_array(transmit_array);
            obj.set_receive_array(receive_array);
        end
        
        function set_transmit_array(obj,array)
            % SET_TRANSMIT_ARRAY Sets the transmit array.
            %
            % Usage:
            %  SET_TRANSMIT_ARRAY(array)
            %
            % Args:
            %  array: array object to use at the transmitter
            if obj.istransmitter()
                obj.transmitter.set_array(array);
                loc = obj.coordinate ./ obj.carrier_wavelength;
                obj.transmitter.array.translate(loc(1),loc(2),loc(3));
            end
        end
        
        function set_receive_array(obj,array)
            % SET_RECEIVE_ARRAY Sets the receive array.
            %
            % Usage:
            %  SET_RECEIVE_ARRAY(array)
            %
            % Args:
            %  array: array object to use at the receiver
            if obj.isreceiver()
                obj.receiver.set_array(array);
                loc = obj.coordinate ./ obj.carrier_wavelength;
                obj.receiver.array.translate(loc(1),loc(2),loc(3));
            end
        end       
        
        function set_num_rf_chains(obj,Lt,Lr)
            % SET_NUM_RF_CHAINS Sets the number of transmit and receive RF
            % chains at the device.
            %
            % Usage:
            %  SET_NUM_RF_CHAINS(Lt,Lr)
            %
            % Args:
            %  Lt: number of transmit RF chains
            %  Lr: (optional) number of receive RF chains; if not passed,
            %  the number of transmit RF chains is used
            if nargin < 3 || isempty(Lr)
                Lr = Lt;
            end
            obj.set_transmit_num_rf_chains(Lt);
            obj.set_receive_num_rf_chains(Lr);
        end
        
        function set_transmit_num_rf_chains(obj,Lt)
            % SET_TRANSMIT_NUM_RF_CHAINS Sets the number of transmit RF 
            % chains at the device.
            %
            % Usage:
            %  SET_TRANSMIT_NUM_RF_CHAINS(Lt)
            %
            % Args:
            %  Lt: number of transmit RF chains
            if obj.istransmitter()
                obj.transmitter.set_num_rf_chains(Lt);
            end
        end
        
        function set_receive_num_rf_chains(obj,Lr)
            % SET_RECEIVE_NUM_RF_CHAINS Sets the number of receive RF 
            % chains at the device.
            %
            % Usage:
            %  SET_RECEIVE_NUM_RF_CHAINS(Lr)
            %
            % Args:
            %  Lr: number of receive RF chains
            if obj.isreceiver()
                obj.receiver.set_num_rf_chains(Lr);
            end
        end
               
        function set_transmit_power(obj,P,unit)
            % SET_TRANSMIT_POWER Sets the transmit power of the device.
            %
            % Usage:
            %  SET_TRANSMIT_POWER(P)
            %  SET_TRANSMIT_POWER(P,unit)
            %
            % Args:
            %  P: transmit power (e.g., in watts or dBm)
            %  unit: (optional) a string specifying the units of P (e.g., 
            %  'dBm'); if not passed, the default is used.
            if nargin < 3
                unit = [];
            end
            if obj.istransmitter()
                obj.transmitter.set_transmit_power(P,unit);
            end
        end
        
        function set_transmit_symbol_covariance(obj,Rs)
            % SET_TRANSMIT_SYMBOL_COVARIANCE Sets the transmit covariance
            % of the device.
            %
            % Usage:
            %  SET_TRANSMIT_SYMBOL_COVARIANCE(Rs)
            %
            % Args:
            %  Rs: a covariance matrix
            if obj.istransmitter()
                obj.transmitter.set_transmit_symbol_covariance(Rs);
            end
        end
               
        function set_noise_power_per_Hz(obj,noise_psd,unit)
            % set_noise_power_per_Hz Sets the noise power per Hz at the
            % receiver.
            %
            % Usage:
            %  set_noise_power_per_Hz(noise_psd)
            %  set_noise_power_per_Hz(noise_psd,unit)
            %
            % Args:
            %  noise_psd: the noise power spectral density
            %  unit: (optional) a string specifying the units of noise_psd 
            %  (e.g., 'dBm_Hz' or 'watts_Hz'); if not passed, the default
            %  will be used
            if nargin < 3
                unit = [];
            end
            if obj.isreceiver()
                obj.receiver.set_noise_power_per_Hz(noise_psd,unit);
            end
        end
               
        function set_transmit_channel_state_information(obj,csi)
            % SET_TRANSMIT_CHANNEL_STATE_INFORMATION Sets the channel state
            % information at the transmitter.
            %
            % Usage:
            %  SET_TRANSMIT_CHANNEL_STATE_INFORMATION(csi)
            %
            % Args:
            %  csi: a struct of channel state information
            if obj.istransmitter()
                obj.transmitter.set_channel_state_information(csi);
            end
        end
        
        function set_receive_channel_state_information(obj,csi)
            % SET_RECEIVE_CHANNEL_STATE_INFORMATION Sets the channel state
            % information at the receiver.
            %
            % Usage:
            %  SET_RECEIVE_CHANNEL_STATE_INFORMATION(csi)
            %
            % Args:
            %  csi: a struct of channel state information
            if obj.isreceiver()
                obj.receiver.set_channel_state_information(csi);
            end
        end
               
        function set_transmit_strategy(obj,strategy)
            % SET_TRANSMIT_STRATEGY Sets the transmit strategy.
            %
            % Usage:
            %  SET_TRANSMIT_STRATEGY(strategy)
            %
            % Args:
            %  strategy: a string specifying the transmit strategy
            
            % obj.transmit_strategy = strategy;
            obj.transmitter.set_transmit_strategy(strategy);
        end
        
        function set_receive_strategy(obj,strategy)
            % SET_RECEIVE_STRATEGY Sets the receive strategy.
            %
            % Usage:
            %  SET_RECEIVE_STRATEGY(strategy)
            %
            % Args:
            %  strategy: a string specifying the receive strategy
            obj.receiver.set_receive_strategy(strategy);
        end
               
        function configure_transmitter(obj,strategy)
            % CONFIGURE_TRANSMITTER Configures the device's transmitter
            % according to the current/specified transmit strategy, 
            % incorporating channel state information as applicable.
            %
            % Usage:
            %  CONFIGURE_TRANSMITTER()
            %  CONFIGURE_TRANSMITTER(strategy)
            %
            % Args:
            %  strategy: (optional) transmit strategy to use; if not
            %  passed, the current transmit strategy will be used
            if obj.istransmitter()
                if nargin < 2
                    strategy = [];
                end
                obj.transmitter.configure_transmitter(strategy);
            end
        end
        
        function configure_receiver(obj,strategy)
            % CONFIGURE_RECEIVER Configures the device's receiver
            % according to the current/specified receive strategy, 
            % incorporating channel state information as applicable.
            %
            % Usage:
            %  CONFIGURE_RECEIVER()
            %  CONFIGURE_RECEIVER(strategy)
            %
            % Args:
            %  strategy: (optional) receive strategy to use; if not
            %  passed, the current receive strategy will be used
            if obj.isreceiver()
                if nargin < 2
                    strategy = [];
                end
                obj.receiver.configure_receiver(strategy);
            end
        end
                      
        % -----------------------------------------------------------------
        % Show functions.
        % -----------------------------------------------------------------
        
        function [fig,ax] = show_transmit_receive_arrays_3d(obj,fignum)
            % SHOW_TRANSMIT_RECEIVE_ARRAYS_3D Plots the transmit array and
            % receive array in 3-D space.
            %
            % Usage:
            %  [fig,ax] = SHOW_TRANSMIT_RECEIVE_ARRAYS_3D()
            %  [fig,ax] = SHOW_TRANSMIT_RECEIVE_ARRAYS_3D(fignum)
            % 
            % Args:
            %  fignum: optional figure number to use when creating plotting
            %
            % Returns:
            %  fig: a figure handle of the plot
            %  ax: an axis handle of the plot
            if nargin < 2 || isempty(fignum)
                fig = figure();
            else
                fig = figure(fignum);
            end
            ax = gca();
            hold(ax,'off');
            obj.transmitter.array.show3d(ax);
            hold(ax,'on');
            obj.receiver.array.show3d(ax);
            hold(ax,'off');
            grid(ax,'on');
            xlabel(ax,'$x$ (wavelengths)');
            ylabel(ax,'$y$ (wavelengths)');
            zlabel(ax,'$z$ (wavelengths)');
        end
        
        % -----------------------------------------------------------------
        % Shorthand functions.
        % -----------------------------------------------------------------
        
        function val = F(obj)
            % F Returns the precoding matrix.
            %
            % Usage:
            %  val = F()
            %
            % Returns:
            %  val: the precoding matrix
            if obj.istransmitter()
                val = obj.transmitter.precoder;
            end
        end       
        
        function val = W(obj)
            % W Returns the combining matrix.
            %
            % Usage:
            %  val = W()
            %
            % Returns:
            %  val: the combining matrix
            if obj.isreceiver()
                val = obj.receiver.combiner;
            end
        end
        
        function val = Nt(obj)
            % Nt Returns the number of transmit antennas.
            %
            % Usage:
            %  val = Nt()
            %
            % Returns:
            %  val: the number of transmit antennas
            if obj.istransmitter()
                val = obj.transmitter.num_antennas;
            end
        end
        
        function val = Nr(obj)
            % Nr Returns the number of receive antennas.
            %
            % Usage:
            %  val = Nr()
            %
            % Returns:
            %  val: the number of receive antennas
            if obj.isreceiver()
                val = obj.receiver.num_antennas;
            end
        end
        
        function val = Ns_tx(obj)
            % Ns_tx Returns the number of streams at the transmitter.
            %
            % Usage:
            %  val = Ns_tx()
            %
            % Returns:
            %  val: the number of streams at the transmitter
            if obj.istransmitter()
                val = obj.transmitter.num_streams;
            end
        end
        
        function val = Ns_rx(obj)
            % Ns_rx Returns the number of streams at the receiver.
            %
            % Usage:
            %  val = Ns_rx()
            %
            % Returns:
            %  val: the number of streams at the receiver
            if obj.isreceiver()
                val = obj.receiver.num_streams;
            end
        end
        
        function val = s_tx(obj)
            % s_tx Returns the transmit symbol vector.
            %
            % Usage:
            %  val = s_tx()
            %
            % Returns:
            %  val: the transmit symbol vector
            if obj.istransmitter()
                val = obj.transmitter.transmit_symbol;
            end
        end
        
        function val = x(obj)
            % x Returns the transmitted signal vector.
            %
            % Usage:
            %  val = x()
            %
            % Returns:
            %  val: the transmitted signal vector
            if obj.istransmitter()
                val = obj.transmitter.transmitted_signal;
            end
        end
        
        function val = s_rx(obj)
            % s_rx Returns the receive symbol vector.
            %
            % Usage:
            %  val = s_rx()
            %
            % Returns:
            %  val: the receive symbol vector
            if obj.isreceiver()
                val = obj.receiver.receive_symbol;
            end
        end
        
        function val = y(obj)
            % y Returns the received signal vector.
            %
            % Usage:
            %  val = y()
            %
            % Returns:
            %  val: the received signal vector
            if obj.isreceiver()
                val = obj.receiver.received_signal;
            end
        end
        
        function val = n(obj)
            % n Returns the noise vector (per-antenna).
            %
            % Usage:
            %  val = n()
            %
            % Returns:
            %  val: the noise vector (per-antenna)
            if obj.isreceiver()
                val = obj.receiver.noise;
            end
        end

        function val = Rs(obj)
            % Rs Returns the transmit symbol covariance matrix at the
            % transmitter.
            %
            % Usage:
            %  val = Rs()
            %
            % Returns:
            %  val: the transmit symbol covariance matrix at the
            %  transmitter
            if obj.istransmitter()
                val = obj.transmitter.transmit_symbol_covariance;
            end
        end
        
        function val = Rn(obj)
            % Rn Returns the noise covariance matrix at the receiver.
            %
            % Usage:
            %  val = Rn()
            %
            % Returns:
            %  val: the noise covariance matrix
            if obj.isreceiver()
                val = obj.receiver.noise_covariance;
            end
        end
        
        function val = N0(obj)
            % N0 Returns the noise energy per symbol of the receiver.
            %
            % Usage:
            %  val = N0()
            %
            % Returns:
            %  val: the noise energy per symbol
            if obj.isreceiver()
                val = obj.receiver.noise_energy_per_symbol;
            end
        end
    end
end
        