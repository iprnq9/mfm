classdef network_mfm < matlab.mixin.Copyable
    properties
        name;
        devices;
        num_devices;
        links;
        num_links;
        sources;
        num_sources;
        destinations;
        num_destinations;
        propagation_velocity = 1;
        carrier_frequency = 1;
        carrier_wavelength = 1;
        symbol_bandwidth = 1;
        channel_state_information_transmit;
        channel_state_information_receive;
    end
    methods(Static)
        function obj = create()
            % CREATE Creates a network object.
            % 
            % Usage:
            %  obj = network_mfm.create()
            %
            % Returns:
            %  obj: an MFM network object
            obj = network_mfm();
        end
    end
    methods
        function obj = network_mfm(name)
            % NETWORK Creates a network object.
            %
            % Usage:
            %  obj = NETWORK()
            %  obj = NETWORK(name)
            %
            % Args:
            %  name: an optional name for the network
            %
            % Returns:
            %  obj: an object representing a network
            if nargin < 1
                name = [];
            end
            obj.set_name(name);
            obj.initialize();
        end
        
        function initialize(obj)
            % INITIALIZE Executes intialization steps to set up a network.
            %
            % Usage:
            %  INITIALIZE()
            obj.num_devices = 0;
            obj.devices = {};
            obj.links = {};
            obj.num_links = 0;
            obj.sources = {};
            obj.num_sources = 0;
            obj.destinations = {};
            obj.num_destinations = 0;
        end
        
        % -----------------------------------------------------------------
        % Set functions.
        % -----------------------------------------------------------------
        
        function set_name(obj,name)
            % SET_NAME Sets the name of the network.
            %
            % Usage:
            %  SET_NAME()
            %  SET_NAME(name)
            %
            % Args:
            %  name: (optional) a string; if not passed, a name is created
            if nargin < 2 || isempty(name)
                name = 'network';
            end
            obj.name = name;
        end
        
        function idx = get_link_index(obj,lnk)
            % GET_LINK_INDEX Returns the index of a link within the links
            % in the network.
            %
            % Usage:
            %  idx = GET_LINK_INDEX(lnk)
            %
            % Args:
            %  lnk: a link object or index of a link
            %
            % Returns:
            %  idx: the index of the link
            if isnumeric(lnk)
                lnk = round(lnk);
                if lnk < 1 || lnk > obj.num_links
                    error('Invalid link index.');
                else
                    idx = lnk;
                end
            elseif isa(lnk,'link')
                idx = find_cell(lnk,obj.links);
                if isempty(idx)
                    warning('Link not found in network. Returning 0.');
                    idx = 0;
                end
            else
                error('Invalid link format.');
            end
        end
        
        % -----------------------------------------------------------------
        % Get functions.
        % -----------------------------------------------------------------
                
        function add_link(obj,lnk)
            % ADD_LINK Adds a link to the network.
            %
            % Usage:
            %  ADD_LINK(lnk)
            %
            % Args:
            %  lnk: a link object
            [~,idx_log] = find_cell(obj.links,lnk);
            if any(idx_log)
                link_already_exists = true;
            else
                link_already_exists = false;
            end
            if ~link_already_exists
                obj.num_links = obj.num_links + 1;
                N = obj.num_links;
                M = cell(N,1);
                S = obj.links;
                M(1:N-1,1) = S;
                M{N,1} = lnk;
                obj.links = M;
                obj.add_device(lnk.head);
                obj.add_device(lnk.tail);
            else
                warning('Link already exists in network, not duplicating it.');
            end            
        end
        
        function add_links(obj,links)
            % ADD_LINKS Adds a vector of links to the network.
            %
            % Usage:
            %  ADD_LINKS(links)
            %
            % Args:
            %  links: a vector of link objects
            N = length(links);
            for i = 1:N
                if iscell(links)
                    obj.add_link(links{i});
                else
                    obj.add_link(links(i));
                end
            end
        end
        
        function remove_link(obj,lnk)
            % REMOVE_LINK Removes a link from the network.
            %
            % Usage:
            %  REMOVE_LINK()
            %  REMOVE_LINK(lnk)
            %
            % Args:
            %  lnk: (optional) link index or object to remove from the 
            %  network; if not passed, the last link in the network is
            %  removed
            if nargin < 2
                lnk = obj.num_links;
            end
            lnk = obj.get_link_index(lnk);
            obj.links = {obj.links{1:lnk-1} obj.links{lnk+1:end}};
            obj.num_links = obj.num_links - 1;
        end
        
        function remove_all_links(obj)
            % REMOVE_ALL_LINKS Removes all links from the network.
            %
            % Usage:
            %  REMOVE_ALL_LINKS()
            obj.links = {};
            obj.num_links = 0;
        end
        
        function remove_all_devices(obj)
            % REMOVE_ALL_DEVICES Removes all devices from the network.
            %
            % Usage:
            %  REMOVE_ALL_DEVICES()
            obj.devices = {};
            obj.num_devices = 0;
        end
        
        function idx = get_device_index(obj,dev)
            % GET_DEVICE_INDEX Returns the index of a device within the
            % devices in the network.
            %
            % Usage:
            %  idx = GET_DEVICE_INDEX(dev)
            %
            % Args:
            %  dev: a device object or index of a device
            %
            % Returns:
            %  idx: the index of the device
            if isnumeric(dev)
                dev = round(dev);
                if dev < 1 || dev > obj.num_devices
                    error('Invalid device index.');
                else
                    idx = dev;
                end
            elseif isa(dev,'device')
                idx = find_cell(dev,obj.devices);
                if isempty(idx)
                    warning('Device not found in network. Returning 0.');
                    idx = 0;
                end
            else
                error('Invalid device format.');
            end
        end
        
        function add_device(obj,device)
            % ADD_DEVICE Adds a device to the network.
            %
            % Usage:
            %  ADD_DEVICE(device)
            %
            % Args:
            %  device: a device object
            [~,idx_log] = find_cell(obj.devices,device);
            if any(idx_log)
                device_already_exists = true;
            else
                device_already_exists = false;
            end
            if ~device_already_exists
                obj.num_devices = obj.num_devices + 1;
                N = obj.num_devices;
                M = cell(N,1);
                S = obj.devices;
                M(1:N-1,1) = S;
                M{N,1} = device;
                obj.devices = M;
            end
        end
        
        function add_devices(obj,devices)
            % ADD_DEVICES Adds a vector of devices to the network.
            %
            % Usage:
            %  ADD_DEVICES(devices)
            %
            % Args:
            %  devices: a vector (or cell) of device objects
            N = length(devices);
            for i = 1:N
                if iscell(devices)
                    obj.add_device(devices{i});
                else
                    obj.add_device(devices(i));
                end
            end
        end
        
        function remove_device(obj,idx)
            % REMOVE_DEVICE Removes a device from the network.
            %
            % Usage:
            %  REMOVE_DEVICE()
            %  REMOVE_DEVICE(idx)
            %
            % Args:
            %  idx: optional device index to remove from the network (default
            %  is the last device in the network)
            if nargin < 2
                idx = obj.num_devices;
            end
            if isa(idx,'device')
                dev = idx;
                idx = obj.get_device_index(idx);
            else
                dev = obj.devices{idx};
            end
            obj.remove_all_links_with_device(dev);
            obj.remove_all_source_destination_with_device(dev);
            obj.devices = {obj.devices{1:idx-1} obj.devices{idx+1:end}};
            obj.num_devices = obj.num_devices - 1;
        end
        
        function remove_all_links_with_device(obj,dev)
            % REMOVE_ALL_LINKS_WITH_DEVICE Removes all links having a
            % particular device as its head or tail.
            %
            % Usage:
            %  REMOVE_ALL_LINKS_WITH_DEVICE(dev)
            %
            % Args:
            %  dev: a device object
            lnks = {};
            i = 1;
            for idx = 1:obj.num_links
                lnk = obj.links{idx};
                if isequal(lnk.head,dev) || isequal(lnk.tail,dev)
                    lnks{i} = lnk;
                    i = i + 1;
                end
            end
            for i = 1:length(lnks)
                obj.remove_link(lnks{i});
            end
        end
        
        function remove_all_source_destination_with_device(obj,dev)
            % REMOVE_ALL_SOURCE_DESTINATION_WITH_DEVICE Removes all
            % source-destination pairs having a particular device as the
            % source or destination.
            %
            % Usage:
            %  REMOVE_ALL_SOURCE_DESTINATION_WITH_DEVICE(dev)
            %
            % Args:
            %  dev: a device object
            i = 1;
            srcs = {};
            dests = {};
            for idx = 1:obj.num_sources
                src = obj.sources{idx};
                dest = obj.destinations{idx};
                if isequal(dev,src) || isequal(dev,dest)
                    srcs{i} = src;
                    dests{i} = dest;
                    i = i + 1;
                end
            end
            for i = 1:length(srcs)
                source = srcs{i};
                destination = dests{i};
                obj.remove_source_destination(source,destination);
            end
        end
        
        function populate_devices_from_links(obj)
            % POPULATE_DEVICES_FROM_LINKS Populates the devices in the
            % network from the links in the network.
            %
            % Usage:
            %  POPULATE_DEVICES_FROM_LINKS()
            obj.remove_all_devices();
            for idx = 1:obj.num_links
                head = obj.links{idx}.head;
                tail = obj.links{idx}.tail;
                obj.add_devices([head,tail]);
            end
        end
        
        function populate_links_from_devices(obj)
            % POPULATE_LINKS_FROM_DEVICES Populates the links in the
            % network from the devices in the network by linking each pair 
            % of devices.
            %
            % Usage:
            %  POPULATE_LINKS_FROM_DEVICES()
            obj.remove_all_links();
            for idx_head = 1:obj.num_devices
                head = obj.devices{idx_head};
                for idx_tail = idx_head:obj.num_devices
                    tail = obj.devices{idx_tail};
                    if ~isequal(head,tail)
                        lnk = link(head,tail);
                        obj.add_link(lnk);
                    end
                end
            end
        end
        
        function populate_links_from_source_destination(obj)
            % POPULATE_LINKS_FROM_SOURCE_DESTINATION Populates the links in
            % the network from the sources and destinations in the network 
            % by linking each pair of devices. Removes all links before
            % populating links.
            %
            % Usage:
            %  POPULATE_LINKS_FROM_SOURCE_DESTINATION()
            obj.remove_all_links();
            N = obj.num_sources;
            M = obj.num_destinations;
            if isequal(M,N)
                for idx_source = 1:N
                    source = obj.sources{idx_source};
                    for idx_destination = 1:M
                        destination = obj.destinations{idx_destination};
                        lnk = link(source,destination);
                        if isequal(idx_source,idx_destination)
                            lnk.set_color('k');
                            lnk.set_line_style('-');
                        else
                            lnk.set_color('r');
                            lnk.set_line_style('--');
                        end
                        obj.add_link(lnk);
                    end
                end
            else
                error('Number of sources and number of destinations in network must be equal.');
            end
        end
                
        function add_source(obj,source)
            % ADD_SOURCE Adds a source device to the network.
            %
            % Usage:
            %  ADD_SOURCE(source)
            %
            % Args:
            %  source: a device object
            obj.num_sources = obj.num_sources + 1;
            N = obj.num_sources;
            M = cell(N,1);
            S = obj.sources;
            M(1:N-1,1) = S;
            M{N,1} = source;
            obj.sources = M;
            obj.add_device(source);
        end
        
        function add_destination(obj,destination)
            % ADD_DESTINATION Adds a destination device to the network.
            % 
            % Usage:
            %  ADD_DESTINATION(destination)
            %
            % Args:
            %  destination: a device object
            obj.num_destinations = obj.num_destinations + 1;
            N = obj.num_destinations;
            M = cell(N,1);
            S = obj.destinations;
            M(1:N-1,1) = S;
            M{N,1} = destination;
            obj.destinations = M;
            obj.add_device(destination);
        end
        
        function add_source_destination(obj,source,destination)
            % ADD_SOURCE_DESTINATION Adds a source-destination pair to the
            % network.
            % 
            % Usage:
            %  ADD_SOURCE_DESTINATION(source,destination)
            %
            % Args:
            %  source: a device object to be the source
            %  destination: a device object to be the destination
            if source.istransmitter() && destination.isreceiver()
                source.set_destination(destination);
                destination.set_source(source);
                obj.add_source(source);
                obj.add_destination(destination);
            else
                warning('Source must be a transmitter and destination must be a receiver.');
                warning('Not adding source-destination pair.');
            end
        end
        
        function remove_source_destination(obj,source,destination)
            % REMOVE_SOURCE_DESTINATION Removes a source-destination pair 
            % from the network.
            % 
            % Usage:
            %  REMOVE_SOURCE_DESTINATION(source,destination)
            %
            % Args:
            %  source: a device object
            %  destination: a device object
            [idx_source,idx_log] = find_cell(obj.sources,source);
            [idx_destination,~] = find_cell(obj.destinations,destination);
            if isequal(idx_source,idx_destination) && logical(sum(idx_log,'all'))
                idx = idx_source;
                obj.sources = {obj.sources{1:idx-1} obj.sources{idx+1:end}};
                obj.num_sources = obj.num_sources - 1;
                obj.destinations = {obj.destinations{1:idx-1} obj.destinations{idx+1:end}};
                obj.num_destinations = obj.num_destinations - 1;
                source.destination = [];
                destination.source = [];
            else
                warning('Source-destination pair not found in network.');
            end
        end
        
        function remove_all_source_destination(obj)
            % REMOVE_ALL_SOURCE_DESTINATION Removes all source-destination
            % pairs from the network.
            %
            % Usage: 
            %  REMOVE_ALL_SOURCE_DESTINATION()
            obj.sources = {};
            obj.num_sources = 0;
            obj.destinations = {};
            obj.num_destinations = 0;
        end

        function compute_channel_state_information(obj)
            % COMPUTE_CHANNEL_STATE_INFORMATION Computes the channel state
            % information for each device in the network. For each device,
            % the CSI is collected across all links in which the device is
            % the transmitter (head on the forward link, tail on the
            % reverse link) or receiver (tail on the forward link, head on
            % the reverse link).
            %
            % Usage:
            %  COMPUTE_CHANNEL_STATE_INFORMATION()
            N = obj.num_sources;
            M = obj.num_destinations;
            if isequal(M,N)
                csi_transmit = cell(N,1);
                csi_receive = cell(N,1);
                for idx = 1:N
                    source = obj.sources{idx};
                    destination = obj.destinations{idx};
                    csi_transmit{idx} = obj.compute_channel_state_information_transmit(source);
                    csi_receive{idx} = obj.compute_channel_state_information_receive(destination);
                end
                obj.channel_state_information_transmit = csi_transmit;
                obj.channel_state_information_receive = csi_receive;
            else
                warning('Number of sources and number of destinations in network must be equal. Not computing CSI.');
            end
        end
        
        function csi = compute_channel_state_information_transmit(obj,dev)
            % COMPUTE_CHANNEL_STATE_INFORMATION_TRANSMIT Computes the
            % transmit channel state information for a device.
            % Specifically, it collects the CSI across all links (forward 
            % and reverse) in which the device could be a transmitter. This
            % reduces to collecting CSI across forward links where the
            % device is the head and across reverse links where the device
            % is the tail.
            %
            % Usage:
            %  csi = COMPUTE_CHANNEL_STATE_INFORMATION_TRANSMIT(dev)
            %
            % Args:
            %  dev: a device object
            %
            % Returns:
            %  csi: a struct of channel state information values
            idx = 1;
            for idx_link = 1:obj.num_links
                head = obj.links{idx_link}.head;
                if isequal(head,dev)
                    csi{idx} = obj.links{idx_link}.compute_channel_state_information_forward();
                    idx = idx + 1;
                end
                tail = obj.links{idx_link}.tail;
                if isequal(tail,dev)
                    csi{idx} = obj.links{idx_link}.compute_channel_state_information_reverse();
                    idx = idx + 1;
                end
            end
        end
        
        function csi = compute_channel_state_information_receive(obj,dev)
            % COMPUTE_CHANNEL_STATE_INFORMATION_RECEIVE Computes the
            % receive channel state information for a device.
            % Specifically, it collects the CSI across all links (forward 
            % and reverse) in which the device could be a receiver. This
            % reduces to collecting CSI across forward links where the
            % device is the tail and across reverse links where the device
            % is the head.
            %
            % Usage:
            %  csi = COMPUTE_CHANNEL_STATE_INFORMATION_RECEIVE(dev)
            %
            % Args:
            %  dev: a device object
            %
            % Returns:
            %  csi: a struct of channel state information values
            idx = 1;
            for idx_link = 1:obj.num_links
                tail = obj.links{idx_link}.tail;
                if isequal(tail,dev)
                    csi{idx} = obj.links{idx_link}.compute_channel_state_information_forward();
                    idx = idx + 1;
                end
                head = obj.links{idx_link}.head;
                if isequal(head,dev)
                    csi{idx} = obj.links{idx_link}.compute_channel_state_information_reverse();
                    idx = idx + 1;
                end
            end
        end
        
        function supply_channel_state_information(obj)
            % SUPPLY_CHANNEL_STATE_INFORMATION Supplies channel state
            % information to each of the sources and destinations in the
            % network.
            %
            % Usage:
            %  SUPPLY_CHANNEL_STATE_INFORMATION()
            for idx_sd = 1:obj.num_sources
                source = obj.sources{idx_sd};
                destination = obj.destinations{idx_sd};
                csi_transmit = obj.channel_state_information_transmit{idx_sd};
                csi_receive = obj.channel_state_information_receive{idx_sd};
                source.set_transmit_channel_state_information(csi_transmit);
                destination.set_receive_channel_state_information(csi_receive);
            end
        end
        
        % -----------------------------------------------------------------
        % Passthrough functions.
        % -----------------------------------------------------------------
        
        function set_path_loss(obj,path_loss_object)
            % SET_PATH_LOSS Sets the path loss model used on each link.
            %
            % Usage:
            %  SET_PATH_LOSS(path_loss_object)
            %
            % Args:
            %  path_loss_object: a path loss object
            for idx = 1:obj.num_links
                obj.links{idx}.set_path_loss(path_loss_object);
            end
        end
        
        function set_channel(obj,channel_object)
            % SET_CHANNEL Sets the channel model used on each link.
            %
            % Usage:
            %  SET_channel(channel_object)
            %
            % Args:
            %  channel_object: a channel object
            for idx = 1:obj.num_links
                obj.links{idx}.set_channel(channel_object);
            end
        end
        
        function set_symbol_bandwidth(obj,B)
            % SET_SYMBOL_BANDWIDTH Sets the symbol bandwidth on each link.
            %
            % Usage:
            %  SET_SYMBOL_BANDWIDTH(B)
            %
            % Args:
            %  B: symbol bandwidth in Hertz
            obj.symbol_bandwidth = B;
            for idx = 1:obj.num_links
                obj.links{idx}.set_symbol_bandwidth(B);
            end
        end
        
        function set_propagation_velocity(obj,velocity)
            % SET_PROPAGATION_VELOCITY Sets the propagation vecocity on 
            % each link.
            %
            % Usage:
            %  SET_PROPAGATION_VELOCITY(velocity)
            %
            % Args:
            %  velocity: propagation velocity in meters per second
            % 
            % Notes:
            %  : Also updates the carrier wavelength
            obj.propagation_velocity = velocity;
            obj.carrier_wavelength = obj.propagation_velocity / obj.carrier_frequency;
            for idx = 1:obj.num_links
                obj.links{idx}.set_propagation_velocity(velocity);
            end
            for idx = 1:obj.num_devices
                obj.devices{idx}.set_propagation_velocity(velocity);
            end
        end
        
        function set_carrier_frequency(obj,fc)
            % SET_CARRIER_FREQUENCY Sets the carrier frequency on each 
            % link.
            %
            % Usage:
            %  SET_CARRIER_FREQUENCY(fc)
            %
            % Args:
            %  fc: carrier frequency in Hertz
            % 
            % Notes:
            %  : Also updates the carrier wavelength
            obj.carrier_frequency = fc;
            obj.carrier_wavelength = obj.propagation_velocity / obj.carrier_frequency;
            for idx = 1:obj.num_links
                obj.links{idx}.set_carrier_frequency(fc);
            end
            for idx = 1:obj.num_devices
                obj.devices{idx}.set_carrier_frequency(fc);
            end
        end
        
        function set_num_streams(obj,num_streams)
            % SET_NUM_STREAMS Sets the number of streams on each link.
            %
            % Usage:
            %  SET_NUM_STREAMS(num_streams)
            %
            % Args:
            %  num_streams: number of data streams
            for idx = 1:obj.num_links
                obj.links{idx}.set_num_streams(num_streams);
            end
        end
        
        function set_arrays(obj,transmit_array,receive_array)
            % SET_ARRAYS Sets the transmit and receive arrays used on each 
            % link.
            %
            % Usage:
            %  SET_ARRAYS(transmit_array,receive_array)
            %
            % Args:
            %  transmit_array: array object
            %  receive_array: array object
            if nargin < 3 || isempty(receive_array)
                receive_array = transmit_array;
            end
            for idx = 1:obj.num_links
                obj.links{idx}.set_arrays(transmit_array,receive_array);
            end
        end
        
        function set_num_rf_chains(obj,Lt,Lr)
            % SET_NUM_RF_CHAINS Sets the transmit and receive RF chains 
            % used on each link.
            %
            % Usage:
            %  SET_NUM_RF_CHAINS(Lt)
            %  SET_NUM_RF_CHAINS(Lt,Lr)
            %
            % Args:
            %  Lt: number of transmit RF chains
            %  Lr: (optional) number of receive RF chains; if not passed,
            %  the number of transmit RF chains is used
            if nargin < 3 || isempty(Lr)
                Lr = Lt;
            end
            for idx = 1:obj.num_links
                obj.links{idx}.set_num_rf_chains(Lt,Lr);
            end
        end
        
        function set_transmit_power(obj,P,unit)
            % SET_TRANSMIT_POWER Sets the transmit power used on each link.
            %
            % Usage:
            %  SET_TRANSMIT_POWER(P)
            %  SET_TRANSMIT_POWER(P,unit)
            %
            % Args:
            %  P: transmit power
            %  unit: (optional) a string specifying the unit of P (e.g.,
            %  'dBm', 'watts'); if not passed, the default is used
            if nargin < 3
                unit = [];
            end
            for idx = 1:obj.num_links
                obj.links{idx}.set_transmit_power(P,unit);
            end
        end
        
        function set_transmit_symbol(obj,s)
            % SET_TRANSMIT_SYMBOL Sets the transmit symbol used on each
            % link.
            %
            % Usage:
            %  SET_TRANSMIT_SYMBOL(s)
            %
            % Args:
            %  s: a symbol vector
            for idx = 1:obj.num_links
                obj.links{idx}.set_transmit_symbol(s);
            end
        end
        
        function set_transmit_symbol_covariance(obj,Rs)
            % SET_TRANSMIT_SYMBOL_COVARIANCE Sets the transmit covariance
            % on each link.
            %
            % Usage:
            %  SET_TRANSMIT_SYMBOL_COVARIANCE(Rs)
            %
            % Args:
            %  Rs: a covariance matrix
            for idx = 1:obj.num_links
                obj.links{idx}.set_transmit_symbol_covariance(Rs);
            end
        end
        
        function set_noise_power_per_Hz(obj,noise_psd,unit)
            % set_noise_power_per_Hz Sets the noise power spectral density
            % on each link.
            %
            % Usage:
            %  set_noise_power_per_Hz(noise_psd)
            %  set_noise_power_per_Hz(noise_psd,unit)
            %
            % Args:
            %  noise_psd: noise power spectral density
            %  unit: (optional) a string specifying the unit of noise_psd;
            %  if not passed, the default will be used
            if nargin < 3
                unit = [];
            end
            for idx = 1:obj.num_links
                obj.links{idx}.set_noise_power_per_Hz(noise_psd,unit);
            end
        end
        
        function set_snr(obj,snr_forward,snr_reverse,unit)
            % SET_SNR Sets the large-scale SNR on each of the links.
            %
            % Usage:
            %  SET_SNR(snr_forward)
            %  SET_SNR(snr_forward,[],unit)
            %  SET_SNR(snr_forward,snr_reverse,unit)
            %
            % Args:
            %  snr_forward: the SNR to use on the forward links
            %  snr_reverse: (optional) the SNR to use on the reverse links;
            %  if not passed, snr_forward is used; 
            %  unit: (optional) a string specifying the units of
            %  snr_forward and snr_reverse (e.g., 'dB' or 'linear')
            if nargin < 2
                snr_forward = [];
            end
            if nargin < 3 || isempty(snr_reverse)
                snr_reverse = snr_forward;
            end
            if nargin < 4 || isempty(unit)
                unit = [];
            end
            for idx = 1:obj.num_links
                obj.links{idx}.set_snr(snr_forward,snr_reverse,unit);
            end 
        end
        
        % -----------------------------------------------------------------
        % Realization functions.
        % -----------------------------------------------------------------
        
        function realization(obj)
            % REALIZATION Invokes a realization of the network, including
            % all of the links it contains.
            %
            % Usage:
            %  REALIZATION()
            for idx = 1:obj.num_links
                obj.links{idx}.realization();
            end
        end
        
        function configure_transmitter(obj,strategy)
            % CONFIGURE_TRANSMITTER Configures each source in the network
            % according to their existing/specified transmit strategy.
            % 
            % Usage:
            %  CONFIGURE_TRANSMITTER()
            %  CONFIGURE_TRANSMITTER(strategy)
            %
            % Args:
            %  strategy: (optional) a string specifying the transmit
            %  strategy to use, overwriting existing ones
            if nargin < 2
                strategy = [];
            end
            for idx = 1:obj.num_sources
                obj.sources{idx}.configure_transmitter(strategy);
            end
        end
        
        function configure_receiver(obj,strategy)
            % CONFIGURE_RECEIVER Configures each destination in the network
            % according to their existing/specified receive strategy.
            % 
            % Usage:
            %  CONFIGURE_RECEIVER()
            %  CONFIGURE_RECEIVER(strategy)
            %
            % Args:
            %  strategy: (optional) a string specifying the receive
            %  strategy to use, overwriting existing ones
            if nargin < 2
                strategy = [];
            end
            for idx = 1:obj.num_destinations
                obj.destinations{idx}.configure_receiver(strategy);
            end
        end
        
        function [idx,fwd_rev] = get_link_index_source_destination(obj,source,destination)
            % GET_LINK_INDEX_SOURCE_DESTINATION Returns the index of the 
            % link with a specific source-destination pair and an indicator
            % for if that link is a forward or reverse link.
            % 
            % Usage:
            %  [idx,fwd_rev] = GET_LINK_INDEX_SOURCE_DESTINATION(source,destination)
            %
            % Args:
            %  source: a device object
            %  destination: a device object
            %
            % Returns:
            %  idx: the index of the link whose head and tail are the
            %  source and destination (or vice versa); returns 0 if not
            %  found
            %  fwd_rev: a boolean indicating if the head or tail of the 
            %  link is the source (0 if head = source, forward; 1 if tail =
            %  source, reverse)
            idx_fwd = obj.get_link_index_source_destination_forward(source,destination);
            idx_rev = obj.get_link_index_source_destination_reverse(source,destination);
            if isequal(idx_fwd,0)
                idx = idx_rev;
                fwd_rev = 1;
            else
                idx = idx_fwd;
                fwd_rev = 0;
            end
        end
                
        function idx = get_link_index_source_destination_forward(obj,source,destination)
            % GET_LINK_INDEX_SOURCE_DESTINATION_FORWARD Returns the index 
            % of the link whose head and tail are specific source and 
            % destination devices, respectively (i.e., which link has head
            % = source and tail = destination).
            % 
            % Usage:
            %  idx = GET_LINK_INDEX_SOURCE_DESTINATION_FORWARD(source,destination)
            %
            % Args:
            %  source: a device object
            %  destination: a device object
            %
            % Returns:
            %  idx: the index of the link whose head and tail are the
            %  source and destination, respectively; returns 0 if not found
            idx = 0;
            for i = 1:obj.num_links
                head = obj.links{i}.head;
                tail = obj.links{i}.tail;
                if isequal(head,source) && isequal(tail,destination)
                    idx = i;
                end
            end
        end
        
        function idx = get_link_index_source_destination_reverse(obj,source,destination)
            % GET_LINK_INDEX_SOURCE_DESTINATION_REVERSE Returns the index 
            % of the link whose tail and head are specific source and 
            % destination devices, respectively (i.e., which link has head 
            % = destination and tail = source).
            % 
            % Usage:
            %  idx = GET_LINK_INDEX_SOURCE_DESTINATION_REVERSE(source,destination)
            %
            % Args:
            %  source: a device object
            %  destination: a device object
            %
            % Returns:
            %  idx: the index of the link whose tail and head are the
            %  source and destination, respectively; returns 0 if not found
            idx = 0;
            for i = 1:obj.num_links
                head = obj.links{i}.head;
                tail = obj.links{i}.tail;
                if isequal(head,destination) && isequal(tail,source)
                    idx = i;
                end
            end
        end
        
        function [idx,fwd_rev] = get_link_index_destination(obj,destination)
            % GET_LINK_INDEX_DESTINATION Returns the indeces of the links
            % in the network that contain a destination device. Also
            % returns whether each of those links is forward or reverse.
            %
            % Usage:
            %  [idx,fwd_rev] = GET_LINK_INDEX_DESTINATION(destination)
            %
            % Args:
            %  destination: a destination device
            % 
            % Returns:
            %  idx: a vector of indeces of links in the network
            %  fwd_rev: a vector of booleans indicating if the forward or
            %  reverse link contains the destination device as the
            %  receiving side (0 for forward, 1 for reverse)
            idx = [];
            fwd_rev = [];
            for i = 1:obj.num_links
                head = obj.links{i}.head;
                tail = obj.links{i}.tail;
                if isequal(tail,destination)
                    idx = [idx i];
                    fwd_rev = [fwd_rev 0];
                end
                if isequal(head,destination)
                    idx = [idx i];
                    fwd_rev = [fwd_rev 1];
                end
            end
            if isempty(idx)
                idx = 0;
                % warning('Forward link not found with given destination.');
            end
        end
        
        function [Ry,Rn] = compute_covariance_desired(obj,source,destination)
            % COMPUTE_COVARIANCE_DESIRED Computes the covariance of the
            % received symbols at a destination device from its source.
            % Also returns the covariance of received noise.
            %
            % Usage:
            %  [Ry,Rn] = COMPUTE_COVARIANCE_DESIRED(source,destination)
            %
            % Args:
            %  source: a source device
            %  destination: a destination device
            %
            % Returns:
            %  Ry: the desired signal covariance
            %  Rn: the noise covariance
            [idx,fwd_rev] = obj.get_link_index_source_destination(source,destination);
            lnk = obj.links{idx};
            P = source.transmitter.transmit_energy_per_symbol;
            F = source.transmitter.precoder;
            Rs = source.transmitter.transmit_symbol_covariance;
            W = destination.receiver.combiner;
            % N = destination.receiver.noise_energy_per_symbol;
            Rn = destination.receiver.noise_covariance;
            if isequal(fwd_rev,0)
                G = lnk.large_scale_gain_forward;
                H = lnk.channel_matrix_forward;
            else
                G = lnk.large_scale_gain_reverse;
                H = lnk.channel_matrix_reverse;
            end
            % SNR = P * G^2 / N;
            Ry = P * G^2 * (W' * (H * (F * Rs * F') * H') * W);
            Rn = W' * Rn * W;
        end
        
        function [R] = compute_covariance_interference(obj,source,destination)
            % COMPUTE_COVARIANCE_INTERFERENCE Computes the covariance of
            % received interference at a destination device (i.e., the
            % contribution of all interference links).
            %
            % Usage:
            %  R = COMPUTE_COVARIANCE_INTERFERENCE(source,destination)
            %
            % Args:
            %  source: a source device
            %  destination: a destination device
            %
            % Returns:
            %  R: the covariance matrix of interference
            %
            % Notes:
            %  : Assumes independence across interferers.
            [idx,fwd_rev] = obj.get_link_index_destination(destination);
            [idx_des,~] = obj.get_link_index_source_destination(source,destination);
            fwd_rev = fwd_rev(idx ~= idx_des);
            idx = idx(idx ~= idx_des);
            R = zeros(destination.receiver.Ns);
            for i = 1:length(idx)
                lnk = obj.links{idx(i)};
                if isequal(fwd_rev(i),0)
                    G = lnk.large_scale_gain_forward;
                    H = lnk.channel_matrix_forward;
                    src = lnk.head;
                else
                    G = lnk.large_scale_gain_reverse;
                    H = lnk.channel_matrix_reverse;
                    src = lnk.tail;
                end
                P = src.transmitter.transmit_energy_per_symbol;
                F = src.transmitter.precoder;
                Rs = src.transmitter.transmit_symbol_covariance;
                W = destination.receiver.combiner;
                Ry = P * G^2 * (W' * (H * (F * Rs * F') * H') * W);
                R = R + Ry;
            end
        end
        
        function y = compute_received_signal(obj,destination)
            % COMPUTE_RECEIVED_SIGNAL Computes the signal impinging the
            % receive array at a destination device. This is comprised of
            % all desired and interfering transmitters as well as additive
            % noise.
            %
            % Usage:
            %  y = COMPUTE_RECEIVED_SIGNAL(destination)
            %
            % Args:
            %  destination: a device object
            %
            % Returns:
            %  y: the vector impinging the receive array at the destination
            [idx,fwd_rev] = obj.get_link_index_destination(destination);
            y = 0;
            for i = 1:length(idx)
                lnk = obj.links{idx(i)};
                if isequal(fwd_rev(i),0)
                    G = lnk.large_scale_gain_forward;
                    H = lnk.channel_matrix_forward;
                    source = lnk.head;
                else
                    G = lnk.large_scale_gain_reverse;
                    H = lnk.channel_matrix_reverse;
                    source = lnk.tail;
                end
                P = source.transmitter.transmit_energy_per_symbol;
                F = source.transmitter.precoder;
                s = source.transmitter.transmit_symbol;
                z = sqrt(P) * G * H * F * s;
                y = y + z;
            end
            % Nr = destination.receiver.num_antennas;
            % N = destination.receiver.noise_energy_per_symbol;
            % n = cgauss_rv(0,N,Nr,1);
            destination.set_noise();
            % y = y + n;
            destination.set_received_signal(y);
        end
        
        function compute_received_signals(obj)
            % COMPUTE_RECEIVED_SIGNALS Computes the received signal at each
            % destination device in the network.
            %
            % Usage:
            %  COMPUTE_RECEIVED_SIGNALS()
            for idx = 1:obj.num_destinations
                d = obj.destinations{idx};
                obj.compute_received_signal(d);
            end
        end
        
        function mi = report_mutual_information(obj,source,destination)
            % REPORT_MUTUAL_INFORMATION Reports the Gaussian mutual
            % information achieved between a source-destination pair based
            % on the current network realization and transmit-receive
            % configurations. Returns the mutual information in bps/Hz. 
            % Discards any residual imaginary component.
            %
            % Usage:
            %  mi = REPORT_MUTUAL_INFORMATION(source,destination)
            %
            % Args:
            %  source: a source device
            %  destination: a destination device
            % 
            % Returns:
            %  mi: the mutual information in bps/Hz
            %
            % Notes:
            %  : Will warn the user if a significant imaginary portion exists
            %  when computing the mutual information. Ultimately discards
            %  it though.
            [Ry,Rn] = obj.compute_covariance_desired(source,destination);
            Rz = obj.compute_covariance_interference(source,destination);
            I = eye(size(Ry));
            % mi = log2(det(I + Ry / (Rn + Rz)));
            r = rank(Rn);
            mi = log2(det(I(1:r,1:r) + Ry(1:r,1:r) / (Rn(1:r,1:r) + Rz(1:r,1:r))));
            if abs(imag(mi)) > 1e-6
                warning('FYI: Imaginary portion of mutual information was greater than 1e-6 in magnitude.');
            end
            mi = real(mi);
        end
        
        function [err,nerr] = report_symbol_estimation_error(obj,source,destination)
            % REPORT_SYMBOL_ESTIMATION_ERROR Returns the squared-error in the
            % estimated receive symbol at the destination compared to the
            % transmit symbol at the source.
            %
            % Usage:
            %  [err,nerr] = REPORT_SYMBOL_ESTIMATION_ERROR(source,destination)
            %
            % Returns:
            %  err: the squared error of the received symbols versus the
            %  transmitted symbols
            %  nerr: err normalized to the transmit symbol power
            s_tx = source.transmitter.transmit_symbol;
            s_rx = destination.receiver.receive_symbol;
            err = norm(s_tx - s_rx,2)^2;
            nerr = err ./ norm(s_tx,2)^2;
        end
                
        function fig = show_3d(obj,fignum)
            % SHOW_3D Displays the network in 3-D.
            %
            % Usage:
            %  SHOW_3D()
            %  SHOW_3D(fignum)
            %
            % Args:
            %  fignum: (optional) a figure number; if not passed, a new 
            %  figure will be created
            %
            % Returns:
            %  fig: the resulting a figure handle
            if nargin < 2 || isempty(fignum)
                fig = figure();
            else
                fig = figure(fignum);
            end
            ax = gca();
            leg = {};
            for i = 1:obj.num_devices
                dev = obj.devices{i};
                xh = dev.coordinate(1);
                yh = dev.coordinate(2);
                zh = dev.coordinate(3);
                plot3(xh,yh,zh,dev.marker); hold(ax,'on');
                leg{i} = dev.name;
            end
            for i = 1:obj.num_links
                head = obj.links{i}.head;
                xh = head.coordinate(1);
                yh = head.coordinate(2);
                zh = head.coordinate(3);
                tail = obj.links{i}.tail;
                xt = tail.coordinate(1);
                yt = tail.coordinate(2);
                zt = tail.coordinate(3);
                color = obj.links{i}.color;
                line_style = obj.links{i}.line_style;
                plot3(xh,yh,zh,head.marker); hold(ax,'on');
                plot3(xt,yt,zt,tail.marker); 
                plot3([xh,xt],[yh,yt],[zh,zt],'Color',color,'LineStyle',line_style);
            end
            hold(ax,'off');
            legend(ax,leg,'Location','EastOutside');
            grid(ax,'on');
            xlabel(ax,'$x$ (meters)');
            ylabel(ax,'$y$ (meters)');
            zlabel(ax,'$z$ (meters)');
        end
        
        function fig = show_2d(obj,fignum)
            % SHOW_2D Displays the network in 2-D.
            %
            % Usage:
            %  SHOW_2D()
            %  SHOW_2D(fignum)
            %
            % Args:
            %  fignum: (optional) a figure number; if not passed, a new 
            %  figure will be created
            %
            % Returns:
            %  fig: the resulting a figure handle
            if nargin < 2 || isempty(fignum)
                fig = figure();
            else
                fig = figure(fignum);
            end
            ax = gca();
            leg = {};
            for i = 1:obj.num_devices
                dev = obj.devices{i};
                xh = dev.coordinate(1);
                yh = dev.coordinate(2);
                plot(xh,yh,dev.marker); hold(ax,'on');
                leg{i} = dev.name;
            end
            for i = 1:obj.num_links
                dev = obj.links{i}.head;
                xh = dev.coordinate(1);
                yh = dev.coordinate(2);
                tail = obj.links{i}.tail;
                xt = tail.coordinate(1);
                yt = tail.coordinate(2);
                color = obj.links{i}.color;
                line_style = obj.links{i}.line_style;
                plot([xh,xt],[yh,yt],'Color',color,'LineStyle',line_style);
                plot(xh,yh,dev.marker);
                plot(xt,yt,tail.marker);
            end
            legend(ax,leg,'Location','Best');
            hold(ax,'off');
            grid(ax,'on');
            xlabel(ax,'$x$ (meters)');
            ylabel(ax,'$y$ (meters)');
        end
    end
end
