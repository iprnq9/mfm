classdef receiver_hybrid < receiver
    % RECEIVER_HYBRID A hybrid digital/analog beamforming MIMO 
    % receiver, commonly used for millimeter-wave receivers. Note
    % that an analog-only beamforming receiver is a special case of this
    % hybrid beamforming receiver where the number of RF chains is one.
    properties
        % Inherited:
        % name; % human-readable identifier
        % type; % 'digital', 'hybrid'
        % array; % array object
        % Nr; % number of receive antennas
        % num_antennas; % number of receive antennas
        % num_streams; % number of data streams
        % symbol_bandwidth; % symbol boandwidth (Hz)
        % symbol_period; % symbol period (seconds)
        % combiner; % effective combining matrix
        % receive_symbol; % receive symbol vector (e.g., after combiner)
        % channel_state_information; % receive channel state information
        % noise_power; % integrated noise power (watts)
        % noise_energy_per_symbol; % noise energy per symbol (joules/symbol)
        % noise_power_per_Hz; % noise power per 1 Hz bandwidth (joules)
        % noise_covariance; % noise covariance matrix (at antenna)
        % received_noise_covariance; % received noise covariance matrix (after combiner)
        num_rf_chains; % number of RF chains
        combiner_digital; % digital combining matrix
        combiner_analog; % analog combining matrix
        combiner_analog_phase_resolution_bits; % analog phase shifter resolution (bits)
        combiner_analog_amplitude_resolution_bits; % analog attenuator resolution (bits)
        combiner_analog_amplitude_quantization_law; % 'linear' or a negative dB step size (amplitude, not power)
        combiner_hybrid_connections; % Nr-by-Lr indicator matrix (1 indicates connection)
    end
    methods
        function obj = receiver_hybrid(name)
            % RECEIVER_HYBRID Creates an instance of a hybrid beamforming 
            % receiver object.
            if nargin < 1 || isempty(name)
                name = 'receiver-hybrid';
            end
            obj.name = name;
            obj.type = 'hybrid';
            obj.set_combiner_analog_phase_resolution_bits(Inf);
            obj.set_combiner_analog_amplitude_resolution_bits(Inf);
            obj.set_combiner_analog_amplitude_quantization_law('linear');
        end
        
        function set_num_rf_chains(obj,L)
            % SET_NUM_RF_CHAINS Sets the number of RF chains in the
            % receiver's hybrid beamforming structure.
            %
            % Usage:
            %  SET_NUM_RF_CHAINS(L)
            % 
            % Args:
            %  L: number of RF chains
            if L < obj.num_streams
                warning('Number of RF chains is less than number of data streams.');
            end
            obj.num_rf_chains = L;
        end
        
        function set_combiner_analog_phase_resolution_bits(obj,bits)
            % SET_COMBINER_ANALOG_PHASE_RESOLUTION_BITS Sets the resolution
            % (in bits) of the phase shifters in the analog combiner.
            %
            % Usage:
            %  SET_COMBINER_ANALOG_PHASE_RESOLUTION_BITS(bits)
            %
            % Args:
            %  bits: number of bits of phase control
            obj.combiner_analog_phase_resolution_bits = bits;
        end
        
        function set_combiner_analog_amplitude_resolution_bits(obj,bits)
            % SET_COMBINER_ANALOG_AMPLITUDE_RESOLUTION_BITS Sets the resolution
            % (in bits) of the attenuators in the analog combiner.
            %
            % Usage:
            %  SET_COMBINER_ANALOG_AMPLITUDE_RESOLUTION_BITS(bits)
            %
            % Args:
            %  bits: number of bits of amplitude control
            %
            % Notes:
            %  : When bits = Inf, amplitudes of analog beamforming weights
            %  can take on any value
            obj.combiner_analog_amplitude_resolution_bits = bits;
        end
        
        function set_combiner_analog_amplitude_quantization_law(obj,law)
            % SET_COMBINER_ANALOG_AMPLITUDE_QUANTIZATION_LAW Sets the
            % analog combiner's amlitude quantization law (e.g., linear or
            % log).
            %
            % Usage:
            %  SET_COMBINER_ANALOG_AMPLITUDE_QUANTIZATION_LAW(law)
            %
            % Args:
            %  law: either 'linear' for uniform linear amplitude
            %  quantization between (0,1]; or a negative number
            %  corresponding to the (amplitude) attenuation step size in dB
            %
            % Notes:
            %  : A law = -0.25 represents an analog combiner with attenuators
            %  having a step size of -0.25 dB, meaning with 3 bits of
            %  amplitude resolution, for example, the attenuators can
            %  attenuate at most -0.25 dB * 2^3 = -2 dB. Practical
            %  attenuators often are stepped in such a log-scale,
            %  motivating us to support both linear- and log-based
            %  amplitude quantization laws.
            if ~strcmp(law,'linear') && logical(law < 0)
                warning('Invalid law for analog combiner quantization.');
                warning('Using linear law instead.');
                law = 'linear';
            end
            obj.combiner_analog_amplitude_quantization_law = law;
        end
        
        function set_combiner_analog(obj,W)
            % SET_COMBINER_ANALOG Sets the analog combining matrix.
            %
            % Usage:
            %  SET_COMBINER_ANALOG(W)
            % 
            % Args:
            %  W: analog combining matrix
            %
            % Notes:
            %  : Also updates the effective combining matrix.
            obj.combiner_analog = W;
            obj.check_combiner_analog_phase_constraint();
            obj.check_combiner_analog_amplitude_constraint();
            obj.update_combiner();
        end
        
        function set_combiner_digital(obj,W)
            % SET_COMBINER_DIGITAL Sets the digital combiner.
            %
            % Usage:
            %  SET_COMBINER_DIGITAL(W)
            %
            % Args:
            %  W: digital combining matrix
            obj.combiner_digital = W;
            obj.check_combiner_digital_dimensions();
            obj.update_combiner();
        end
        
        function update_combiner(obj)
            % UPDATE_COMBINER Updates the effective combining matrix based
            % on the current analog and digital combiners. Only updates if 
            % the dimensionality supports the matrix multiply.
            % 
            % Usage:
            %  UPDATE_COMBINER()
            W_RF = obj.combiner_analog;
            W_BB = obj.combiner_digital;
            if ~obj.check_combiner_dimensions()
                msg = 'Analog and digital combiner dimensions not yet compatible. Setting effective combiner to empty.';
                warning(msg);
                W = [];
            else
                W = W_RF * W_BB;
            end
            obj.set_combiner(W);
        end
        
        function out = check_combiner_dimensions(obj)
            % CHECK_COMBINER_DIMENSIONS Checks to see if the analog and
            % digital combiners have compatible dimensions.
            %
            % Usage:
            %  out = CHECK_COMBINER_DIMENSIONS()
            %
            % Returns:
            %  out: a boolean indicating if the analog and digital 
            %  combiners have compatible dimensions.
            check_digital = obj.check_combiner_digital_dimensions();
            check_analog = obj.check_analog_combiner_dimensions();
            check = check_digital && check_analog;
            if ~check
                warning('The digital and analog combiners do not have compatible dimensions.');
            end
            out = check;
        end
        
        function out = check_combiner_digital_dimensions(obj)
            % CHECK_COMBINER_DIGITAL_DIMENSIONS Checks to see if the
            % digital combining matrix has dimensions appropriate for the
            % current number of RF chains and streams.
            %
            % Usage:
            %  out = CHECK_COMBINER_DIGITAL_DIMENSIONS()
            %
            % Returns:
            %  out: a boolean indicating if the digital combiner is of
            %  appropriate dimension
            W_BB = obj.combiner_digital;
            [Lr,Ns] = size(W_BB);
            N = obj.num_rf_chains;
            M = obj.num_streams;
            out = true;
            if ~(Lr == N)
                msg = ['The number of rows in the digital combiner (' num2str(Lr) ') should be equal to the number of RF chains (' num2str(N) ').'];
                warning(msg);
                out = false;
            end
            if ~(Ns == M)
                msg = ['The number of columns in the digital combiner (' num2str(Ns) ') should be equal to the number of streams (' num2str(M) ').'];
                warning(msg);
                out = false;
            end
        end
        
        function out = check_combiner_analog_dimensions(obj)
            % CHECK_COMBINER_ANALOG_DIMENSIONS Checks to see if the
            % analog combining matrix has dimensions appropriate for the
            % current number of RF chains and antennas.
            %
            % Usage:
            %  out = CHECK_COMBINER_ANALOG_DIMENSIONS()
            %
            % Returns:
            %  out: a boolean indicating if the analog combiner is of
            %  appropriate dimension
            W_RF = obj.combiner_analog;
            [Nr,Lr] = size(W_RF);
            N = obj.num_rf_chains;
            M = obj.num_antennas;
            out = true;
            if ~(Lr == N)
                msg = ['The number of columns in the analog combiner (' num2str(Lr) ') should be equal to the number of RF chains (' num2str(N) ').'];
                warning(msg);
                out = false;
            end
            if ~(Ns == M)
                msg = ['The number of rows in the analog combiner (' num2str(Nr) ') should be equal to the number of antennas (' num2str(M) ').'];
                warning(msg);
                out = false;
            end
        end
        
        function out = check_combiner_analog_phase_constraint(obj)
            % CHECK_COMBINER_ANALOG_PHASE_CONSTRAINT Checks if the analog
            % combiner has entries whose phases have been quantized to the
            % appropriate resolution.
            %
            % Usage:
            %  out = CHECK_COMBINER_ANALOG_PHASE_CONSTRAINT()
            %
            % Returns:
            %  out: a boolean indicating whether or not the analog phase
            %  constraint has been met
            bits = obj.combiner_analog_phase_resolution_bits;
            if ~isinf(bits)
                W_RF = obj.combiner_analog;
                N = 2^bits;
                step = 2 * pi / N;
                M = mod(angle(W_RF),step);
                M(M < 1e-12) = 0; % to avoid numerical errors
                M = boolean(M);
                if ismember(1,M)
                    out = false;
                else
                    out = true;
                end
            else
                out = true;
            end
        end
        
        function out = check_combiner_analog_amplitude_constraint(obj)
            % CHECK_COMBINER_ANALOG_AMPLITUDE_CONSTRAINT Checks if the analog
            % combiner has entries whose amplitudes have been quantized to 
            % the appropriate resolution.
            %
            % Usage:
            %  out = CHECK_COMBINER_ANALOG_AMPLITUDE_CONSTRAINT()
            %
            % Returns:
            %  out: a boolean indicating whether or not the analog
            %  amplitude constraint has been met
            bits = obj.combiner_analog_amplitude_resolution_bits;
            W_RF = obj.combiner_analog;
            Q = abs(W_RF);
            M = Q > (1 + 1e-12);
            if ismember(1,M) % if any entries are greater than 1 in magnitude
                out = false;
            elseif ~isinf(bits) % check resolution
                law = obj.combiner_analog_amplitude_quantization_law;
                N = 2^bits;
                if strcmp(law,'linear')
                    step = 1 / N;
                else
                    step = law; % law is a negative number and is an amplitude value!
                    Q = 10*log10(Q);
                end
                M = mod(Q,-step);
                M(M < 1e-12) = 0; % to avoid numerical errors
                M = boolean(M);
                if ismember(1,M)
                    out = false;
                else
                    out = true;
                end
            else % less than 1 in magnitude and infinte resolution, no need to check anything
                out = true;
            end
        end
        
        function enforce_combiner_power_budget(obj)
            % ENFORCE_COMBINER_POWER_BUDGET Ensures that the digital
            % combiner is normalized such that the total combining power
            % budget is satisfied.
            % 
            % Usage:
            %  ENFORCE_COMBINER_POWER_BUDGET()
            %
            % Notes:
            %  : Supersedes the function with the same name in the parent.
            P = obj.combiner_power_budget;
            W_RF = obj.combiner_analog;
            W_BB = obj.combiner_digital;
            W = W_RF * W_BB;
            val = norm(W,'fro');
            if val > sqrt(P)
                warning('Effective combiner violates power budget.');
                warning('Scaling digital combiner to meet combining power budget.');
                W_BB = W_BB .* P / val;
            end
            obj.set_combiner_digital(W_BB);
        end
        
        % -----------------------------------------------------------------
        
        function set_hybrid_approximation_method(obj,method)
            % SET_HYBRID_APPROXIMATION_METHOD Sets the method to use during
            % hybrid approximation of a fully-digital beamformer.
            %
            % Usage:
            %  SET_HYBRID_APPROXIMATION_METHOD(method)
            % 
            % Args:
            %  method: a string declaring which method to use
            obj.hybrid_approximation_method = method;
        end
         
        function [X,RF,BB,err] = hybrid_approximation(obj,F)
            % HYBRID_APPROXIMATION Performs hybrid approximation of a
            % fully-digital beamforming matrix based on the hybrid
            % approximation method and any associated options.
            %
            % Usage:
            %  [X,RF,BB,err] = HYBRID_APPROXIMATION(F,opt)
            %
            % Args:
            %  F: fully-digital combining matrix
            %  opt: struct of options specific to the hybrid approximation
            %       method
            %
            % Returns:
            %  X: the effective fully-digital beamforming matrix
            %  RF: the resulting analog beamforming matrix
            %  BB: the resulting digital beamforming matrix
            %  err: the squared Frobenius norm of the error in hyrbrid
            %       approximation
            % 
            % Notes:
            %  : The analog and digital combiners are set after
            %  approximation.
            opt = obj.hybrid_approximation_method;
            method = opt.method;
            if strcmp(method,'omp')
                A = opt.codebook;
                [X,RF,BB] = obj.omp_based_hybrid_approximation(F,A);
            else
                error('Invalid hybrid approximation method.');
            end
            err = norm((F-X).^2,'fro');
            obj.set_combiner_analog(RF);
            obj.set_combiner_digital(BB);
            obj.set_combiner(X);
        end
        
        function [X,X_RF,X_BB] = omp_based_hybrid_approximation(obj,F,A)
            % Spatially Sparse Precoding/Combining Design via OMP
            % 
            % Args:
            %  A: predefined RF beamforming vectors as columns (codebook)
            %  B: desired fully-digital beamformer
            %  C: identity of size Nr by Nr (eye(Nr)) (covariance matrix E[yy*]?)
            %  Nrf: number of RF chains
            %  P: power constraint (optional)           
            P = 1;
            C = eye(obj.array.Na);
            Nrf = obj.num_rf_chains;
            X_res = F;
            X_RF = [];
            for r = 1:Nrf
                PHI = A' * C' * X_res;
                k = argmax(diag(PHI * PHI'));
                X_RF = [X_RF A(:,k)];
                X_BB = inv(X_RF' * C' * C * X_RF) * X_RF' * C' * F;
                num = F - C * X_RF * X_BB;
                X_res = num / norm(num,'fro');
            end
            X_BB = sqrt(P) * X_BB / norm(X_RF * X_BB,'fro');
            X = X_RF * X_BB;
        end
        
        function impose_beamformer_power_constraint(obj)
            % IMPOSE_DIGITAL_BEAMFORMER_POWER_CONSTRAINT Imposes the power
            % constraint to the digital beamformer such that each stream's
            % beamformer has Frobenius norm  of square root of the number
            % of transmit antennas.
            %
            % Usage:
            %  IMPOSE_DIGITAL_BEAMFORMER_POWER_CONSTRAINT()
            F = obj.combiner;
            BB = obj.combiner_digital;
            p = sqrt(obj.Nr);
            p = 1;
            Ns = obj.num_streams;
            N = norm(F,'fro');
            obj.combiner = F ./ N;
            obj.combiner_digital = BB ./ N;
        end
        
        function show_analog_beamformer(obj,idx)
            % SHOW_ANALOG_BEAMFORMER Plots one of the analog beamformers.
            %
            % Usage:
            %  SHOW_ANALOG_BEAMFORMER()
            %  SHOW_ANALOG_BEAMFORMER(idx)
            %
            % Args:
            %  idx: an index specifying which beamformer in the analog
            %  combining matrix to plot (default of 1)
            if nargin < 2 || isempty(idx)
                idx = 1;
            end
            f = obj.combiner_analog(:,idx);
            obj.array.show_beamformer_pattern(f);
        end
                
        function show_effective_beamformer(obj,idx)
            % SHOW_EFFECTIVE_BEAMFORMER Plots one of the effective 
            % beamformers (combination of analog and digital).
            %
            % Usage:
            %  SHOW_EFFECTIVE_BEAMFORMER()
            %  SHOW_EFFECTIVE_BEAMFORMER(idx)
            %
            % Args:
            %  idx: an index specifying which beamformer in the effective
            %  combining matrix to plot (default of 1)
            if nargin < 2 || isempty(idx)
                idx = 1;
            end
            F = obj.combiner_analog * obj.combiner_digital;
            f = F(:,idx);
            obj.array.show_beamformer_pattern(f);
        end
    end
end