classdef receiver < matlab.mixin.Copyable
    % RECEIVER A generic MIMO receiver.
    properties
        name; % human-readable identifier
        type; % 'digital', 'hybrid'
        array; % array object
        num_antennas; % number of receive antennas
        num_streams; % number of data streams
        symbol_bandwidth; % symbol boandwidth (Hz)
        symbol_period; % symbol period (seconds)
        combiner; % effective combining matrix
        noise; % noise vector at the receive antennas
        received_signal; % the vector impinging the receive array
        receive_symbol; % receive symbol vector (e.g., after combiner)
        channel_state_information; % receive channel state information
        noise_power; % integrated noise power (watts)
        noise_energy_per_symbol; % noise energy per symbol (joules/symbol)
        noise_power_per_Hz; % noise power per 1 Hz bandwidth (joules)
        noise_covariance; % noise covariance matrix (at antenna)
        receive_strategy; % specifier telling receiver how to configure itself
        source; % the device object the receiver aims to serve; used for interpreting CSI
    end
    methods(Static)
        function obj = create(type)
            % CREATE Creates a receiver.
            %
            % Usage:
            %  obj = receiver.CREATE()
            %  obj = receiver.CREATE(type)
            %
            % Args:
            %  type: (optional) a string specifying the type of receiver
            %  to create; either 'digital' or 'hybrid'
            %
            % Returns:
            %  obj: a receiver object
            if nargin < 1 || isempty(type)
                type = 'digital';
            end
            if strcmp(type,'digital')
                obj = receiver();
            elseif strcmp(type,'hybrid')
                obj = receiver_hybrid();
            else
                warning('Invalid receiver type. Creating digital receiver.');
                obj = receiver();
            end
        end
    end
    methods
        % -----------------------------------------------------------------
        % Constructor and intialization functions.
        % -----------------------------------------------------------------
        
        function obj = receiver(name)
            % RECEIVER Creates an instance of a receiver object.
            % 
            % Usage:
            %  obj = RECEIVER()
            %  obj = RECEIVER(name)
            %
            % Args:
            %  name: (optional) a name for the receiver
            % 
            % Returns:
            %  obj: a receiver object
            if nargin < 1 || isempty(name)
                name = 'receiver';
            end
            obj.set_name(name);
            obj.set_type('digital');
            obj.initialize();
        end
        
        function initialize(obj)
            % INITIALIZE Executes initializations for a receiver.
            %
            % Usage:
            %  INITIALIZE()
            obj.set_num_streams(1);
            obj.set_receive_strategy('identity');
            obj.set_array(array(1));
            obj.set_symbol_bandwidth(100e6);
            obj.set_noise_power_per_Hz(-174,'dBm_Hz');
        end
        
        % -----------------------------------------------------------------
        % Set functions.
        % -----------------------------------------------------------------
        
        function set_name(obj,name)
            % SET_NAME Sets the name of the receiver.
            %
            % Usage:
            %  SET_NAME()
            %  SET_NAME(name)
            %
            % Args:
            %  name: (optional) a string; if not passed, 'receiver' is
            %  the default name used
            if nargin < 2 || isempty(name)
                name = 'receiver';
            end
            obj.name = name;
        end
        
        function set_type(obj,type)
            % SET_TYPE Sets the type of the receiver.
            %
            % Usage:
            %  SET_TYPE()
            %  SET_TYPE(type)
            %
            % Args:
            %  type: (optional) a string; if not passed, 'digital' is
            %  the default type used
            if nargin < 2 || isempty(type)
                type = 'digital';
            end
            obj.type = type;
        end
        
        function set_receive_symbol(obj,s)
            % SET_RECEIVE_SYMBOL Sets the receive symbol.
            %
            % Usage:
            %  SET_RECEIVE_SYMBOL(s)
            %
            % Args:
            %  s: the receive symbol (e.g., after combiner)
            obj.receive_symbol = s;
        end
        
        function set_received_signal(obj,y)
            % SET_RECEIVED_SIGNAL Sets the received signal vector.
            %
            % Usage:
            %  SET_RECEIVED_SIGNAL(y)
            %
            % Args:
            %  y: the received signal vector (i.e., vector impinging the 
            %  receive array)
            obj.received_signal = y;
            obj.update_receive_symbol();
        end
        
        function set_noise(obj,n)
            % SET_NOISE Sets the noise vector at the receive array.
            %
            % Usage:
            %  SET_NOISE()
            %  SET_NOISE(n)
            %
            % Args:
            %  n: (optional) a noise vector; if not passed, noise will be
            %  generated according to the noise power
            if nargin < 2 || isempty(n)
                N = obj.noise_energy_per_symbol;
                Nr = obj.num_antennas;
                n = cgauss_rv(0,N,Nr,1);
            end
            obj.noise = n;
            % obj.update_receive_symbol();
        end
        
        function set_num_streams(obj,Ns)
            % SET_NUM_STREAMS Sets the number of data streams being
            % received.
            %
            % Usage:
            %  SET_NUM_STREAMS(Ns)
            %
            % Args:
            %  Ns: number of data streams
            obj.num_streams = Ns;
        end
        
        function set_channel_state_information(obj,csi)
            % SET_CHANNEL_STATE_INFORMATION Sets the receive channel state
            % information (CSI).
            %
            % Usage:
            %  SET_CHANNEL_STATE_INFORMATION(csi)
            %
            % Args:
            %  csi: a cell of channel state information structs
            if ~iscell(csi)
                csi = {csi};
            end
            obj.channel_state_information = csi;
        end
        
        function set_array(obj,array)
            % SET_ARRAY Sets the receiver's array object and updates the
            % number of receive antennas.
            %
            % Usage:
            %  SET_ARRAY(array)
            %
            % Args:
            %  array: an array object
            %
            % Notes:
            %  : Also updates the noise covariance.
            obj.array = array;
            obj.num_antennas = obj.array.num_antennas;
            obj.set_noise(ones(obj.num_antennas,1));
            obj.update_noise_covariance();
            obj.configure_receiver_identity();
            obj.set_received_signal(ones(obj.num_antennas,1));
            % obj.configure_receiver();
        end
        
        function set_noise_covariance(obj,Rn)
            % SET_NOISE_COVARIANCE Sets the noise covariance matrix.
            %
            % Usage:
            %  SET_NOISE_COVARIANCE(Rn)
            %
            % Args:
            %  Rn: noise covariance matrix
            obj.noise_covariance = Rn;
        end
        
        function set_symbol_bandwidth(obj,B)
            % SET_SYMBOL_BANDWIDTH Sets the receive symbol bandwidth.
            % 
            % Usage:
            %  SET_SYMBOL_BANDWIDTH(B)
            %
            % Args:
            %  B: symbol bandwidth in Hertz
            % 
            % Notes:
            %  : Also updates the symbol period and noise power.
            obj.symbol_bandwidth = B;
            obj.symbol_period = 1 / B;
            obj.update_noise_power();     
        end
        
        function set_noise_power_per_Hz(obj,noise_psd,unit)
            % SET_NOISE_POWER_PER_HZ Sets the noise power per Hz based for
            % a given noise power spectral density.
            %
            % Usage:
            %  SET_NOISE_POWER_PER_HZ(noise_psd)
            %  SET_NOISE_POWER_PER_HZ(noise_psd,unit)
            %
            % Args:
            %  noise_psd: noise power spectral density (power per Hz)
            %  (e.g., -174 dBm/Hz)
            %  unit: an optional unit specifying the units of noise_psd
            %  (e.g., 'dBm_Hz' for dBm/Hz (default) or 'watts_Hz' for 
            %  watts/Hz)
            % 
            % Notes:
            %  : Also updates the noise power and noise covariance.
            if nargin < 3 || isempty(unit)
                unit = 'dBm_Hz';
            end
            if strcmp(unit,'dBm_Hz')
                noise_power_per_Hz = 10^((noise_psd-30)/10);
            elseif strcmp(unit,'watts_Hz')
                noise_power_per_Hz = noise_psd;
            end
            obj.noise_power_per_Hz = noise_power_per_Hz;
            obj.update_noise_power();
        end
        
        function set_combiner(obj,W)
            % SET_COMBINER Sets the receive combining matrix.
            %
            % Usage:
            %  SET_COMBINER(W)
            %
            % Args:
            %  W: receive combining matrix
            if obj.check_combiner_dimensions(W)
                old_combiner = obj.combiner;
                obj.combiner = W;
                if isequal(size(old_combiner),size(W))
                    obj.update_receive_symbol();
                end
            else
                warning('Combiner does not have valid dimensions. Not setting it.');
            end
        end
        
        function set_receive_strategy(obj,strategy)
            % SET_RECEIVE_STRATEGY Sets the receive strategy.
            %
            % Usage:
            %  SET_RECEIVE_STRATEGY()
            %  SET_RECEIVE_STRATEGY(strategy)
            %
            % Args:
            %  strategy: (optional) a string specifying the receive 
            %  strategy; options vary depending on receiver type 
            %  (digital vs. hybrid); if not passed, identity reception
            %  will be used
            if nargin < 2 || isempty(strategy)
                strategy = 'identity';
            end
            valid_receive_strategy = obj.check_receive_strategy(strategy);
            if valid_receive_strategy
                obj.receive_strategy = strategy;
            else
                warning('Invalid receive strategy. Defaulting to identity reception.');
                obj.receive_strategy = 'identity';
            end
        end
        
        function set_source(obj,device_source)
            % SET_SOURCE Sets the source (device the receiver aims to 
            % serve).
            %
            % Usage:
            %  SET_SOURCE(device_source)
            %
            % Args:
            %  device_source: a device object
            obj.source = device_source;
        end
        
        % -----------------------------------------------------------------
        % Get functions.
        % -----------------------------------------------------------------
        
        function out = get_valid_receive_strategies(obj)
            % GET_VALID_RECEIVE_STRATEGIES Returns a cell containing all
            % valid receive strategy strings.
            %
            % Usage:
            %  out = GET_VALID_RECEIVE_STRATEGIES()
            %
            % Returns:
            %  out: a cell of strings
            %
            % Notes:
            %  : This function will differ between a fully-digital
            %  receiver and hybrid digital/analog receiver to account
            %  for receive strategies that are specific to each.
            %
            %  : This function will need to be updated anytime a custom/new
            %  receive strategy is added.
            %  
            %  : The strings should be all lowercase.
            valid_strategies = {'identity','eigen','mmse','mmse-int'};
            out = valid_strategies;
        end

        function idx = get_source_channel_state_information_index(obj)
            % GET_SOURCE_CHANNEL_STATE_INFORMATION_INDEX Returns the
            % index of the receive CSI entry whose transmit device
            % corresponds to the receiver's source device.
            %
            % Usage:
            %  idx = GET_SOURCE_CHANNEL_STATE_INFORMATION_INDEX()
            %
            % Returns:
            %  idx: an index; if zero, the CSI entry was not found
            idx = 0;
            csi = obj.channel_state_information;
            for i = 1:length(csi)
                source = csi{i}.device_transmit;
                if isequal(source,obj.source)
                    idx = i;
                end
            end
            if isequal(idx,0)
                if ~isempty(obj.source)
                    warning('CSI index not found. Is receiver source set properly?');
                end
            end
        end
        
        function s = get_receive_symbol(obj)
            % GET_RECEIVE_SYMBOL Returns the receive symbol based on the
            % curent received signal vector, noise, and combiner.
            %
            % Usage:
            %  s = GET_RECEIVE_SYMBOL()
            %
            % Returns:
            %  s: the receive symbol
            obj.update_receive_symbol();
            s = obj.receive_symbol;
        end
        
        % -----------------------------------------------------------------
        % Misc. functions.
        % -----------------------------------------------------------------
        
        function turn_off(obj)
            % TURN_OFF Turns off the receiver by setting its combiner to
            % a zeros matrix.
            %
            % Usage:
            %  TURN_OFF()
            obj.set_combiner(zeros(obj.Nr,obj.Ns));
        end
        
        function [csi_des,csi_int] = parse_channel_state_information(obj)
            % PARSE_CHANNEL_STATE_INFORMATION Splits the CSI into two
            % components: (i) a struct containing CSI for receiving from 
            % the source and (ii) a cell of structs containing CSI for 
            % all other links (e.g., interference channels).
            %
            % Usage:
            %  [csi_des,csi_int] = PARSE_CHANNEL_STATE_INFORMATION()
            %
            % Returns:
            %  csi_des: a CSI struct
            %  csi_int: a cell of one or more CSI structs
            csi = obj.channel_state_information;
            idx = obj.get_source_channel_state_information_index();
            if ~isequal(idx,0)
                csi_des = csi{idx};
                csi_int = {csi{1:idx-1} csi{idx+1:end}};
            elseif isempty(obj.source)
                csi_des = csi{1};
                csi_int = {};
            else
                error('CSI index is zero but source has been set. CSI not found for source. Cannot parse CSI.');
            end
        end
        
        % -----------------------------------------------------------------
        % Configure functions.
        % -----------------------------------------------------------------
        
        function configure_receiver(obj,strategy)
            % CONFIGURE_RECEIVER Configures the receiver's combiner
            % according to the current/specified receive strategy, 
            % incorporating channel state information as applicable.
            %
            % Usage:
            %  CONFIGURE_RECEIVER()
            %  CONFIGURE_RECEIVER(strategy)
            %
            % Args:
            %  strategy: (optional) receive strategy to use; if not
            %  passed, the current receive strategy will be used
            if nargin < 2 || isempty(strategy)
                strategy = obj.receive_strategy;
            else
                obj.set_receive_strategy(strategy);
            end
            if strcmpi(strategy,'identity')
                obj.configure_receiver_identity();
            elseif strcmpi(strategy,'eigen')
                obj.configure_receiver_eigen();
            elseif strcmpi(strategy,'mmse')
                obj.configure_receiver_mmse_desired();
            elseif strcmpi(strategy,'mmse-int')
                obj.configure_receiver_mmse_interference();
            else
                warning('Unfamiliar receive strategy. Not configuring receiver.');
            end
        end
        
        function configure_receiver_identity(obj)
            % CONFIGURE_RECEIVER_IDENTITY Configures the receive
            % combiner to an identity matrix.
            %
            % Usage:
            %  CONFIGURE_RECEIVER_IDENTITY()
            Nr = obj.num_antennas;
            Ns = obj.num_streams;
            W = eye(Nr,Ns) ./ sqrt(Ns);
            obj.set_combiner(W);
        end   

        function configure_receiver_eigen(obj)
            % CONFIGURE_RECEIVER_EIGEN Configures the receiver to
            % receive along the strongest components of the desired
            % channel, neglecting interference, etc.
            %
            % Usage:
            %  CONFIGURE_RECEIVER_EIGEN()
            [csi_des, ~] = obj.parse_channel_state_information();
            G = csi_des.large_scale_gain;
            P = csi_des.transmit_energy_per_symbol;
            H = csi_des.channel_matrix;
            Ns = csi_des.num_streams;
            [U,~,~] = svd(H);
            W = U(:,1:Ns); % / (sqrt(P) * G) / S(1:Ns,1:Ns);
            obj.set_combiner(W);
        end
        
        function configure_receiver_mmse_desired(obj)
            % CONFIGURE_RECEIVER_MMSE_DESIRED Configures the receiver in an
            % LMMSE fashion to reject noise. Does not reject interference.
            %
            % Usage:
            %  CONFIGURE_RECEIVER_MMSE_DESIRED()
            [csi_des, ~] = obj.parse_channel_state_information();
            G = csi_des.large_scale_gain;
            P = csi_des.transmit_energy_per_symbol;
            F = csi_des.device_transmit.transmitter.precoder;
            Rs = csi_des.device_transmit.transmitter.transmit_symbol_covariance;
            H = csi_des.channel_matrix;
            % Ns = csi_des.num_streams;
            % N = csi_des.noise_energy_per_symbol;
            Rn = obj.noise_covariance;
            % SNR = P * G^2 / N;
            % Nr = obj.num_antennas;
            W = sqrt(P) * G * inv(P * G^2 * H * (F * Rs * F') * H' + Rn) * (H * F * Rs);
            obj.set_combiner(W);
        end
        
        function configure_receiver_mmse_interference(obj)
            % CONFIGURE_RECEIVER_MMSE_INTERFERENCE Configures the receiver 
            % in an LMMSE fashion to reject noise and interference.
            %
            % Usage:
            %  CONFIGURE_RECEIVER_MMSE_INTERFERENCE()
            [csi_des,csi_int] = obj.parse_channel_state_information();
            Rint = zeros(obj.Nr);
            for idx_int = 1:length(csi_int)
                csi = csi_int{idx_int};
                G = csi.large_scale_gain;
                P = csi.transmit_energy_per_symbol;
                F = csi.device_transmit.transmitter.precoder;
                Rs = csi.device_transmit.transmitter.transmit_symbol_covariance;
                H = csi.channel_matrix;
                Rint = Rint + P * G^2 * H * (F * Rs * F') * H';
            end
            G = csi_des.large_scale_gain;
            P = csi_des.transmit_energy_per_symbol;
            F = csi_des.device_transmit.transmitter.precoder;
            Rs = csi_des.device_transmit.transmitter.transmit_symbol_covariance;
            H = csi_des.channel_matrix;
            % Ns = csi_des.num_streams;
            % N = csi_des.noise_energy_per_symbol;
            Rn = obj.noise_covariance;
            % SNR = P * G^2 / N;
            % Nr = obj.num_antennas;
            W = sqrt(P) * G * inv(P * G^2 * H * (F * Rs * F') * H' + Rint + Rn) * (H * F * Rs);
            obj.set_combiner(W);
        end
        
        % -----------------------------------------------------------------
        % Update functions.
        % -----------------------------------------------------------------
        
        function update_noise_covariance(obj)
            % UPDATE_NOISE_COVARIANCE Updates the noise covariance matrix
            % based on the current number of antennas and noise power.
            %
            % Usage:
            %  UPDATE_NOISE_COVARIANCE()
            %
            % Notes:
            %  : We have assumed i.i.d. noise across antennas.
            Nr = obj.num_antennas;
            N = obj.noise_energy_per_symbol;
            if ~isempty(Nr) && ~isempty(N)
                Rn = eye(Nr) .* N;
                obj.set_noise_covariance(Rn);
            end
        end
        
        function update_noise_power(obj)
            % UPDATE_NOISE_POWER Updates the noise power based on the
            % current noise power per Hz and symbol bandwidth.
            %
            % Usage:
            %  UPDATE_NOISE_POWER()
            % 
            % Notes:
            %  : Also updates the noise covariance matrix.
            % 
            %  : As of now (Jan. 12, 2021), noise per symbol is equal to the
            %  noise power per Hz. Changes to notation or future 
            %  accommodations may change how we define these two quantities
            %  so we are leaving them both present for the time being.
            B = obj.symbol_bandwidth;
            T = obj.symbol_period;
            noise_power_per_Hz = obj.noise_power_per_Hz; % noise power per 1 Hz bandwidth (joules)
            obj.noise_power = noise_power_per_Hz * B; % integrated noise power (watts)
            obj.noise_energy_per_symbol = obj.noise_power * T; % noise energy per symbol (joules/symbol)
            obj.update_noise_covariance();
            obj.set_noise();
        end
        
        function update_receive_symbol(obj)
            % UPDATE_RECEIVE_SYMBOL Updates the receive symbol (after
            % combiner) based on the current received signal and noise.
            %
            % Usage:
            %  UPDATE_RECEIVE_SYMBOL()
            %
            % Notes:
            %  : Should be called when the combiner, received signal, or 
            %  noise are changed.
            W = obj.combiner;
            y = obj.received_signal;
            n = obj.noise;
            s = W' * (y + n);
            obj.set_receive_symbol(s);
        end
        
        % -----------------------------------------------------------------
        % Check functions.
        % -----------------------------------------------------------------
        
        function out = check_combiner_dimensions(obj,W)
            % CHECK_COMBINER_DIMENSIONS Checks to see if the combining 
            % matrix has dimensions appropriate for the current number of 
            % antennas and streams.
            %
            % Usage:
            %  out = CHECK_COMBINER_DIMENSIONS()
            %  out = CHECK_COMBINER_DIMENSIONS(W)
            %
            % Args:
            %  W: (optional) a combining matrix; if not passed, the
            %  receivers's current combiner will be assessed
            %
            % Returns:
            %  out: a boolean indicating if the combiner is of appropriate 
            %  dimension
            if nargin < 2 || isempty(W)
                W = obj.combiner;
            end
            [Nr,Ns] = size(W);
            N = obj.num_antennas;
            M = obj.num_streams;
            out = true;
            if ~(Nr == N)
                msg = ['The number of rows in the combiner (' num2str(Nr) ') should be equal to the number of antennas (' num2str(N) ').'];
                warning(msg);
                out = false;
            end
            if ~(Ns == M)
                msg = ['The number of columns in the combiner (' num2str(Ns) ') should be equal to the number of streams (' num2str(M) ').'];
                warning(msg);
                out = false;
            end
        end
        
        function out = check_receive_strategy(obj,strategy)
            % CHECK_RECEIVE_STRATEGY Returns a boolean indicating if a
            % receive strategy is valid.
            %
            % Usage:
            %  out = CHECK_RECEIVE_STRATEGY(strategy)
            %
            % Args:
            %  strategy: a string indentifying a specific receive strategy
            %
            % Returns:
            %  out: a boolean
            valid_strategies = obj.get_valid_receive_strategies();
            out = ismember(lower(strategy),valid_strategies);
        end
        
        % -----------------------------------------------------------------
        % Shorthand functions.
        % -----------------------------------------------------------------
        
        function val = W(obj)
            % W Returns the combining matrix.
            %
            % Usage:
            %  val = W()
            %
            % Returns:
            %  val: the combining matrix
            val = obj.combiner;
        end
        
        function val = Nr(obj)
            % Nr Returns the number of receive antennas.
            %
            % Usage:
            %  val = Nr()
            %
            % Returns:
            %  val: the number of receive antennas
            val = obj.num_antennas;
        end
        
        function val = Ns(obj)
            % Ns Returns the number of streams.
            %
            % Usage:
            %  val = Ns()
            %
            % Returns:
            %  val: the number of streams
            val = obj.num_streams;
        end
        
        function val = s(obj)
            % s Returns the receive symbol vector.
            %
            % Usage:
            %  val = s()
            %
            % Returns:
            %  val: the receive symbol vector
            val = obj.receive_symbol;
        end
        
        function val = y(obj)
            % y Returns the received signal vector.
            %
            % Usage:
            %  val = y()
            %
            % Returns:
            %  val: the received signal vector
            val = obj.received_signal;
        end
        
        function val = n(obj)
            % n Returns the noise vector (per-antenna).
            %
            % Usage:
            %  val = n()
            %
            % Returns:
            %  val: the noise vector (per-antenna)
            val = obj.noise;
        end
        
        function val = Rn(obj)
            % Rn Returns the noise covariance matrix.
            %
            % Usage:
            %  val = Rn()
            %
            % Returns:
            %  val: the noise covariance matrix
            val = obj.noise_covariance;
        end
        
        function val = N0(obj)
            % N0 Returns the noise energy per symbol.
            %
            % Usage:
            %  val = N0()
            %
            % Returns:
            %  val: the noise energy per symbol
            val = obj.noise_energy_per_symbol;
        end
    end
end