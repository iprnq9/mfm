classdef transmitter < matlab.mixin.Copyable
    % TRANSMITTER A generic MIMO transmitter.
    properties
        name; % human-readable identifier
        type; % 'digital', 'hybrid'
        array; % transmit array object
        num_antennas = 1; % number of transmit antennas
        num_streams = 1; % number of data streams
        symbol_bandwidth; % symbol boandwidth (Hz)
        symbol_period; % symbol period (seconds)
        transmit_power; % transmit power (Watts)
        transmit_energy_per_symbol; % transmit energy per symbol
        transmitted_signal; % the vector exiting the transmit array
        transmit_symbol; % transmit symbol vector
        transmit_symbol_covariance; % covariance matrix of transmit symbols
        precoder; % transmit precoding matrix
        precoder_power_budget; % total power allotted to precoding matrix
        channel_state_information; % transmit channel state information
        transmit_strategy; % specifier telling transmitter how to configure itself
        destination; % the device object the transmitter aims to serve; used for interpreting CSI
    end
    methods(Static)
        function obj = create(type)
            % CREATE Creates a transmitter.
            %
            % Usage:
            %  obj = transmitter.CREATE()
            %  obj = transmitter.CREATE(type)
            %
            % Args:
            %  type: (optional) a string specifying the type of transmitter
            %  to create; either 'digital' or 'hybrid'
            %
            % Returns:
            %  obj: a transmitter object
            if nargin < 1 || isempty(type)
                type = 'digital';
            end
            if strcmp(type,'digital')
                obj = transmitter();
            elseif strcmp(type,'hybrid')
                obj = transmitter_hybrid();
            else
                warning('Invalid transmitter type. Creating digital transmitter.');
                obj = transmitter();
            end
        end
    end
    methods
        function obj = transmitter(name)
            % TRANSMITTER Creates an instance of a transmitter object.
            %
            % Usage:
            %  obj = TRANSMITTER()
            %  obj = TRANSMITTER(name)
            %
            % Args:
            %  name: (optional) a name for the transmitter
            %
            % Returns:
            %  obj: a transmitter object
            if nargin < 1 || isempty(name)
                name = 'transmitter';
            end
            obj.set_name(name);
            obj.set_type('digital');
            obj.initialize();
        end
        
        function initialize(obj)
            % INITIALIZE Executes initializations for a transmitter.
            %
            % Usage:
            %  INITIALIZE()
            obj.set_array(array(1));
            obj.set_num_streams(1);
            % obj.set_transmit_symbol(ones(obj.Ns,1)./sqrt(obj.Ns));
            obj.set_transmit_power(1);
            obj.set_symbol_bandwidth(100e6);
            obj.set_precoder_power_budget(1);
            obj.set_transmit_strategy('identity');
            obj.configure_transmitter();
        end
        
        % -----------------------------------------------------------------
        % Set functions.
        % -----------------------------------------------------------------
        
        function set_name(obj,name)
            % SET_NAME Sets the name of the transmitter.
            %
            % Usage:
            %  SET_NAME()
            %  SET_NAME(name)
            %
            % Args:
            %  name: (optional) a string; if not passed, 'transmitter' is
            %  the default name used
            if nargin < 2 || isempty(name)
                name = 'transmitter';
            end
            obj.name = name;
        end
        
        function set_type(obj,type)
            % SET_TYPE Sets the type of the transmitter.
            %
            % Usage:
            %  SET_TYPE()
            %  SET_TYPE(type)
            %
            % Args:
            %  type: (optional) a string; if not passed, 'digital' is
            %  the default type used
            if nargin < 2 || isempty(type)
                type = 'digital';
            end
            obj.type = type;
        end
        
        function set_transmit_symbol(obj,s)
            % SET_TRANSMIT_SYMBOL Sets the symbol vector to be transmitted.
            %
            % Usage:
            %  SET_TRANSMIT_SYMBOL(s)
            %
            % Args:
            %  s: the symbol vector transmitted
            s = s(:);
            if isequal(length(s),obj.num_streams)
                obj.transmit_symbol = s;
                % obj.update_transmitted_signal();
            else
                warning('The symbol vector is not appropriate size. Not setting.');
            end
        end
        
        function set_transmitted_signal(obj,x)
            % SET_TRANSMITTED_SIGNAL Sets the signal vector that is  
            % transmitted (i.e., the signal that exits the transmit array).
            %
            % Usage:
            %  SET_TRANSMITTED_SIGNAL(x)
            %
            % Args:
            %  x: the vector leaving the transmit array (i.e., 
            %  x = sqrt(Ptx) *  F * s)
            obj.transmitted_signal = x;
        end
        
        function set_num_streams(obj,Ns)
            % SET_NUM_STREAMS Sets the number of data streams being
            % transmitted.
            %
            % Usage:
            %  SET_NUM_STREAMS(Ns)
            %
            % Args:
            %  Ns: number of data streams
            %
            % Notes:
            %  : Also updates the transmit symbol covariance to 
            %  Rs = eye(Ns) ./ sqrt(Ns). This can be overwritten if needed
            %  using set_transmit_symbol_covariance().
            % 
            %  : Also updates the precoder power budget to the number of
            %  streams. This can be overwritten if needed.
            obj.num_streams = Ns;
            Rs = eye(Ns) ./ Ns;
            obj.set_transmit_symbol_covariance(Rs);
            obj.set_transmit_symbol(ones(Ns,1)./sqrt(Ns));
            obj.set_precoder_power_budget(Ns);
            obj.set_precoder();
        end
        
        function set_channel_state_information(obj,csi)
            % SET_CHANNEL_STATE_INFORMATION Sets the transmit channel state 
            % information (CSI).
            %
            % Usage:
            %  SET_CHANNEL_STATE_INFORMATION(csi)
            %
            % Args:
            %  csi: a cell of channel state information structs or a single
            %  struct of csi
            if ~iscell(csi)
                csi = {csi};
            end
            obj.channel_state_information = csi;
        end
        
        function set_array(obj,array)
            % SET_ARRAY Sets the transmitter's array object and updates the
            % number of transmit antennas.
            %
            % Usage:
            %  SET_ARRAY(array)
            %
            % Args:
            %  array: an array object
            %
            % Notes:
            %  : Also updates number of antennas based on array.
            obj.array = copy_object(array);
            obj.num_antennas = obj.array.num_antennas;
            obj.set_precoder();
        end
        
        function set_precoder_power_budget(obj,P)
            % SET_PRECODER_POWER_BUDGET Sets the precoding matrix power
            % budget.
            % 
            % Usage:
            %  SET_PRECODER_POWER_BUDGET(P)
            %
            % Args:
            %  P: total power budget (squared Frobenius norm) of effective 
            %  precoding matrix
            obj.precoder_power_budget = P;        
        end
        
        function set_transmit_power(obj,P,unit)
            % SET_TRANSMIT_POWER Sets the transmit power.
            %
            % Usage:
            %  SET_TRANSMIT_POWER(P)
            %  SET_TRANSMIT_POWER(P,unit)
            %
            % Args:
            %  P: the transmit power
            %  unit: an optional unit specifying the units of P (default of
            %  Watts)
            if nargin < 3 || isempty(unit)
                unit = 'Watts';
            end
            if strcmp(unit,'dBm')
                P = 10^((P-30)/10);
            elseif strcmp(unit,'dBW')
                P = 10^(P/10);
            end
            obj.transmit_power = P;
            obj.update_transmit_energy_per_symbol();
            obj.update_transmitted_signal();
        end

        function set_symbol_bandwidth(obj,B)
            % SET_SYMBOL_BANDWIDTH Sets the transmit symbol bandwidth.
            % 
            % Usage:
            %  SET_SYMBOL_BANDWIDTH(B)
            %
            % Args:
            %  B: symbol bandwidth in Hertz
            % 
            % Notes:
            %  : Also updates the symbol period and transmit energy per 
            %  symbol accordingly.
            obj.symbol_bandwidth = B;
            obj.symbol_period = 1 / B;
            obj.update_transmit_energy_per_symbol();
        end
        
        function set_transmit_symbol_covariance(obj,Rs)
            % SET_TRANSMIT_SYMBOL_COVARIANCE Sets the transmit symbol
            % covariance matrix.
            %
            % Usage:
            %  SET_TRANSMIT_SYMBOL_COVARIANCE()
            %  SET_TRANSMIT_SYMBOL_COVARIANCE(Rs)
            %
            % Args:
            %  Rs: (optional) the covariance matrix of the transmit
            %  symbols; if not passed, the default is an identity matrix
            %  normalized to unit power
            if nargin < 2
                Rs = eye(obj.Ns) ./ sqrt(obj.Ns);
            end
            if ~ishermitian(Rs)
                warning('Transmit symbol covariance matrix is not Hermitian. Setting anyway.');
            end
            obj.transmit_symbol_covariance = Rs;
        end
        
        function set_precoder(obj,F)
            % SET_PRECODER Sets the transmit precoding matrix.
            %
            % Usage:
            %  SET_PRECODER()
            %  SET_PRECODER(F)
            %
            % Args:
            %  F: transmit precoding matrix; if not provided an identity
            %  matrix of size Nt-by-Ns will be used
            if nargin < 2 || isempty(F)
                % F = eye(obj.Nt,obj.Ns);
                obj.configure_transmitter('identity');
            elseif obj.check_precoder_dimensions(F)
                obj.precoder = F;
                obj.enforce_precoder_power_budget();
                obj.update_transmitted_signal();
            else
                warning('Precoder does not have valid dimensions. Not setting it.');
            end
        end
        
        function set_transmit_strategy(obj,strategy)
            % SET_TRANSMIT_STRATEGY Sets the transmit strategy.
            %
            % Usage:
            %  SET_TRANSMIT_STRATEGY()
            %  SET_TRANSMIT_STRATEGY(strategy)
            %
            % Args:
            %  strategy: (optional) a string specifying the transmit 
            %  strategy; options vary depending on transmitter type 
            %  (digital vs. hybrid); if not passed, identity transmission
            %  will be used
            if nargin < 2 || isempty(strategy)
                strategy = 'identity';
            end
            valid_transmit_strategy = obj.check_transmit_strategy(strategy);
            if valid_transmit_strategy
                obj.transmit_strategy = strategy;
            else
                warning('Invalid transmit strategy. Defaulting to identity transmission.');
                obj.transmit_strategy = 'identity';
            end
        end
        
        function set_destination(obj,device_destination)
            % SET_DESTINATION Sets the destination (device the transmitter
            % aims to serve).
            %
            % Usage:
            %  SET_DESTINATION(device_destination)
            %
            % Args:
            %  device_destination: a device object
            obj.destination = device_destination;
        end
        
        % -----------------------------------------------------------------
        % Get functions.
        % -----------------------------------------------------------------
        
        function out = get_valid_transmit_strategies(obj)
            % GET_VALID_TRANSMIT_STRATEGIES Returns a cell containing all
            % valid transmit strategy strings.
            %
            % Usage:
            %  out = GET_VALID_TRANSMIT_STRATEGIES()
            %
            % Returns:
            %  out: a cell of strings
            %
            % Notes:
            %  : This function will differ between a fully-digital
            %  transmitter and hybrid digital/analog transmitter to account
            %  for transmit strategies that are specific to each.
            %
            %  : This function will need to be updated anytime a custom/new
            %  transmit strategy is added.
            %  
            %  : The strings should be all lowercase.
            valid_strategies = {'identity','eigen'};
            out = valid_strategies;
        end
        
        function idx = get_destination_channel_state_information_index(obj)
            % GET_DESTINATION_CHANNEL_STATE_INFORMATION_INDEX Returns the
            % index of the transmit CSI entry whose receive device
            % corresponds to the transmitter's destination device.
            %
            % Usage:
            %  idx = GET_DESTINATION_CHANNEL_STATE_INFORMATION_INDEX()
            %
            % Returns:
            %  idx: an index; if zero, the CSI entry was not found
            idx = 0;
            csi = obj.channel_state_information;
            for i = 1:length(csi) % can be faster by making this a while loop
                dest = csi{i}.device_receive;
                if isequal(dest,obj.destination)
                    idx = i;
                end
            end
            if isequal(idx,0)
                if ~isempty(obj.destination)
                    warning('CSI index not found. Is transmitter destination set?');
                end
            end
        end
        
        % -----------------------------------------------------------------
        % Misc. functions.
        % -----------------------------------------------------------------
        
        function turn_off(obj)
            % TURN_OFF Turns off the transmitter by setting its precoder to
            % a zeros matrix.
            %
            % Usage:
            %  TURN_OFF()
            obj.set_precoder(zeros(obj.Nt,obj.Ns));
        end
        
        function [csi_des,csi_int] = parse_channel_state_information(obj)
            % PARSE_CHANNEL_STATE_INFORMATION Splits the CSI into two
            % components: (i) a struct containing CSI for transmitting to 
            % the destination and (ii) a cell of structs containing CSI for 
            % all other links (e.g., interference channels).
            %
            % Usage:
            %  [csi_des,csi_int] = PARSE_CHANNEL_STATE_INFORMATION()
            %
            % Returns:
            %  csi_des: a CSI struct
            %  csi_int: a cell of one or more CSI structs
            csi = obj.channel_state_information;
            idx = obj.get_destination_channel_state_information_index();
            if ~isequal(idx,0)
                csi_des = csi{idx};
                csi_int = {csi{1:idx-1} csi{idx+1:end}};
            elseif isempty(obj.destination)
                csi_des = csi{1};
                csi_int = {};
            else
                error('CSI not found for destination.');
            end
        end
        
        % -----------------------------------------------------------------
        % Configure functions.
        % -----------------------------------------------------------------
        
        function configure_transmitter(obj,strategy)
            % CONFIGURE_TRANSMITTER Configures the transmitter's precoder
            % according to the current/specified transmit strategy, 
            % incorporating channel state information as applicable.
            %
            % Usage:
            %  CONFIGURE_TRANSMITTER()
            %  CONFIGURE_TRANSMITTER(strategy)
            %
            % Args:
            %  strategy: (optional) transmit strategy to use; if not
            %  passed, the current transmit strategy will be used
            if nargin < 2 || isempty(strategy)
                strategy = obj.transmit_strategy;
            else
                obj.set_transmit_strategy(strategy);
            end
            if strcmpi(strategy,'identity')
                obj.configure_transmitter_identity();
            elseif strcmpi(strategy,'eigen')
                obj.configure_transmitter_eigen();
            else
                warning('Unfamiliar transmit strategy. Not configuring transmitter.');
            end
        end
        
        function configure_transmitter_identity(obj)
            % CONFIGURE_TRANSMITTER_IDENTITY Configures the transmit
            % precoder to an identity matrix.
            %
            % Usage:
            %  CONFIGURE_TRANSMITTER_IDENTITY()
            Nt = obj.num_antennas;
            Ns = obj.num_streams;
            F = eye(Nt,Ns);
            obj.set_precoder(F);
        end   

        function configure_transmitter_eigen(obj)
            % CONFIGURE_TRANSMITTER_EIGEN Configures the transmitter to
            % transmit along the strongest components of the desired
            % channel, neglecting interference, etc.
            %
            % Usage:
            %  CONFIGURE_TRANSMITTER_EIGEN()
            [csi_des, ~] = obj.parse_channel_state_information();
            H = csi_des.channel_matrix;
            SNR = csi_des.snr;
            P = obj.precoder_power_budget;
            Ns = obj.num_streams;
            [~,~,V] = svd(H);
            F_V = V(:,1:Ns);
            p = waterfilling(H,P,SNR,Ns);
            F = F_V * diag(p);
            obj.set_precoder(F);
        end
        
        % -----------------------------------------------------------------
        % Update functions.
        % -----------------------------------------------------------------
               
        function update_transmit_energy_per_symbol(obj)
            % UPDATE_TRANSMIT_ENERGY_PER_SYMBOL Updates the energy per 
            % symbol property.
            % 
            % Usage:
            %  UPDATE_TRANSMIT_ENERGY_PER_SYMBOL()
            % 
            % Notes:
            %  : Is automatically called when transmit power or symbol
            %  bandwidth is set.
            obj.transmit_energy_per_symbol = obj.transmit_power * obj.symbol_period;
        end
        
        function update_transmitted_signal(obj)
            % UPDATE_TRANSMITTED_SIGNAL Recomputes the transmitted signal
            % vector.
            %
            % Usage:
            %  UPDATE_TRANSMITTED_SIGNAL()
            %
            % Notes:
            %  : Should be called anytime the transmit power, precoder, or
            %  transmit symbol are changed.
            P = obj.transmit_energy_per_symbol;
            F = obj.precoder;
            s = obj.transmit_symbol;
            x = sqrt(P) * F * s;
            obj.set_transmitted_signal(x);
        end
        
        % -----------------------------------------------------------------
        % Check functions.
        % -----------------------------------------------------------------
        
        function out = check_precoder_dimensions(obj,F)
            % CHECK_PRECODER_DIMENSIONS Checks to see if the precoding 
            % matrix has dimensions appropriate for the current number of 
            % antennas and streams.
            %
            % Usage:
            %  out = CHECK_PRECODER_DIMENSIONS()
            %  out = CHECK_PRECODER_DIMENSIONS(F)
            %
            % Args:
            %  F: (optional) a precoding matrix; if not passed, the
            %  transmitter's current precoder will be assessed
            %
            % Returns:
            %  out: a boolean indicating if the precoder is of appropriate 
            %  dimension
            if nargin < 2 || isempty(F)
                F = obj.precoder;
            end
            [Nt,Ns] = size(F);
            N = obj.num_antennas;
            M = obj.num_streams;
            out = true;
            if ~(Nt == N)
                msg = ['The number of rows in the precoder (' num2str(Nt) ') should be equal to the number of antennas (' num2str(N) ').'];
                warning(msg);
                out = false;
            end
            if ~(Ns == M)
                msg = ['The number of columns in the precoder (' num2str(Ns) ') should be equal to the number of streams (' num2str(M) ').'];
                warning(msg);
                out = false;
            end
        end
        
        function out = check_transmit_strategy(obj,strategy)
            % CHECK_TRANSMIT_STRATEGY Returns a boolean indicating if a
            % transmit strategy is valid.
            %
            % Usage:
            %  out = CHECK_TRANSMIT_STRATEGY(strategy)
            %
            % Args:
            %  strategy: a string indentifying a specific transmit strategy
            %
            % Returns:
            %  out: a boolean
            valid_strategies = obj.get_valid_transmit_strategies();
            out = ismember(lower(strategy),valid_strategies);
        end
        
        function out = check_precoder_power_budget(obj,F)
            % CHECK_PRECODER_POWER_BUDGET Checks to see if the
            % precoding matrix meets the allotted precoding power budget.
            % 
            % Usage:
            %  out = CHECK_PRECODER_POWER_BUDGET()
            %  out = CHECK_PRECODER_POWER_BUDGET(F)
            % 
            % Args:
            %  F: (optional) a precoding matrix; if not passed, the 
            %  transmitter's current precoding matrix will be assessed
            %
            % Returns:
            %  out: a boolean indicating if the precoding power budget is
            %  met by the current precoding matrix (returns true if met)
            if nargin < 2 || isempty(F)
                F = obj.precoder;
            end
            P = obj.precoder_power_budget;
            val = norm(F,'fro')^2;
            out = logical(val <= P + 1e-3); % to avoid numerical issues
        end
        
        % -----------------------------------------------------------------
        % Enforce functions.
        % -----------------------------------------------------------------
        
        function enforce_precoder_power_budget(obj)
            % ENFORCE_PRECODER_POWER_BUDGET Scales the precoding matrix to
            % ensure that it satisfies the precoding power budget.
            %
            % Usage:
            %  ENFORCE_PRECODER_POWER_BUDGET()
            F = obj.precoder;
            P = obj.precoder_power_budget;
            met = obj.check_precoder_power_budget();
            if ~met
                % warning('Precoder does not satisfy precoder power budget. Enforcing.');
                F = F ./ norm(F,'fro') .* sqrt(P);
                obj.set_precoder(F);
            end
        end
        
        % -----------------------------------------------------------------
        % Shorthand functions.
        % -----------------------------------------------------------------
        
        function val = F(obj)
            % F Returns the precoding matrix.
            %
            % Usage:
            %  val = F()
            %
            % Returns:
            %  val: the precoding matrix
            val = obj.precoder;
        end
        
        function val = Nt(obj)
            % Nt Returns the number of transmit antennas.
            %
            % Usage:
            %  val = Nt()
            %
            % Returns:
            %  val: the number of transmit antennas
            val = obj.num_antennas;
        end
        
        function val = Ns(obj)
            % Ns Returns the number of streams.
            %
            % Usage:
            %  val = Ns()
            %
            % Returns:
            %  val: the number of streams
            val = obj.num_streams;
        end
        
        function val = s(obj)
            % s Returns the transmit symbol vector.
            %
            % Usage:
            %  val = s()
            %
            % Returns:
            %  val: the transmit symbol vector
            val = obj.transmit_symbol;
        end
        
        function val = x(obj)
            % x Returns the transmitted signal vector.
            %
            % Usage:
            %  val = x()
            %
            % Returns:
            %  val: the transmitted signal vector
            val = obj.transmitted_signal;
        end
        
        function val = Rs(obj)
            % Rs Returns the transmit symbol covariance matrix.
            %
            % Usage:
            %  val = Rs()
            %
            % Returns:
            %  val: the transmit symbol covariance matrix
            val = obj.transmit_symbol_covariance;
        end
        
        function val = P(obj)
            % P Returns the transmit energy per symbol (joules per symbol).
            %
            % Usage:
            %  val = P()
            %
            % Returns:
            %  val: the transmit energy per symbol
            val = obj.transmit_energy_per_symbol;
        end
        
        function val = Ptx(obj)
        % Ptx Returns the transmit energy per symbol (joules per symbol).
            %
            % Usage:
            %  val = Ptx()
            %
            % Returns:
            %  val: the transmit energy per symbol
            val = obj.transmit_energy_per_symbol;
        end
    end
end