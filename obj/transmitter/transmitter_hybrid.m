classdef transmitter_hybrid < transmitter
    % TRANSMITTER_HYBRID A hybrid digital/analog beamforming MIMO 
    % transmitter, commonly used for millimeter-wave transmitters. Note
    % that an analog-only beamforming transmitter is a special case of this
    % hybrid beamforming transmitter where the number of RF chains is one.
    properties
        % Inherited:
        % name; % human-readable identifier
        % type; % 'digital', 'hybrid'
        % array; % transmit array object
        % num_antennas; % number of transmit antennas
        % num_streams; % number of data streams
        % symbol_bandwidth; % symbol boandwidth (Hz)
        % symbol_period; % symbol period (seconds)
        % transmit_power; % transmit power (Watts)
        % transmit_energy_per_symbol; % transmit energy per symbol
        % transmit_symbol; % transmit symbol vector
        % transmit_symbol_covariance; % covariance matrix of transmit symbols
        % precoder; % transmit precoding matrix
        % precoder_power_budget; % total power allotted to precoding matrix
        % channel_state_information; % transmit channel state information
        % transmit_strategy; % specifier telling transmitter how to configure itself
        % destination; % the device object the transmitter aims to serve; used for interpreting CSI
        num_rf_chains; % number of RF chains
        precoder_digital; % digital precoding matrix
        precoder_analog; % analog precoding matrix
        precoder_digital_power_budget; % digital precoding power budget
        precoder_analog_phase_resolution_bits; % analog phase shifter resolution (bits)
        precoder_analog_amplitude_resolution_bits; % analog attenuator resolution (bits)
        precoder_analog_amplitude_quantization_law; % 'linear' or a negative dB step size (amplitude, not power)
        precoder_hybrid_connections; % Nt-by-Lt indicator matrix (1 indicates connection)
    end
    methods
        function obj = transmitter_hybrid(name)
            % TRANSMITTER_HYBRID Creates an instance of a hybrid 
            % beamforming transmitter object.
            if nargin < 1 || isempty(name)
                name = 'transmitter-hybrid';
            end
            obj.set_name(name);
            obj.set_type('hybrid');
            obj.initialize();
            obj.initialize_hybrid();
        end
        
        function initialize_hybrid(obj)
            % INITIALIZE_HYBRID Executes initializations for a hybrid
            % digital/analog transmitter.
            %
            % Usage:
            %  INITIALIZE_HYBRID()
            %
            % Notes:
            %  : Should be executed after the default transmitter
            %  initialization (transmitter.initialize()).
            obj.set_num_rf_chains(1);
            obj.set_precoder_digital_power_budget(Inf);
            obj.set_precoder_analog_phase_resolution_bits(Inf);
            obj.set_precoder_analog_amplitude_resolution_bits(Inf);
            obj.set_precoder_analog_amplitude_quantization_law('linear');
        end
        
        % -----------------------------------------------------------------
        % Set functions.
        % -----------------------------------------------------------------
        
        function set_num_rf_chains(obj,L)
            % SET_NUM_RF_CHAINS Sets the number of RF chains in the
            % transmitter's hybrid beamforming structure.
            %
            % Usage:
            %  SET_NUM_RF_CHAINS(L)
            % 
            % Args:
            %  L: number of RF chains
            if L < obj.num_streams
                warning('Number of RF chains is less than number of data streams.');
            end
            obj.num_rf_chains = L;
        end
        
        function set_precoder_analog_phase_resolution_bits(obj,bits)
            % SET_PRECODER_ANALOG_PHASE_RESOLUTION_BITS Sets the resolution
            % (in bits) of the phase shifters in the analog precoder.
            %
            % Usage:
            %  SET_PRECODER_ANALOG_PHASE_RESOLUTION_BITS(bits)
            %
            % Args:
            %  bits: number of bits of phase control
            %
            % Notes:
            %  : When bits = Inf, phase control is not quantized.
            obj.precoder_analog_phase_resolution_bits = bits;
        end
        
        function set_precoder_analog_amplitude_resolution_bits(obj,bits)
            % SET_PRECODER_ANALOG_AMPLITUDE_RESOLUTION_BITS Sets the resolution
            % (in bits) of the attenuators in the analog precoder.
            %
            % Usage:
            %  SET_PRECODER_ANALOG_AMPLITUDE_RESOLUTION_BITS(bits)
            %
            % Args:
            %  bits: number of bits of amplitude control
            %
            % Notes:
            %  : When bits = Inf, amplitude control is not quantized.
            obj.precoder_analog_amplitude_resolution_bits = bits;
        end
        
        function set_precoder_analog_amplitude_quantization_law(obj,law)
            % SET_PRECODER_ANALOG_AMPLITUDE_QUANTIZATION_LAW Sets the
            % analog precoder's amlitude quantization law (e.g., linear or
            % log).
            %
            % Usage:
            %  SET_PRECODER_ANALOG_AMPLITUDE_QUANTIZATION_LAW(law)
            %
            % Args:
            %  law: either 'linear' for uniform linear amplitude
            %  quantization between (0,1]; or a negative number
            %  corresponding to the (amplitude) attenuation step size in dB
            %
            % Notes:
            %  : A law = -0.25 represents an analog precoder with attenuators
            %  having a step size of -0.25 dB, meaning with 3 bits of
            %  amplitude resolution, for example, the attenuators can
            %  attenuate at most -0.25 dB * 2^3 = -2 dB. Practical
            %  attenuators often are stepped in such a log-scale,
            %  motivating us to support both linear- and log-based
            %  amplitude quantization laws.
            if ~strcmp(law,'linear') && logical(law < 0)
                warning('Invalid law for analog precoder quantization.');
                warning('Using linear law instead.');
                law = 'linear';
            end
            obj.precoder_analog_amplitude_quantization_law = law;
        end
        
        function set_precoder_analog(obj,F)
            % SET_PRECODER_ANALOG Sets the analog precoding matrix.
            %
            % Usage:
            %  SET_PRECODER_ANALOG(F)
            % 
            % Args:
            %  F: analog precoding matrix
            %
            % Notes:
            %  : Also updates the effective precoding matrix.
            out = obj.check_precoder_analog_dimensions(F);
            out = out & obj.check_precoder_analog_phase_constraint(F);
            out = out & obj.check_precoder_analog_amplitude_constraint(F);
            if out
                obj.precoder_analog = F;
                obj.update_precoder();
            else
                warning('Not setting analog precoder.');
            end
        end
        
        function set_precoder_digital(obj,F)
            % SET_PRECODER_DIGITAL Sets the digital precoder.
            %
            % Usage:
            %  SET_PRECODER_DIGITAL(F)
            %
            % Args:
            %  F: digital precoding matrix
            out = obj.check_precoder_digital_dimensions(F);
            if out
                obj.precoder_digital = F;
                obj.enforce_precoder_digital_power_budget();
                obj.update_precoder();
            else
                warning('Not setting digital precoder.');
            end
        end
        
        function set_precoder_digital_power_budget(obj,P)
            % SET_PRECODER_DIGITAL_POWER_BUDGET Sets the power budget of
            % the digital precoding matrix.
            %
            % Usage:
            %  SET_PRECODER_DIGITAL_POWER_BUDGET(P)
            %
            % Args:
            %  P: the digital precoding power budget
            obj.precoder_digital_power_budget = P;
        end
        
        function set_precoder_hybrid_connections(obj,M)
            % SET_PRECODER_HYBRID_CONNECTIONS Defines which RF chains are
            % connected to which antennas, allowing for arbitrary hybrid
            % beamforming configurations, including subarray architectures.
            %
            % Usage:
            %  SET_PRECODER_HYBRID_CONNECTIONS(M)
            %
            % Args:
            %  M: a matrix of booleans whose (i,j)-th entry indicates 
            %  whether or not the i-th antenna is connected to the j-th RF
            %  chain. The number of rows in M should equal the number of
            %  antennas. The number of columns in M should equal the number
            %  of RF chains. A logical 1 indicates a connection. A logical
            %  0 indicates no connection. A subarray architecture, for
            %  example, would take on a block diagonal form.
            out = obj.check_precoder_hybrid_connections_dimensions(M);
            if out
                M = logical(M);
                obj.precoder_hybrid_connections = M;
            else
                warning('Not setting precoder hybrid connections matrix.');
            end
        end
        
        % -----------------------------------------------------------------
        % Update functions.
        % -----------------------------------------------------------------
        
        function update_precoder(obj)
            % UPDATE_PRECODER Updates the effective precoding matrix based
            % on the current analog and digital precoders. Only updates if 
            % the dimensionality supports the matrix multiply.
            % 
            % Usage:
            %  UPDATE_PRECODER()
            F_RF = obj.precoder_analog;
            F_BB = obj.precoder_digital;
            if ~obj.check_precoder_hybrid_dimensions()
                msg = 'Analog and digital precoder dimensions not yet compatible. Setting effective precoder to empty.';
                warning(msg);
                F = [];
            else
                F = F_RF * F_BB;
            end
            obj.set_precoder(F);
        end
        
        % -----------------------------------------------------------------
        % Check functions.
        % -----------------------------------------------------------------
        
        function out = check_precoder_digital_power_budget(F_BB)
            % CHECK_PRECODER_DIGITAL_POWER_BUDGET Checks to see if the
            % digital precoding matrix meets the allotted digital precoding
            % power budget.
            % 
            % Usage:
            %  out = CHECK_PRECODER_DIGITAL_POWER_BUDGET()
            %  out = CHECK_PRECODER_DIGITAL_POWER_BUDGET(F_BB)
            % 
            % Args:
            %  F_BB: (optional) a digital precoding matrix; if not passed, 
            %  the transmitter's current digital precoding matrix will be 
            %  assessed
            %
            % Returns:
            %  out: a boolean indicating if the digital precoding power 
            %  budget is met by the current precoding matrix (returns true 
            %  if met)
            if nargin < 2 || isempty(F_BB)
                F_BB = obj.precoder_digital;
            end
            P = obj.precoder_digital_power_budget;
            val = norm(F_BB,'fro')^2;
            out = logical(val <= P + 1e-3); % to avoid numerical issues
        end
        
        function out = check_precoder_hybrid_dimensions(obj,F_BB,F_RF)
            % CHECK_PRECODER_HYBRID_DIMENSIONS Checks to see if the analog 
            % and digital precoders have appropriate dimensions.
            %
            % Usage:
            %  out = CHECK_PRECODER_HYBRID_DIMENSIONS()
            %  out = CHECK_PRECODER_HYBRID_DIMENSIONS(F_BB)
            %  out = CHECK_PRECODER_HYBRID_DIMENSIONS(F_BB,F_RF)
            %  out = CHECK_PRECODER_HYBRID_DIMENSIONS([],F_RF)
            %  out = CHECK_PRECODER_HYBRID_DIMENSIONS(F_BB,[])
            %
            % Args:
            %  F_BB: (optional) a digital precoding matrix; if not passed, 
            %  the transmitter's current digital precoder will be assessed
            %  F_RF: (optional) an analog precoding matrix; if not passed, 
            %  the transmitter's current analog precoder will be assessed
            %
            % Returns:
            %  out: a boolean indicating if the analog and digital 
            %  precoders have compatible dimensions.
            if nargin < 2
                F_BB = [];
            end
            if nargin < 3
                F_RF = [];
            end
            check_digital = obj.check_precoder_digital_dimensions(F_BB);
            check_analog = obj.check_precoder_analog_dimensions(F_RF);
            check = check_digital && check_analog;
            if ~check
                warning('The digital and analog precoders do not have appropriate dimensions.');
            end
            out = check;
        end
        
        function out = check_precoder_digital_dimensions(obj,F)
            % CHECK_PRECODER_DIGITAL_DIMENSIONS Checks to see if the
            % digital precoding matrix has dimensions appropriate for the
            % current number of RF chains and streams.
            %
            % Usage:
            %  out = CHECK_PRECODER_DIGITAL_DIMENSIONS()
            %  out = CHECK_PRECODER_DIGITAL_DIMENSIONS(F)
            %
            % Args:
            %  F: (optional) a digital precoding matrix; if not passed, the
            %  transmitter's current digital precoder will be assessed
            %
            % Returns:
            %  out: a boolean indicating if the digital precoder is of
            %  appropriate dimension
            if nargin < 2 || isempty(F)
                F = obj.precoder_digital;
            end
            [Lt,Ns] = size(F);
            N = obj.num_rf_chains;
            M = obj.num_streams;
            out = true;
            if ~(Lt == N)
                msg = ['The number of rows in the digital precoder (' num2str(Lt) ') should be equal to the number of RF chains (' num2str(N) ').'];
                warning(msg);
                out = false;
            end
            if ~(Ns == M)
                msg = ['The number of columns in the digital precoder (' num2str(Ns) ') should be equal to the number of streams (' num2str(M) ').'];
                warning(msg);
                out = false;
            end
        end
        
        function out = check_precoder_analog_dimensions(obj,F)
            % CHECK_PRECODER_ANALOG_DIMENSIONS Checks to see if the
            % analog precoding matrix has dimensions appropriate for the
            % current number of RF chains and antennas.
            %
            % Usage:
            %  out = CHECK_PRECODER_ANALOG_DIMENSIONS()
            %  out = CHECK_PRECODER_ANALOG_DIMENSIONS(F)
            %
            % Args:
            %  F: (optional) an analog precoding matrix; if not passed, the
            %  transmitter's current analog precoder will be assessed
            %
            % Returns:
            %  out: a boolean indicating if the analog precoder is of
            %  appropriate dimension
            if nargin < 2 || isempty(F)
                F = obj.precoder_analog;
            end
            [Nt,Lt] = size(F);
            N = obj.num_rf_chains;
            M = obj.num_antennas;
            out = true;
            if ~(Lt == N)
                msg = ['The number of columns in the analog precoder (' num2str(Lt) ') should be equal to the number of RF chains (' num2str(N) ').'];
                warning(msg);
                out = false;
            end
            if ~(Nt == M)
                msg = ['The number of rows in the analog precoder (' num2str(Nt) ') should be equal to the number of antennas (' num2str(M) ').'];
                warning(msg);
                out = false;
            end
        end
                
        function out = check_precoder_analog_phase_constraint(obj,F)
            % CHECK_PRECODER_ANALOG_PHASE_CONSTRAINT Checks if the analog
            % precoder has entries whose phases have been quantized to the
            % appropriate resolution.
            %
            % Usage:
            %  out = CHECK_PRECODER_ANALOG_PHASE_CONSTRAINT()
            %  out = CHECK_PRECODER_ANALOG_PHASE_CONSTRAINT(F)
            %
            % Args:
            %  F: (optional) an analog precoding matrix; if not passed, the
            %  transmitter's current analog precoder will be assessed
            %
            % Returns:
            %  out: a boolean indicating whether or not the analog phase
            %  constraint has been met
            bits = obj.precoder_analog_phase_resolution_bits;
            if ~isinf(bits)
                if nargin < 2 || isempty(F)
                    F_RF = obj.precoder_analog;
                end
                N = 2^bits;
                step = 2 * pi / N;
                M = mod(angle(F_RF),step);
                M(M < 1e-12) = 0; % to avoid numerical errors
                M = boolean(M);
                if ismember(1,M)
                    out = false;
                else
                    out = true;
                end
            else
                out = true;
            end
        end
        
        function out = check_precoder_analog_amplitude_constraint(obj,F)
            % CHECK_PRECODER_ANALOG_AMPLITUDE_CONSTRAINT Checks if the analog
            % precoder has entries whose amplitudes have been quantized to 
            % the appropriate resolution.
            %
            % Usage:
            %  out = CHECK_PRECODER_ANALOG_AMPLITUDE_CONSTRAINT()
            %  out = CHECK_PRECODER_ANALOG_AMPLITUDE_CONSTRAINT(F)
            %
            % Args:
            %  F: (optional) an analog precoding matrix; if not passed, the
            %  transmitter's current analog precoder will be assessed
            %
            % Returns:
            %  out: a boolean indicating whether or not the analog
            %  amplitude constraint has been met
            if nargin < 2 || isempty(F)
                F = obj.precoder_analog;
            end
            bits = obj.precoder_analog_amplitude_resolution_bits;
            Q = abs(F);
            M = Q > (1 + 1e-12);
            if ismember(1,M) % if any entries are greater than 1 in magnitude
                out = false;
            elseif ~isinf(bits) % check resolution
                law = obj.precoder_analog_amplitude_quantization_law;
                N = 2^bits;
                if strcmp(law,'linear')
                    step = 1 / N;
                else
                    step = law; % law is a negative number and is an amplitude value!
                    Q = 10*log10(Q);
                end
                M = mod(Q,-step);
                M(M < 1e-12) = 0; % to avoid numerical errors
                M = boolean(M);
                if ismember(1,M)
                    out = false;
                else
                    out = true;
                end
            else % less than 1 in magnitude and infinte resolution, no need to check anything
                out = true;
            end
        end
                
        function out = check_precoder_hybrid_connections(obj,M,F)
            % CHECK_PRECODER_HYBRID_CONNECTIONS Checks to see if the analog
            % precoder satisfies the constraints imposed by the
            % connectivity between antennas and RF chains.
            %
            % Usage:
            %  out = CHECK_PRECODER_HYBRID_CONNECTIONS()
            %  out = CHECK_PRECODER_HYBRID_CONNECTIONS(M)
            %  out = CHECK_PRECODER_HYBRID_CONNECTIONS(M,F)
            %  out = CHECK_PRECODER_HYBRID_CONNECTIONS([],F)
            %
            % Args:
            %  M: (optional) a hybrid connections matrix; if not passed, 
            %  the transmitter's current hybrid connections matrix will be 
            %  used
            %  F: (optional) an analog precoding matrix; if not passed, the
            %  transmitter's current analog precoder will be assessed
            %
            % Returns:
            %  out: a boolean indicating whether or not the hybrid
            %  connections are satisfied by the analog precoder
            %
            % Notes:
            %  : If the hybrid connections matrix has not been set (is
            %  empty), then it is assumed fully-connected hybrid
            %  beamforming is employed, meaning the structure of the analog
            %  precoder is unconstrained. Thus, out = true.
            if nargin < 2 || isempty(M)
                M = obj.precoder_hybrid_connections();
            end
            if isempty(M)
                out = true; % if not set, assume fully-connected (unconstrained)
            else
                if nargin < 3 || isempty(F)
                    F = obj.precoder_analog;
                end
                Q = logical(abs(F));
                out = isequal(Q,logical(M));
            end
        end
        
        function out = check_precoder_hybrid_connections_dimensions(obj,M)
            % CHECK_PRECODER_HYBRID_CONNECTIONS_DIMENSIONS Checks to see if
            % the hybrid connections matrix has dimensions appropriate for 
            % the current number of RF chains and antennas.
            %
            % Usage:
            %  out = CHECK_PRECODER_HYBRID_CONNECTIONS_DIMENSIONS()
            %  out = CHECK_PRECODER_HYBRID_CONNECTIONS_DIMENSIONS(M)
            %
            % Args:
            %  M: (optional) a hybrid connections matrix; if not passed, 
            %  the transmitter's current hybrid connections matrix will be 
            %  used
            %
            % Returns:
            %  out: a boolean indicating if the hybrid connections matrix 
            %  is of appropriate dimension
            if nargin < 2 || isempty(M)
                M = obj.precoder_hybrid_connections;
            end
            [Nt,Lt] = size(M);
            N = obj.num_rf_chains;
            M = obj.num_antennas;
            out = true;
            if ~(Lt == N)
                msg = ['The number of columns in the hybrid connections matrix (' num2str(Lt) ') should be equal to the number of RF chains (' num2str(N) ').'];
                warning(msg);
                out = false;
            end
            if ~(Nt == M)
                msg = ['The number of rows in the hybrid connections matrix (' num2str(Nt) ') should be equal to the number of antennas (' num2str(M) ').'];
                warning(msg);
                out = false;
            end
        end
        
        % -----------------------------------------------------------------
        % Enforce functions.
        % -----------------------------------------------------------------
        
        function enforce_precoder_power_budget(obj)
            % ENFORCE_PRECODER_POWER_BUDGET Ensures that the digital
            % precoder is normalized such that the total precoding power
            % budget is satisfied.
            % 
            % Usage:
            %  ENFORCE_PRECODER_POWER_BUDGET()
            %
            % Notes:
            %  : Supersedes the function with the same name in the parent in
            %  order to only scale the digital precoding portion.
            F = obj.precoder;
            out = obj.check_precoder_power_budget(F);
            if ~out
                warning('Effective precoder violates power budget.');
                warning('Scaling digital precoder to meet precoding power budget.');
                val = norm(F,'fro');
                P = obj.precoder_power_budget;
                F_BB = obj.precoder_digital;
                F_BB = F_BB .* sqrt(P) / val;
                obj.set_precoder_digital(F_BB);
            end
        end
        
        function enforce_precoder_digital_power_budget(obj)
            % ENFORCE_PRECODER_DIGITAL_POWER_BUDGET Ensures that the 
            % digital precoder is normalized such that the digital 
            % precoding power budget is satisfied.
            % 
            % Usage:
            %  ENFORCE_PRECODER_DIGITAL_POWER_BUDGET()
            F_BB = obj.precoder_digital;
            out = obj.check_precoder_digital_power_budget(F_BB);
            if ~out
                warning('Digital precoder violates power budget.');
                warning('Scaling digital precoder to meet digital precoding power budget.');
                val = norm(F_BB,'fro');
                P = obj.precoder_digital_power_budget;
                F_BB = F_BB .* sqrt(P) / val;
                obj.set_precoder_digital(F_BB);
            end
        end
        
        function enforce_precoder_hybrid_connections(obj)
            % ENFORCE_PRECODER_HYBRID_CONNECTIONS Ensures that the analog
            % precoder is constructed in accordance with the connectivity
            % offered between antennas and RF chains.
            %
            % Usage:
            %  ENFORCE_PRECODER_HYBRID_CONNECTIONS()
            %
            % Notes:
            %  : Updates the analog precoder and effective precoder.
            M = obj.precoder_hybrid_connections;
            F_RF = obj.precoder_analog;
            out = obj.check_precoder_hybrid_connections(M,F_RF);
            if ~out
                F_RF = F_RF .* M;
                obj.set_precoder_analog(F_RF);
                obj.update_precoder();
            end
        end

        % -----------------------------------------------------------------
        % Get functions.
        % -----------------------------------------------------------------
        
        function out = get_valid_transmit_strategies(obj)
            % GET_VALID_TRANSMIT_STRATEGIES Returns a cell containing all
            % valid transmit strategy strings.
            %
            % Usage:
            %  out = GET_VALID_TRANSMIT_STRATEGIES()
            %
            % Returns:
            %  out: a cell of strings
            %
            % Notes:
            %  : This function will differ between a fully-digital
            %  transmitter and hybrid digital/analog transmitter to account
            %  for transmit strategies that are specific to each.
            %
            %  : This function will need to be updated anytime a custom/new
            %  transmit strategy is added.
            valid_strategies = {'identity','eigen'};
            out = valid_strategies;
        end
        
        % -----------------------------------------------------------------
        % Configure functions.
        % -----------------------------------------------------------------
        
%         function configure_precoder_digital(obj,strategy)
%             if strcmpi(strategy,'eigen')
%                 obj.configure_precoder_digital_eigen();
%             else
%                 error('Invalid digital precoding strategy.');
%             end
%         end
%         
%         function configure_precoder_digital_eigen(obj)
%             % CONFIGURE_PRECODER_DIGITAL_EIGEN Configures the precoder to a
%             
%             H = obj.channel_state_information.channel_matrix;
%             N = obj.channel_state_information.noise_power_per_Hz;
%             P = obj.channel_state_information.transmit_energy_per_symbol;
%             G = obj.channel_state_information.large_scale_gain;
%             SNR = P * G^2 / N;
%             Ns = obj.channel_state_information.num_streams;
%         end
%         
        % -----------------------------------------------------------------
        % Shorthand.
        % -----------------------------------------------------------------

        function val = F_RF(obj)
            % F_RF Returns the analog precoding matrix.
            %
            % Usage:
            %  val = F_RF()
            %
            % Returns:
            %  val: the analog precoding matrix
            val = obj.precoder_analog;
        end
        
        function val = F_BB(obj)
            % F_BB Returns the digital precoding matrix.
            %
            % Usage:
            %  val = F_BB()
            %
            % Returns:
            %  val: the digital precoding matrix
            val = obj.precoder_digital;
        end
        
        function val = Lt(obj)
            % Lt Returns the number of transmit RF chains.
            %
            % Usage:
            %  val = Lt()
            %
            % Returns:
            %  val: the number of transmit RF chains
            val = obj.num_rf_chains;
        end
        
        % -----------------------------------------------------------------
        % To do.
        % -----------------------------------------------------------------
        
        function set_hybrid_approximation_method(obj,method)
            % SET_HYBRID_APPROXIMATION_METHOD Sets the method to use during
            % hybrid approximation of a fully-digital beamformer.
            %
            % Usage:
            %  SET_HYBRID_APPROXIMATION_METHOD(method)
            % 
            % Args:
            %  method: a string declaring which method to use
            obj.hybrid_approximation_method = method;
        end
         
        function [X,RF,BB,err] = hybrid_approximation(obj,F)
            % HYBRID_APPROXIMATION Performs hybrid approximation of a
            % fully-digital beamforming matrix based on the hybrid
            % approximation method and any associated options.
            %
            % Usage:
            %  [X,RF,BB,err] = HYBRID_APPROXIMATION(F,opt)
            %
            % Args:
            %  F: fully-digital precoding matrix
            %  opt: struct of options specific to the hybrid approximation
            %       method
            %
            % Returns:
            %  X: the effective fully-digital beamforming matrix
            %  RF: the resulting analog beamforming matrix
            %  BB: the resulting digital beamforming matrix
            %  err: the squared Frobenius norm of the error in hyrbrid
            %       approximation
            % 
            % Notes:
            %  : The analog and digital precoders are set after
            %  approximation.
            opt = obj.hybrid_approximation_method;
            method = opt.method;
            if strcmp(method,'omp')
                A = opt.codebook;
                [X,RF,BB] = obj.omp_based_hybrid_approximation(F,A);
            else
                error('Invalid hybrid approximation method.');
            end
            err = norm((F-X).^2,'fro');
            obj.set_precoder_analog(RF);
            obj.set_precoder_digital(BB);
            obj.set_precoder(X);
        end
        
        function [X,X_RF,X_BB] = omp_based_hybrid_approximation(obj,F,A)
            % Spatially Sparse Precoding/Combining Design via OMP
            % 
            % Args:
            %  A: predefined RF beamforming vectors as columns (codebook)
            %  B: desired fully-digital beamformer
            %  C: identity of size Nt by Nt (eye(Nt)) (covariance matrix E[yy*]?)
            %  Nrf: number of RF chains
            %  P: power constraint (optional)           
            P = 1;
            C = eye(obj.array.Na);
            Nrf = obj.num_rf_chains;
            X_res = F;
            X_RF = [];
            for r = 1:Nrf
                PHI = A' * C' * X_res;
                k = argmax(diag(PHI * PHI'));
                X_RF = [X_RF A(:,k)];
                X_BB = inv(X_RF' * C' * C * X_RF) * X_RF' * C' * F;
                num = F - C * X_RF * X_BB;
                X_res = num / norm(num,'fro');
            end
            X_BB = sqrt(P) * X_BB / norm(X_RF * X_BB,'fro');
            X = X_RF * X_BB;
        end
        
        function impose_beamformer_power_constraint(obj)
            % IMPOSE_DIGITAL_BEAMFORMER_POWER_CONSTRAINT Imposes the power
            % constraint to the digital beamformer such that each stream's
            % beamformer has Frobenius norm  of square root of the number
            % of transmit antennas.
            %
            % Usage:
            %  IMPOSE_DIGITAL_BEAMFORMER_POWER_CONSTRAINT()
            F = obj.precoder;
            BB = obj.precoder_digital;
            p = sqrt(obj.Nt);
            p = 1;
            Ns = obj.num_streams;
            N = norm(F,'fro');
            obj.precoder = F ./ N;
            obj.precoder_digital = BB ./ N;
        end
        
        function show_analog_beamformer(obj,idx)
            % SHOW_ANALOG_BEAMFORMER Plots one of the analog beamformers.
            %
            % Usage:
            %  SHOW_ANALOG_BEAMFORMER()
            %  SHOW_ANALOG_BEAMFORMER(idx)
            %
            % Args:
            %  idx: an index specifying which beamformer in the analog
            %  precoding matrix to plot (default of 1)
            if nargin < 2 || isempty(idx)
                idx = 1;
            end
            f = obj.precoder_analog(:,idx);
            obj.array.show_beamformer_pattern(f);
        end
                
        function show_effective_beamformer(obj,idx)
            % SHOW_EFFECTIVE_BEAMFORMER Plots one of the effective 
            % beamformers (combination of analog and digital).
            %
            % Usage:
            %  SHOW_EFFECTIVE_BEAMFORMER()
            %  SHOW_EFFECTIVE_BEAMFORMER(idx)
            %
            % Args:
            %  idx: an index specifying which beamformer in the effective
            %  precoding matrix to plot (default of 1)
            if nargin < 2 || isempty(idx)
                idx = 1;
            end
            F = obj.precoder_analog * obj.precoder_digital;
            f = F(:,idx);
            obj.array.show_beamformer_pattern(f);
        end
    end
end