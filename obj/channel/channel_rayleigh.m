classdef channel_rayleigh < channel
    % CHANNEL A Rayleigh-faded channel.
    properties
        % Inherited:
        % name; % human-readable identifier
        % channel_matrix; % channel matrix
        % num_antennas_transmit; % number of transmit antennas
        % num_antennas_receive; % number of receive antennas
        % array_transmit; % transmit array at the channel input
        % array_receive; % receive array at the channel output
        % carrier_frequency; % carrier frequency (Hz)
        % carrier_wavelength; % carrier wavelength (m)
        % propagation_velocity; % propagation velocity (m/s)
        % normalized_channel_energy; % total energy the channel matrix has after normalization
        % force_channel_energy_normalization; % a boolean indicating if the channel matrix should be normalized
    end
    methods
        function obj = channel_rayleigh(name)
            % CHANNEL Creates a MIMO channel object.
            % 
            % Usage:
            %  obj = CHANNEL()
            %  obj = CHANNEL(name)
            % 
            % Args:
            %  name: an optional name for the object
            % 
            % Returns:
            %  obj: an object representing a MIMO channel
            if nargin < 1 || isempty(name)
                name = 'channel-rayleigh';
            end
            obj.name = name;
        end
        
        function H = realization(obj)
            % REALIZATION Realizes a Rayleigh channel matrix where each 
            % entry in the channel matrix is drawn i.i.d. from a complex 
            % normal distribution.
            %
            % Usage:
            %  H = REALIZATION()
            % 
            % Returns:
            %  H: a channel matrix whose entries are Rayleigh-faded
            H = cgauss_rv(0,1,obj.Nr(),obj.Nt());
            obj.set_channel_matrix(H);
            H = obj.get_channel_matrix();
        end
    end
end