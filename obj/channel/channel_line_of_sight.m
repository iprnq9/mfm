classdef channel_line_of_sight < channel
    properties
        azimuth;
        elevation;
    end
    methods
        function obj = channel_line_of_sight(name)
            % CHANNEL_LINE_OF_SIGHT Creates a line-of-sight MIMO 
            % channel object.
            % 
            % Usage:
            %  obj = CHANNEL_LINE_OF_SIGHT()
            %  obj = CHANNEL_LINE_OF_SIGHT(name)
            % 
            % Args:
            %  name: an optional name for the object
            % 
            % Returns:
            %  obj: an object representing a line-of-sight MIMO channel
            if nargin < 1 || isempty(name)
                name = 'channel-line-of-sight';
            end
            obj.name = name;
        end
        
        function set_angular_separation(obj,azimuth,elevation)
            % SET_ANGULAR_SEPARATION Sets the angular separation between
            % the transmitter and receiver. If no arguments are passed, the
            % relative angular separation will be computed based on the
            % locations of the transmit array and receive array.
            % 
            % Usage:
            %  SET_ANGULAR_SEPARATION()
            %  SET_ANGULAR_SEPARATION(az)
            %  SET_ANGULAR_SEPARATION(az,el)
            %
            % Args:
            %  az: (optional) the azimuth angle of the line departing the
            %  transmitter (radians); if not passed, 0 will be used
            %  el: (optional) the elevation angle of the line departing the
            %  transmitter (radians); if not passed, 0 will be used
            if isempty(azimuth)
                azimuth = 0;
            end
            if nargin < 3 || isempty(elevation)
                elevation = 0;
            end
            if nargin < 2
                atx_pos = obj.array_transmit.get_array_center();
                arx_pos = obj.array_receive.get_array_center();
                [~,az,el] = get_relative_position(atx_pos,arx_pos);
                azimuth = az;
                elevation = el;
            end
            obj.azimuth = azimuth;
            obj.elevation = elevation;
        end
        
        function H = realization(obj)
            % REALIZATION Realizes the channel based on its current 
            % parameters.
            %
            % Usage:
            %  H = REALIZATION()
            %
            % Returns:
            %  H: the realized channel matrix
            H = obj.channel_realization();
            obj.set_channel_matrix(H);
            H = obj.get_channel_matrix();
        end
        
        function H = channel_realization(obj)
            % CHANNEL_REALIZATION Realizes a random instance of the channel
            % matrix based on the channel object's parameters.
            %
            % Usage:
            %  H = CHANNEL_REALIZATION()
            %
            % Returns:
            %  H: the resulting channel matrix
            az = obj.azimuth;
            el = obj.elevation;
            atx = obj.array_transmit.get_array_response(az,el);
            arx = obj.array_receive.get_array_response(pi+az,pi+el);
            H = arx * atx';
            obj.set_channel_matrix(H);
        end
    end
end