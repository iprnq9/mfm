# MIMO for MATLAB (MFM)

MFM is a MATLAB toolbox for simulating multiple-input multiple-output (MIMO) communication.

[https://mimoformatlab.com](https://mimoformatlab.com)

By: [Ian P. Roberts](https://ianproberts.com)
