classdef mfm < handle
    methods(Static)
        function setup()
            % SETUP Sets up MFM.
            %
            % Usage:
            %  mfm.setup()
            disp(['Setting up MFM...']);

            % -------------------------------------------------------------------------
            % OBJECTS.
            % -------------------------------------------------------------------------
            addpath('obj');
            addpath('obj/array');
            addpath('obj/channel');
            addpath('obj/device');
            addpath('obj/link');
            addpath('obj/network');
            addpath('obj/path_loss');
            addpath('obj/receiver');
            addpath('obj/transmitter');

            % -------------------------------------------------------------------------
            % MAIN SCRIPTS.
            % -------------------------------------------------------------------------
            addpath('main');

            % -------------------------------------------------------------------------
            % UTILITIES.
            % -------------------------------------------------------------------------
            addpath('utilities');
            addpath('utilities/cs');
            addpath('utilities/etc');
            addpath('utilities/figures');
            addpath('utilities/mimo');
            addpath('utilities/rand');
            addpath('utilities/linalg');
            addpath('utilities/geom');
            addpath('utilities/stat');

            disp('Done!');
        end
    end
end
